// AQUI FICA O WEBSERVICE

var express = require("express");
var app = express();
const fs = require('fs')
var cors = require('cors');

app.use(cors());

//Comando para testar: 'node {path_ _file(api.js)}'
app.get("/appMedicamentos", function(req,res){
    fs.readFile('./files/medicamentos.json', 'utf8', (err, jsonString) => {
        if (err) {
            console.log("File read failed:", err)
            return
        }
        res.send(jsonString);
    });
})

var server = app.listen(8091, function(){
    var host = server.address().address
    var port = server.address().port
});
