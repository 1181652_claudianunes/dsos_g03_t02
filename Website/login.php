<!DOCTYPE html>
<html>

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  
  <title>Login</title>

  <link href="styles/sb-admin-2.css" rel="stylesheet">
  <link href="styles/style.css" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

  <div class="login_container container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Bem-vindo!</h1>
                  </div>
                  <form class="user">
                    <div class="form-group">
                      <input type="text" class="form-control form-control-user" id="txtUsername" placeholder="Username">
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control form-control-user" id="txtPassword" placeholder="Password">
                    </div>
                    <a onclick="AutenthicationValidation();" class="btn btn-primary btn-user btn-block" style="cursor:pointer">
                      Login
                    </a>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="scripts/jquery/jquery.js"></script>
  <script src="scripts/bootstrap/js/bootstrap.bundle.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="scripts/jquery-easing/jquery.easing.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="scripts/sb-admin-2.js"></script>

  <script src="login.js"></script>

</body>

</html>
