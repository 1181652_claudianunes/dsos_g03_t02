$(document).ready(function () {

    var tablePorExeutar = $('#tblPorExecutar').DataTable({
        ajax: {
            url: "../method/getTerapeuticas.php",
            dataSrc: "",
            type: "POST",
            data:{
                modoVis: "1"
            }
        },
        bSort: false,
        language: { url: "../scripts/datatables/lang_pt.json" },
        columns: [
            { title: "Utente", data: 'Utente', orderable: false, width: "30%"},
            { title: "Medicamento", data: 'medicamento', orderable: false, width: "30%"},
            { title: "Qtd.", data: 'quantidade', orderable: false, width: "15%"},
            { title: "Data", data: 'dataExecucao', orderable: false, width: "25%"},
        ],
        select: { style: 'single' },
        scrollY: '90vh',
        scrollCollapse: true,
        lengthChange: false,
        dom: "<'row'<'col-md-6 d-flex mb-1'<'toolbar'>>>" +
            "<'row'<'col-sm-12't>>",
        fnInitComplete: function () {
            $('div.toolbar').html("<div class='btn-group' role='group' aria-label='Basic example'>" +  
            "<button id='btnExec' type='button' class='btn btn-light bg-success text-white' onclick='execTerapeutica()'><i class='far fa-check-circle'></i></button>" + 
            "<button id='btnNaoExec' type='button' class='btn btn-light bg-danger text-white' onclick='naoExecTerapeutica()'><i class='far fa-times-circle'></i></button>" + 
            "</div>");
        }
    });

    var tableExecutado = $('#tblExecutado').DataTable({
        ajax: {
            url: "../method/getTerapeuticas.php",
            dataSrc: "",
            type: "POST",
            data:{
                modoVis: "2"
            }
        },
        bSort: false,
        language: { url: "../scripts/datatables/lang_pt.json" },
        columns: [
            { title: "Utente", data: 'Utente', orderable: false, width: "30%"},
            { title: "Medicamento", data: 'medicamento', orderable: false, width: "30%"},
            { title: "Qtd.", data: 'quantidade', orderable: false, width: "15%"},
            { title: "Data", data: 'dataExecucao', orderable: false, width: "25%"},
        ],
        select: { style: 'single' },
        scrollY: '90vh',
        scrollCollapse: true,
        lengthChange: false,
        dom: "<'row'<'col-sm-12't>>",
    });

    $('#tblPorExecutar tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            tablePorExeutar.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });
});

function execTerapeutica(e) {
    var table = $('#tblPorExecutar').DataTable();
    if (!table.rows('.selected').any()) {
        alert("Selecione uma linha.");
        return;
    }

    var obj = table.rows('.selected').data()[0];
    document.getElementById("hfRowId").value = obj.idAgendamento;

    let datahora = new Date().toISOString().substr(0, 19);
    document.querySelector("#execDataHora").value = datahora;

    var modal = $('#execTerapeutica');
    modal.modal('show');
    modal.find('.modal-title').text('Execução de Terapêutica');
}

$("#executar").click(function () {
    var novadata = new Date($('#execDataHora').val());
    var strdata = formatDate(novadata);
    $.ajax({
        url: "../method/execTerapeutica.php",
        type: "POST",
        data: {
            idKey: document.getElementById("hfRowId").value,
            data: strdata,
            gerarmovimento: '1'
        },
        cache: false,
        success: function (dataResult) {
            if (dataResult == "true") {
                var tableExec = $('#tblPorExecutar').DataTable();
                tableExec.ajax.reload();

                var tableNExec = $('#tblExecutado').DataTable();
                tableNExec.ajax.reload();

                var modal = $('#execTerapeutica');
                modal.modal('hide');
            } else {
                alert(dataResult)

            }
        }
    });
});

function naoExecTerapeutica(){
    var table = $('#tblPorExecutar').DataTable();
    if (!table.rows('.selected').any()) {
        alert("Selecione uma linha.");
        return;
    }

    var obj = table.rows('.selected').data()[0];
    $.ajax({
        url: "../method/execTerapeutica.php",
        type: "POST",
        data: {
            idKey: obj.idAgendamento
        },
        cache: false,
        success: function (dataResult) {
            if (dataResult == "true") {
                var tableExec = $('#tblPorExecutar').DataTable();
                tableExec.ajax.reload();

                var tableNExec = $('#tblExecutado').DataTable();
                tableNExec.ajax.reload();
            } else {
                alert(dataResult)

            }
        }
    });
}









