$(document).ready(function() {
    carregarTable();
});

function carregarTable(){
    var table = $('#tblStock').DataTable( {
        ajax: {
            url: "../method/getStock.php",
            dataSrc: "",
            type: "POST",
            data: {
                modoVis: function(){ return document.getElementById("hfModoVis").value; }
            }
        },
        language: { url: "../scripts/datatables/lang_pt.json" },
        columns: [
            {
                title: "Utente",
                data: "utente",
                defaultContent: ""
            },
            {
                title: "Medicamento",
                data: "medicamento" 
            },
            {
                title: "Quantidade",
                data: 'qtd' 
            }
        ],
        select: {
            style: 'single'
        },
        scrollY:'90vh',
        scrollCollapse: true,
        lengthChange: false,
        dom: "<'row'<'col-md-6 d-flex'<'toolbar'>><'col-md-6'f>>" +
			"<'row'<'col-sm-12'tr>>" +
			"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
        fnInitComplete: function(){
            var strSelected = "Por Medicamento";
            if(document.getElementById("hfModoVis").value == "2"){
                strSelected = "Por Utente";
            }

            $('div.toolbar').html("<div class='btn-group' role='group' aria-label='Basic example'>" + 
            "<div class='btn-group' role='group'>" + 
                "<button id='cbModoVis' type='button' class='btn btn-light dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>" + 
                strSelected +
                "</button>" + 
                "<div class='dropdown-menu' aria-labelledby='cbModoVis'>" + 
                "<a id='optModoVis_1' class='dropdown-item' href='#' value='1'>Por Medicamento</a>" + 
                "<a id='optModoVis_2' class='dropdown-item' href='#' value='2'>Por Utente</a>" + 
                "</div>" + 
            "</div>" +
            "<button id='btnTradeStock' type='button' class='btn btn-light' onclick='tradeStock()'><i class='fas fa-exchange-alt'></i></button>" + 
            "</div>");
        }
    } );

    table.on( 'preDraw', function () {
        $("#tblStock").DataTable().column(0).visible(document.getElementById("hfModoVis").value == "1" ? false : true);
    } )

    $('#tblStock tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } ); 

}

$(document).on("click", "a[id^='optModoVis_']", function(e){
    document.getElementById("hfModoVis").value = $(e.target).attr("value");
    $("#cbModoVis").text($(e.target).text());
    var table = $('#tblStock').DataTable();
    table.ajax.reload();
    table.columns.adjust().draw( false );
})

function tradeStock(){
    $("#cbMedicamento").children('option:not(:first)').remove();
    $("#cbMedicamento").val("0");
    $("#cbMedicamento").prop("disabled", true);  
    $("#txtQuantidade").val("");
    $("#txtQuantidade").prop("disabled", true);

    bindForm();

    var modal = $('#tradeStock');
    modal.modal('show');
    modal.find('.modal-title').text('Troca de stock');
}

function bindForm(){
    $.ajax({
        url: '../method/getUtentes.php',
        success: function (data) {
            var retJson = JSON.parse(data);
            var deUtente = $("#cbDeUtente");
            var paraUtente = $("#cbParaUtente");

            deUtente.children('option:not(:first)').remove();
            paraUtente.children('option:not(:first)').remove();
            $.each(retJson, function (index, obj) {
                deUtente.append("<option value='" + obj.idUtente + "'>" + obj.nome + "</option>");
                paraUtente.append("<option value='" + obj.idUtente + "'>" + obj.nome + "</option>");
            });

            
        }
    });
}

$(document).on("change", "#cbDeUtente", function(e) {
    if($(this).val() == "0"){
        $("#cbMedicamento").prop("disabled", true);
        $("#txtQuantidade").prop("disabled", true);
        setPreviousValue(this);
        return;
    }

    //VERIFICAR SE AMBAS AS COMBOBOX TEM O MESMO UTENTE
    if (!verifySameUtente()){
        alert("Selecione utentes diferentes para a troca.")
        $(this).val($(this).data('val'));
        return;
    }
    // GRAVAR NOVO VALOR
    setPreviousValue(this);

    //CARREGAR COMBOBOX DOS MEDICAMENTOS
    $.ajax({
        url: '../method/getStock.php',
        type: 'POST',
        data: {
            modoVis: 2,
            idUtente: $(this).val(),
        },
        success: function (data) {
            var retJson = JSON.parse(data);
            var medicamento = $("#cbMedicamento");
            var qtd = $("#txtQuantidade");

            medicamento.val("0");
            medicamento.prop("disabled", true);  
            qtd.val("");
            qtd.prop("disabled", true);
            qtd.attr('min', '0');

            medicamento.children('option:not(:first)').remove();
            var hasOptions = false;
            $.each(retJson, function (index, obj) {
                if (obj.qtd > 0){
                    medicamento.append("<option value='" + obj.idMedicamento + "' qtd='" + obj.qtd + "'>" + obj.medicamento +"</option>");
                    if (!hasOptions){
                        medicamento.prop("disabled", false);  
                        qtd.prop("disabled", false);
                        hasOptions = true;
                    }
                }
            }); 

        }
    });
});

$(document).on("change", "#cbParaUtente", function(e) {
    if (!verifySameUtente()){
        alert("Selecione utentes diferentes para a troca.")
        $(this).val($(this).data('val'));
        return;
    }
    setPreviousValue(this);
});

function setPreviousValue(obj) {
    $(obj).data('val', $(obj).val());
}

function verifySameUtente(){
    var deUtente = $("#cbDeUtente");
    var paraUtente = $("#cbParaUtente");

    if (deUtente.val() != "0" && paraUtente.val() != "0"){
        if(deUtente.val() == paraUtente.val()){
            return false;
        }   
    }
    return true;
}

function validateForm(form){

    var qtd = $('#cbMedicamento option:selected').attr('qtd');
    if(parseFloat($("#txtQuantidade").val()) > parseFloat(qtd)){
        alert("Quantidade superior ao stock do medicamento.");
        return;
    }

    $.ajax({
        url: form.action,
        type: form.method,
        data: {
            idUtenteDe: $('#cbDeUtente').val(),
            idUtentePara: $('#cbParaUtente').val(),
            idMedicamento: $('#cbMedicamento').val(),
            qtd: $('#txtQuantidade').val()
        },
        cache: false,
        success: function (dataResult) {
            if (dataResult == "true") {
                var table = $('#tblStock').DataTable();
                table.ajax.reload();

                var modal = $('#tradeStock');
                modal.modal('hide');
            } else {
                alert(dataResult)

            }
        }
    });
}


$(function () {
    $("#formTradeStock").validate({
        rules: {
            deUtente: {
                required: true,
                isSelected: "0"
            },

            paraUtente: {
                required: true,
                isSelected: "0"
            },

            medicamento: {
                required: true,
                isSelected: "0"
            },

            qtd: {
                required: true,
                min: 0
            }
        },
            
        submitHandler: validateForm
    });
});