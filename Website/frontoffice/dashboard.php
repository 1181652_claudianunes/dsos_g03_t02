<!DOCTYPE html>
<html>

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>SB Admin 2 - Dashboard</title>

  <!-- Custom styles for this template-->
  <link href="../styles/fontawesome-free/all.css" rel="stylesheet">
  <link href="../styles/font/nunito.css" rel="stylesheet">
  <link href="../styles/sb-admin-2.css" rel="stylesheet">
  <link href="../styles/datatables/dataTables.bootstrap4.css" rel="stylesheet">

  <link href="../styles/style.css" rel="stylesheet">
</head>

<body>

<!-- Page Heading -->
<div class="container-fluid h-100perc">
  <div class="row mt-4">
    <div class="col-6">
        <div class="card shadow mb-4">
            <div class="card-header bg-warning py-3">
              <h6 class="m-0 font-weight-bold text-white">Por Executar</h6>
            </div>

            <div class="card-body">
              <table id="tblPorExecutar" class="dataTable table table-striped table-bordered" width="100%" height="100%"></table>
            </div>
        </div>
    </div>

    <div class="col-6">
        <div class="card shadow mb-4">
            <div class="card-header bg-success py-3">
              <h6 class="m-0 font-weight-bold text-white">Executado</h6>
            </div>
        
            <div class="card-body">
              <table id="tblExecutado" class="dataTable table table-striped table-bordered" width="100%" height="100%"></table>
            </div>
        </div>
   </div>

<!-- Executar Terapêutica-->
  <div class="modal fade" id="execTerapeutica" tabindex="-1" role="dialog" aria-labelledby="execTerapeutica" aria-hidden="true" width="100%" height="100%">
    <div class="modal-dialog" role="document">
      <div class="modal-content">

          <div class="modal-header">
            <h5 class="modal-title" id="executarTerapeuticaCabecalho"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">x</span>
            </button>
          </div>
          
          <div class="modal-body">
            <input type="hidden" id="hfRowId" value=""/>
            <div class="form-row">
               <div class="form-group col-3">
                  <label for="inlineFormInput" class="col-form-label">Data/Hora:</label>
                  <input type="datetime-local" class="form-control" id="execDataHora" name="data">
               </div>
            </div>
          </div>
          
          <div class="modal-footer">
            <div class='btn-group' role='group' aria-label='Basic example'>
                 <button class="btn btn-primary" type="submit" value="Executar" id="executar" name="submit">
                    <i class='far fa-check-circle'></i>
                 </button>
                 <button class="btn btn-secondary" type="button" data-dismiss="modal">
                    <i class="fas fa-chevron-left"></i>
                 </button>
            </div>
          </div>
      </div>
    </div>
  </div>
  </div>
</div>

    <script src="../scripts/jquery/jquery.js"></script>
    <script src="../scripts/bootstrap/js/bootstrap.bundle.js"></script>
    <script src="../scripts/jquery-easing/jquery.easing.js"></script>
    <script src="../scripts/sb-admin-2.js"></script>

    <script src="../scripts/jquery/jquery.validate.js"></script>
    <script src="../scripts/jquery/jquery.dataTables.js"></script>
    <script src="../scripts/datatables/dataTables.bootstrap4.js"></script>
    <script src="../scripts/chart/Chart.js"></script>

    <script src="dashboard.js"></script>
    <script src="../scripts/geral.js"></script>

</body>
</html>