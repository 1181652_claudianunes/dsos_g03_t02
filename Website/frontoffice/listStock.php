<!DOCTYPE html>
<html>

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>SB Admin 2 - Stock</title>

  <!-- Custom styles for this template-->
  <link href="../styles/fontawesome-free/all.css" rel="stylesheet">
  <link href="../styles/font/nunito.css" rel="stylesheet">
  <link href="../styles/sb-admin-2.css" rel="stylesheet">
  <link href="../styles/datatables/dataTables.bootstrap4.css" rel="stylesheet">

  <link href="../styles/style.css" rel="stylesheet">

</head>
<body>

  <input type="hidden" id="hfModoVis" value="1" />

<!-- Page Heading -->
  <div class="container-fluid h-100perc mt-3">

	  <div class="table-responsive">
		  <table id="tblStock" class="dataTable table table-striped table-bordered" width="100%" height="100%" cellspacing="0">
		  </table>
    </div>

    <div class="modal fade" id="tradeStock" tabindex="-1" role="dialog" aria-labelledby="tradeStock" aria-hidden="true" width="100%" height="100%">
    <div class="modal-dialog" role="document">
      <div class="modal-content">

          <div class="modal-header">
            <h5 class="modal-title" id="tradeStockCabecalho"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">x</span>
            </button>
          </div>
          
          <form action="../method/tradeStock.php" method="POST" id="formTradeStock">

            <div class="modal-body">
              <input type="hidden" id="hfRowId" value=""/>

              <div class="form-row">
                <div class="form-group col-6">
                      <label for="inlineFormInput" class="col-form-label">De:</label>
                      <select for="inlineFormInput" class="custom-select" id="cbDeUtente" name="deUtente">
                        <option value="0">...</option>
                      </select> 
                </div>
                <div class="form-group col-6">
                      <label for="inlineFormInput" class="col-form-label">Para:</label>
                      <select for="inlineFormInput" class="custom-select" id="cbParaUtente" name="paraUtente">
                        <option value="0">...</option>
                      </select> 
                </div>
              </div>

              <div class="form-row">
                <div class="form-group col-6">
                    <label for="inlineFormInput" class="col-form-label">Medicamento:</label>
                    <select for="inlineFormInput" class="custom-select" id="cbMedicamento" name="medicamento" disabled>
                        <option value="0">...</option>
                      </select> 
                </div>
                <div class="form-group col-6">
                    <label for="inlineFormInput" class="col-form-label">Quantidade:</label>
                    <input type="number" class="form-control" id="txtQuantidade" name="qtd" disabled>
                </div>
              </div>
          </div>
          
          <div class="modal-footer">
            <div class='btn-group' role='group' aria-label='Basic example'>
              <button class="btn btn-primary" type="submit" value="Executar" id="executar" name="submit">
                <i class='fas fa-exchange-alt'></i>
              </button>
              <button class="btn btn-secondary" type="button" data-dismiss="modal">
                <i class="fas fa-chevron-left"></i>
              </button>
            </div>
          </div>

        </form>

      </div>
    </div>
  </div>

<!-- Bootstrap core JavaScript-->
    <script src="../scripts/jquery/jquery.js"></script>
    <script src="../scripts/bootstrap/js/bootstrap.bundle.js"></script>
    <script src="../scripts/jquery-easing/jquery.easing.js"></script>
    <script src="../scripts/sb-admin-2.js"></script>


    <script src="../scripts/jquery/jquery.validate.js"></script>
    <script src="../scripts/jquery/jquery.dataTables.js"></script>
    <script src="../scripts/datatables/dataTables.bootstrap4.js"></script>


    <script src="listStock.js"></script>
    <script src="../scripts/geral.js"></script>

	
</body>
</html>