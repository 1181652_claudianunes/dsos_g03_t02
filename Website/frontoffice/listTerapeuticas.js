$(document).ready(function () {
    var table = $('#tblTerapeuticas').DataTable({
        ajax: {
            url: "../method/getTerapeuticas.php",
            dataSrc: ""
        },
        language: {
            url: "../scripts/datatables/lang_pt.json"
        },
        columns: [
            { data: 'utente' },
            { data: 'medicamento' },
            { data: 'quantidade' },
            { data: 'dataHora' },
            { data: 'dataExecucao' },
            { data: 'agendamento' },
            { data: 'periodicidade' },
            { data: 'fim' }
        ],
        select: {
            style: 'single'
        },
        scrollY: '90vh',
        scrollCollapse: true,
        lengthChange: false,
        dom: "<'row'<'col-md-6 d-flex'<'toolbar'>><'col-md-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
        fnInitComplete: function () {
            $('div.toolbar').html("<div class='btn-group' role='group' aria-label='Basic example'>" + 
            "<button id='btnNew' type='button' class='btn btn-light' onclick='createTerapeutica()'><i class='far fa-file'></i></button>" + 
            "<button id='btnRegist' type='button' class='btn btn-light' onclick='execTerapeutica()'><i class='far fa-check-circle'></i></button>" + 
            "<button type='button' class='btn btn-light' onclick='deleteTerapeutica()'><i class='far fa-trash-alt'></button>" + 
            "</div>");
        }
    });

    $('#tblTerapeuticas tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });
});

function bindForm() {
    var loadUtente = $.ajax({
        url: '../method/getUtentes.php',
        success: function (data) {
            var retJson = JSON.parse(data);
            var combo = $("#utente");
            combo.children('option:not(:first)').remove();
            $.each(retJson, function (index, obj) {
                combo.append("<option value='" + obj.idUtente + "'>" + obj.nome + "</option>");
            });
        }
    });

    var loadMedicamento = $.ajax({
        url: '../method/getMedicamentos.php',
        success: function (data) {
            var retJson = JSON.parse(data);
            var combo = $("#medicamento");
            combo.children('option:not(:first)').remove();
            $.each(retJson, function (index, obj) {
                combo.append("<option value='" + obj.idMedicamento + "'>" + obj.nome + " (" + obj.marca + ")" + "</option>");
            });
        }
    });

    var loadAgendamento = $.ajax({
        url: '../method/getTiposAgendamento.php',
        success: function (data) {
            var retJson = JSON.parse(data);
            var combo = $("#agendamento");
            combo.children('option:not(:first)').remove();
            $.each(retJson, function (index, obj) {
                combo.append("<option value='" + obj.idAgendamentoTipo + "'>" + obj.descricao + "</option>");
            });
        }
    });

    $.when(loadUtente, loadMedicamento, loadAgendamento).then(function (e) {
        return;
    });
}

//MODAL - CRIAÇÃO DE TERAPEUTICAS
function createTerapeutica(e) {
    document.getElementById("hfRowId").value = 0;
    $("#utente").val("");
    $("#medicamento").val("");
    $("#quantidade").val("");
    $("#dataHora").val("");
    $("#agendamento").val("");
    $("#periodo").val("");
    $("#fim").val("");

    bindForm();

    var modal = $('#editTerapeutica');
    modal.modal('show');
    modal.find('.modal-title').text('Nova Terapêutica');
}

//MODAL - EXECUÇÃO DE TERAPÊUTICAS
function execTerapeutica(e) {
    var table = $('#tblTerapeuticas').DataTable();
    if (!table.rows('.selected').any()) {
        alert("Selecione uma linha.");
        return;
    }

    var obj = table.rows('.selected').data()[0];
    document.getElementById("hfRowId").value = obj.idAgendamento;

    let datahora = new Date().toISOString().substr(0, 19);
    document.querySelector("#execDataHora").value = datahora;

    $("#execUtente").val(obj.utente);
    $("#execMedicamento").val(obj.medicamento);
    $("#execQuantidade").val(obj.quantidade);

    var modal = $('#execTerapeutica');
    modal.modal('show');
    modal.find('.modal-title').text('Execução de Terapêutica');
}

$("#executar").click(function () {
    var novadata = new Date($('#execDataHora').val());
    var strdata = formatDate(novadata);
    $.ajax({
        url: "../method/execTerapeutica.php",
        type: "POST",
        data: {
            idKey: document.getElementById("hfRowId").value,
            data: strdata,
            gerarmovimento: '1'
        },
        cache: false,
        success: function (dataResult) {
            if (dataResult == "true") {
                var table = $('#tblTerapeuticas').DataTable();
                table.ajax.reload();

                var modal = $('#execTerapeutica');
                modal.modal('hide');
            } else {
                alert(dataResult)

            }
        }
    });
});

$("#nExecutar").click(function () {
    $.ajax({
        url: "../method/execTerapeutica.php",
        type: "POST",
        data: {
            idKey: document.getElementById("hfRowId").value
        },
        cache: false,
        success: function (dataResult) {
            if (dataResult == "true") {
                var table = $('#tblTerapeuticas').DataTable();
                table.ajax.reload();

                var modal = $('#execTerapeutica');
                modal.modal('hide');
            } else {
                alert(dataResult)

            }
        }
    });
});

$("#agendamento").on("change", function(){
    $("#fim").val("");
    if(this.value == "2"){
        $("#fim").prop("disabled", false);
    }else{
        $("#fim").prop("disabled", true);    
    }
})

//ELIMINAÇÃO DE TERAPÊUTICAS
function deleteTerapeutica() {
    var table = $('#tblTerapeuticas').DataTable();
    if (!table.rows('.selected').any()) {
        alert("Selecione uma linha.");
        return;
    }

    var obj = table.rows('.selected').data()[0];
    document.getElementById("hfRowId").value = obj.idAgendamento;
    $.ajax({
        url: "../method/deleteTerapeutica.php",
        type: "POST",
        data: {
            idAgendamento: obj.idAgendamento
        },
        cache: false,
        success: function (dataResult) {
            if (dataResult === "true") {
                var table = $('#tblTerapeuticas').DataTable();
                table.ajax.reload();
            } else {
                alert("Erro!");
            }
        },
        fail: function (jqXHR, textStatus) {
            alert("Erro!");
        }

    });
}

//FORMULÁRIO - CRIAÇÃO DE UMA TERAPÊUTICA
function validateForm(form) {

    var novadata = new Date($('#dataHora').val());
    var strdata = formatDate(novadata);
    var strdataFim = "";
    if ($('#fim').val() != "") {
        novadata = new Date($('#fim').val());
        strdataFim = formatDate(novadata);
    }

    $.ajax({
        url: form.action,
        type: form.method,
        data: {
            idAgendamento: document.getElementById("hfRowId").value,
            idUtente: $('#utente').val(),
            idMedicamento: $('#medicamento').val(),
            qtd: $('#quantidade').val(),
            data: strdata,
            idTipoAgendamento: $('#agendamento').val(),
            periodicidade: $('#periodo').val(),
            dataFim: strdataFim
        },
        cache: false,
        success: function (dataResult) {
            if (dataResult == "true") {
                var table = $('#tblTerapeuticas').DataTable();
                table.ajax.reload();

                var modal = $('#editTerapeutica');
                modal.modal('hide');
            } else {
                alert(dataResult)

            }
        }
    });

}

//VALIDAÇÃO FORMULÁRIO - CRIAÇÃO DE TERAPÊUTICAS
$(function () {
    $("#formTerapeutica").validate({
        rules: {
            idUtente: {
                required: true,
                isSelected: "0"
            },

            idMedicamento: {
                required: true,
                isSelected: "0"
            },

            qtd: {
                required: true
            },

            data: {
                required: true
            },

            idTipoAgendamento: {
                required: true,
                isSelected: "0"
            },

            periodicidade: {
                required: true,
                min: 1
            },

            dataFim: {
                isRequiredIf: "2"
            },
        },
        messages: {
            dataFim: {
                isRequiredIf: "Preenchimento obrigatório."
            }
        },
            
        submitHandler: validateForm
    });
});

// VERIFICAR SE EXISTE ALGUM VALOR SELECIONADO
jQuery.validator.addMethod("isRequiredIf", function(value, element, arg) {
    if ($('#agendamento').val() !== arg){
        return true;
    }else{
        return (value != "");
    }
});