<!DOCTYPE html>
<html>

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>SB Admin 2 - Terapêuticas</title>

  <!-- Custom styles for this template-->
  <link href="../styles/fontawesome-free/all.css" rel="stylesheet">
  <link href="../styles/font/nunito.css" rel="stylesheet">
  <link href="../styles/sb-admin-2.css" rel="stylesheet">
  <link href="../styles/datatables/dataTables.bootstrap4.css" rel="stylesheet">

  <link href="../styles/style.css" rel="stylesheet">

</head>

<body>
   
<!-- Page Heading -->
  <div class="container-fluid h-100perc mt-3">      
    <div class="table-responsive">
      <table id="tblTerapeuticas" class="dataTable table table-striped table-bordered" width="100%" height="100%">
        <thead>
            <tr>
                <th class="sorting">Utente</th>
                <th class="sorting">Medicamento</th>
                <th class="sorting">Quantidade</th>
                <th class="sorting">Data/Hora</th>
                <th class="sorting">Data Execução</th>
                <th class="sorting">Agendamento</th>
                <th class="sorting">Periodicidade(h)</th>
                <th class="sorting">Fim da Terapêutica </th> 
            </tr>
        </thead>
      </table>
    </div>
  </div>

<!-- Nova Terapêutica-->
  <div class="modal fade" id="editTerapeutica" tabindex="-1" role="dialog" aria-labelledby="editTerapeutica" aria-hidden="true" width="100%" height="100%">
    <div class="modal-dialog" role="document">
      <div class="modal-content">

          <div class="modal-header">
            <h5 class="modal-title" id="editarTerapeuticaCabecalho"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">x</span>
            </button>
          </div>

          <form action="../method/createTerapeuticas.php" method="POST" id="formTerapeutica">
          
            <div class="modal-body">
              <input type="hidden" id="hfRowId" value=""/>
              <div class="form-row">
                <div class="form-group col-6">
                    <label for="inlineFormInput" class="col-form-label">Utente:</label>
                    <select for="inlineFormInput" class="custom-select" id="utente" name="idUtente">
                      <option value="0">...</option>
                    </select>
                </div>
                <div class="form-group col-6">
                    <label for="inlineFormInput" class="col-form-label">Tipo de Agendamento:</label>
                    <select for="inlineFormInput" class="custom-select" id="agendamento" name="idTipoAgendamento">
                      <option value="0">...</option>
                    </select>
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col-6">
                    <label for="inlineFormInput" class="col-form-label">Medicamento:</label>
                    <select for="inlineFormInput" class="custom-select" id="medicamento" name="idMedicamento">
                      <option value="0">...</option>
                    </select>
                </div>
                <div class="form-group col-6">
                    <label for="inlineFormInput" class="col-form-label">Quantidade:</label>
                    <input type="number" min="0" class="form-control" id="quantidade" name="qtd">
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col-4">
                    <label for="inlineFormInput" class="col-form-label">Data/Hora:</label>
                    <input type="datetime-local" class="form-control" id="dataHora" name="data">
                </div>
                <div class="form-group col-4">
                    <label for="inlineFormInput" class="col-form-label">Periodicidade(em horas):</label>
                    <input type="number" min="0" class="form-control" id="periodo" name="periodicidade">
                </div>
                <div class="form-group col-4">
                    <label for="inlineFormInput" class="col-form-label">Fim da Terapêutica:</label>
                    <input type="datetime-local" class="form-control" id="fim" name="dataFim" disabled>
                </div>
              </div>
          </div>
          
          <div class="modal-footer">
            <div class='btn-group' role='group' aria-label='Basic example'>
              <button class="btn btn-primary" type="submit" id="btnGravar" name="submit">
                <i class="far fa-save"></i>
              </button>
              <button class="btn btn-secondary" type="button" data-dismiss="modal">
                <i class="fas fa-chevron-left"></i>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

<!-- Executar Terapêutica-->
  <div class="modal fade" id="execTerapeutica" tabindex="-1" role="dialog" aria-labelledby="execTerapeutica" aria-hidden="true" width="100%" height="100%">
    <div class="modal-dialog" role="document">
      <div class="modal-content">

          <div class="modal-header">
            <h5 class="modal-title" id="executarTerapeuticaCabecalho"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">x</span>
            </button>
          </div>
          
            <div class="modal-body">
              <input type="hidden" id="hfRowId" value=""/>
              <div class="form-row">
                <div class="form-group col-3">
                    <label for="inlineFormInput" class="col-form-label">Data/Hora:</label>
                    <input type="datetime-local" class="form-control" id="execDataHora" name="data">
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col-6">
                    <label for="inlineFormInput" class="col-form-label">Utente:</label>
                    <input type="text" class="form-control" id="execUtente" name="utente" readonly>
                </div>
                <div class="form-group col-4">
                    <label for="inlineFormInput" class="col-form-label">Medicamento:</label>
                    <input type="text" class="form-control" id="execMedicamento" name="medicamento" readonly>
                </div>
                <div class="form-group col-2">
                    <label for="inlineFormInput" class="col-form-label">Quantidade:</label>
                    <input type="number" min="0" class="form-control" id="execQuantidade" name="qtd" readonly>
                </div>
              </div>
          </div>
          
          <div class="modal-footer">
            <div class='btn-group' role='group' aria-label='Basic example'>
              <button class="btn btn-primary" type="submit" value="Executar" id="executar" name="submit">
                <i class='far fa-check-circle'></i>
              </button>
              <button class="btn btn-primary" type="submit" value="Não Executar" id="nExecutar" name="submit">
                <i class="far fa-times-circle"></i>
              </button>
              <button class="btn btn-secondary" type="button" data-dismiss="modal">
                <i class="fas fa-chevron-left"></i>
              </button>
            </div>
          </div>
      </div>
    </div>
  </div>

<!-- Bootstrap core JavaScript-->
    <script src="../scripts/jquery/jquery.js"></script>
    <script src="../scripts/bootstrap/js/bootstrap.bundle.js"></script>
    <script src="../scripts/jquery-easing/jquery.easing.js"></script>
    <script src="../scripts/sb-admin-2.js"></script>

    <script src="../scripts/jquery/jquery.dataTables.js"></script>
    <script src="../scripts/jquery/jquery.validate.js"></script>
    <script src="../scripts/datatables/dataTables.bootstrap4.js"></script>

    <script src="listTerapeuticas.js"></script>
    <script src="../scripts/geral.js"></script>

</body>

</html>