<?php

include ('DatabaseConnection.php');

try{

    $_idUtenteDe = $_POST['idUtenteDe'];
    $_idUtentePara = $_POST['idUtentePara'];
    $_idMedicamento = $_POST['idMedicamento'];
    $_qtd = $_POST['qtd'];

    $_sql = "EXEC spTradeStock @idUtenteDe=?, @idUtentePara=?, @idMedicamento=?, @qtd=?";
    $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
    $_result = $_stmt->execute( array( $_idUtenteDe, $_idUtentePara, $_idMedicamento, $_qtd) );
    if ($_result === false) {
        die("false");
    }
    die("true"); 

} catch (Exception $e) {
    die($e->getMessage());
}

?>