<?php
include "validateSession.php";
include 'DatabaseConnection.php';
try {

    $_idLar = "NULL";
    if(isset($_SESSION["idLar"])){
        $_idLar = $_SESSION["idLar"];
    }

    if (isset($_POST["modoVis"])){
        $_modoVis = (int)$_POST["modoVis"];
    }

    if (isset($_POST["idAgendamento"])){
        $_idAgendamento = (int)$_POST["idAgendamento"];
    }

    if(!isset($_modoVis)){
        if(!isset($_idAgendamento)){
            $_sql = "EXEC spSelectAgendamentos @idLar=?";
            $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
            $_stmt->execute(array($_idLar));
        }else{
            $_sql = "EXEC spSelectAgendamentos @idLar=?, @idAgendamento=?";
            $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
            $_stmt->execute(array($_idLar, $_idAgendamento));
        }
    }else{
        if($_modoVis == "1"){
            $_sql = "EXEC spSelectAgendamentosPorExecutar @idLar=?";
            $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
            $_stmt->execute(array($_idLar));
        }else if($_modoVis == "2"){
            $_sql = "EXEC spSelectAgendamentosExecutado @idLar=?";
            $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
            $_stmt->execute(array($_idLar));
        }
    }
    //$_row = $_stmt->fetch( PDO::FETCH_ASSOC );
    
    while ( $_row = $_stmt->fetch( PDO::FETCH_ASSOC ) ){
        $myObj[] = $_row;
    }

    // VERIFICAR SE EXISTEM VALORES A NULL
    if (!isset($myObj)){
        $_json = "{}";
    }else{
        $_json = json_encode($myObj);
    }

    die($_json);

} catch (Exception $e) {
    die($e->getMessage());
}

?>