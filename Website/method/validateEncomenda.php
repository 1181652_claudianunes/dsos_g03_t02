<?php

include 'DatabaseConnection.php';
try{
    
    $_idEncomenda = (int)$_POST["idEncomenda"];
    $_valido = $_POST["valida"];

    //die($_idEncomenda . "|" . $_valido);
    $_sql = "EXEC spValidateEncomenda @idEncomenda=?, @valido=?";
    $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
    $_result = $_stmt->execute( array( $_idEncomenda, $_valido) );
    if ($_result === false) {
        die("false");
    }
    die("true");
} catch (Exception $e) {
    die($e->getMessage());
}

?>