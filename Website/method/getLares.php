<?php

include "validateSession.php";
include 'DatabaseConnection.php';

try {

    $_idLar = "NULL";
    if(isset($_SESSION["idLar"])){
        $_idLar = $_SESSION["idLar"];
    }
    
    $_sql = "EXEC spSelectLar @idLar=?";
    $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
    $_stmt->execute( array( $_idLar ) );   
    //$_row = $_stmt->fetch( PDO::FETCH_ASSOC );
    
    while ( $_row = $_stmt->fetch( PDO::FETCH_ASSOC ) ){
        $myObj[] = $_row;
    }

    // VERIFICAR SE EXISTEM VALORES A NULL
    if (!isset($myObj)){
        $_json = "{}";
    }else{
        $_json = json_encode($myObj);
    }

    die($_json);

    /*
        while ( $row = $stmt->fetch( PDO::FETCH_ASSOC ) ){
        print "$row[Name]\n";
        }
        echo "\n..\n";

        $row = $stmt->fetch( PDO::FETCH_BOTH, PDO::FETCH_ORI_FIRST );
        print_r($row);

        $row = $stmt->fetch( PDO::FETCH_ASSOC, PDO::FETCH_ORI_REL, 1 );
        print "$row[Name]\n";

        $row = $stmt->fetch( PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT );
        print "$row[1]\n";

        $row = $stmt->fetch( PDO::FETCH_NUM, PDO::FETCH_ORI_PRIOR );
        print "$row[1]..\n";

        $row = $stmt->fetch( PDO::FETCH_NUM, PDO::FETCH_ORI_ABS, 0 );
        print_r($row);

        $row = $stmt->fetch( PDO::FETCH_NUM, PDO::FETCH_ORI_LAST );
        print_r($row);
    */
} catch (Exception $e) {
    die($e->getMessage());
}

?>