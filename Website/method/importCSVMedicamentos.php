<?php  

include 'DatabaseConnection.php';

 if(!empty($_FILES["file"]["name"]))  
 {  

      $output = '';  
      // VERIFICAR EXTENÇÃO
      $_allowed_ext = array("csv");  
      $_extension = end(explode(".", $_FILES["file"]["name"]));        
      if(in_array($_extension, $_allowed_ext))  
      {  
           $_file_data = fopen($_FILES["file"]["tmp_name"], 'r');  
           fgetcsv($_file_data);  
           while($_row = fgetcsv($_file_data, 10000, ";"))  
           {  
                $_principiosativos = $_row[0];  
                $_medicamento = $_row[1];  
                $_posologia = $_row[2];  
                $_dose = $_row[3];  
                $_marca = $_row[6];  

                $_sql = "EXEC spImportCSVMedicamentos @principiosativos=?, @medicamento=?, @posologia=?, @doses=?, @marca=?";
                $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
                $_stmt->setAttribute(PDO::SQLSRV_ATTR_ENCODING, PDO::SQLSRV_ENCODING_SYSTEM);
                $_result = $_stmt->execute(array($_principiosativos, $_medicamento, $_posologia, $_dose, $_marca));
                if ($_result === false) {
                     $_cenas = $_stmt->errorInfo();
                    die(print_r($_cenas, true) . " @principiosativos='" . $_principiosativos . "', @medicamento='" . $_medicamento . "', @posologia='" . $_posologia ."', @doses='" . $_dose . "', @marca='" . $_marca . "'");
                }
           }
           die("true");
      }  
      else  
      {  
           die("Error1");  
      }  
 }  
 else  
 {  
      die("Error2");  
 }  
 ?>  