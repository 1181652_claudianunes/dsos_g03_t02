<?php

include "validateSession.php";
include "DatabaseConnection.php";

try{

    $_idUtilizador = "NULL";
    if(isset($_SESSION["userId"])){
        $_idUtilizador = $_SESSION["userId"];
    }

    $_idEncomenda = $_POST["idEncomenda"];
    $_data = $_POST["data"];
    $_idUtente = $_POST["idUtente"];
    $_encomendaLinhas = $_POST["encomendaLinhas"];

    
    if ($_idEncomenda > 0) {
        $_sql = "EXEC spUpdateEncomendas @idEncomenda=?, @data=?, @idUtente=?";
        $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
        $_result = $_stmt->execute( array( $_idEncomenda, $_data, $_idUtente ) );
        if ($_result === false) {
            die("false - 1");
        }
    }else{
        $_sql = "EXEC spCreateEncomendas @idUtilizador=?, @idUtente=?, @data=?";
        $_stmt = $conn->prepare( $_sql );         
        $_stmt->bindParam(1, $_idUtilizador); 
        $_stmt->bindParam(2, $_idUtente); 
        $_stmt->bindParam(3, $_data); 
        $_result = $_stmt->execute();
        if ($_result === false) {
            die("false - 1");
        }
        
        $_row = $_stmt->fetch( PDO::FETCH_ASSOC );
        $_idEncomenda = $_row['idEncomenda'];
    }

    // LIMPAR RELACIONAMENTO ENTRE ENCOMENDAS E ENCOMENDAS LINHAS
    $_sql = "EXEC spDeleteAllEncomendasLinhas @idEncomenda=?";
    $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
    $_result = $_stmt->execute( array( $_idEncomenda ) );
    if ($_result === false) {
        die("false - 2");
    }
    // CRIAR RELACIONAMENTO ENTRE ENCOMENDAS E ENCOMENDAS LINHAS
    $_objEL = json_decode($_encomendaLinhas);
    foreach ($_objEL as $_linha) {
        $_idMedicamento = $_linha->_idMedicamento;
        $_qtd = $_linha->_qtd;
		$_pCusto = $_linha->_pCusto;
		$_idUnidade = $_linha->_idUnidade;

        $_sql = "EXEC spCreateEncomendasLinhas @idEncomenda=?, @idMedicamento=?, @qtd=?, @pCusto=?, @idUnidade=?";
        $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
        $_result = $_stmt->execute( array( $_idEncomenda, $_idMedicamento, $_qtd, $_pCusto, $_idUnidade ) );
        if ($_result === false) {
            die("false - 3");
        }
    }

    die("true");
} catch(Exception $e) {
    die($e->getMessage());
}

?>