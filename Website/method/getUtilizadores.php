<?php

include "validateSession.php";
include 'DatabaseConnection.php';

try {
    $_idLar = "NULL";
    if(isset($_SESSION["idLar"])){
        $_idLar = $_SESSION["idLar"];
    }

    if (isset($_POST["idUtilizador"])){
        $_idUtilizador = (int)$_POST["idUtilizador"];
    }
    
    if(!isset($_idUtilizador)){
        $_sql = "EXEC spSelectUtilizador @idLar=?";
        $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
        $_stmt->execute(array( $_idLar )); 
    }else{
        $_sql = "EXEC spSelectUtilizador @idUtilizador=?";
        $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
        $_stmt->execute(array($_idUtilizador)); 
    }
      
    //$_row = $_stmt->fetch( PDO::FETCH_ASSOC );
    while ( $_row = $_stmt->fetch( PDO::FETCH_ASSOC ) ){
        $myObj[] = $_row;
    }

    // VERIFICAR SE EXISTEM VALORES A NULL
    if (!isset($myObj)){
        $_json = "{}";
    }else{
        $_json = json_encode($myObj);
    }

    die($_json);

} catch (Exception $e) {
    die($e->getMessage());
}

?>