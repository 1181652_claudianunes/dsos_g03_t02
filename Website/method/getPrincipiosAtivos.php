<?php

include 'DatabaseConnection.php';
try {

    // PROCURAR PRINCIPIOS ATIVOS PELO IDPRINCIPIOATIVO
    if (isset($_POST["idPrincipioAtivo"])){
        $_idPrincipioAtivo = (int)$_POST["idPrincipioAtivo"];
    }

    // PROCURAR PRINCIPIOS ATIVOS PELO IDMEDICAMENTO
    if (isset($_POST["idMedicamento"])){
        $_idMedicamento = (int)$_POST["idMedicamento"];
    }

    if(isset($_idPrincipioAtivo)) {
        $_sql = "EXEC spSelectPrincipiosAtivos @idPrincipioAtivo=?";
        $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
        $_stmt->execute(array($_idPrincipioAtivo));
    }elseif(isset($_idMedicamento)){
        $_sql = "EXEC spSelectMedicamentosPA @idMedicamento=?";
        $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
        $_stmt->execute(array($_idMedicamento));
    }else{
        $_sql = "EXEC spSelectPrincipiosAtivos";
        $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
        $_stmt->execute();
    }
    //$_row = $_stmt->fetch( PDO::FETCH_ASSOC );
    
    while ( $_row = $_stmt->fetch( PDO::FETCH_ASSOC ) ){
        $myObj[] = $_row;
    }

    // VERIFICAR SE EXISTEM VALORES A NULL
    if (!isset($myObj)){
        $_json = "{}";
    }else{
        $_json = json_encode($myObj);
    }

    die($_json);

    /*
        while ( $row = $stmt->fetch( PDO::FETCH_ASSOC ) ){
        print "$row[Name]\n";
        }
        echo "\n..\n";

        $row = $stmt->fetch( PDO::FETCH_BOTH, PDO::FETCH_ORI_FIRST );
        print_r($row);

        $row = $stmt->fetch( PDO::FETCH_ASSOC, PDO::FETCH_ORI_REL, 1 );
        print "$row[Name]\n";

        $row = $stmt->fetch( PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT );
        print "$row[1]\n";

        $row = $stmt->fetch( PDO::FETCH_NUM, PDO::FETCH_ORI_PRIOR );
        print "$row[1]..\n";

        $row = $stmt->fetch( PDO::FETCH_NUM, PDO::FETCH_ORI_ABS, 0 );
        print_r($row);

        $row = $stmt->fetch( PDO::FETCH_NUM, PDO::FETCH_ORI_LAST );
        print_r($row);
    */
} catch (Exception $e) {
    die($e->getMessage());
}

?>