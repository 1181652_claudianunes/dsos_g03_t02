<?php

include 'DataBaseConnection.php';
try {

    $_ModoVis = $_POST["modoVis"];

    if($_ModoVis == "1"){
        $_sql = "EXEC spSelectTop10MedicamentosMaisUsados";
        $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
        $_stmt->execute(); 
    }else if($_ModoVis == "2"){
        $_sql = "EXEC spspSelectTotalMedicamentosMaisUsados";
        $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
        $_stmt->execute(); 
    }
    
    while ( $_row = $_stmt->fetch( PDO::FETCH_ASSOC ) ){
        $myObj[] = $_row;
    }

    // VERIFICAR SE EXISTEM VALORES A NULL
    if (!isset($myObj)){
        $_json = "{}";
    }else{
        $_json = json_encode($myObj);
    }

    die($_json);

} catch (Exception $e) {
    echo $e->getMessage();
}

?>