<?php

include 'DatabaseConnection.php';
try{
    $_idAgendamento = (int)$_POST["idAgendamento"];
    $_sql = "EXEC spDeleteAgendamento @idAgendamento=?";
    $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
    $_result = $_stmt->execute( array( $_idAgendamento) );
    if ($_result === false) {
        die("false");
    }
    die("true");
} catch (Exception $e) {
    die($e->getMessage());
}

?>