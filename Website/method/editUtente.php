<?php

include "DatabaseConnection.php";

try{
	$_idUtente = (int)$_POST["idUtente"];
    $_nome = $_POST["nome"];
    $_morada = $_POST["morada"];
	$_contacto = $_POST["contacto"];
	$_idLar = $_POST["idLar"];

    if ($_idUtente > 0) {
        $_sql = "EXEC spUpdateUtente @idUtente=?, @nome=?, @morada=?, @contacto=?, @idLar=?";
        $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
        $_result = $_stmt->execute( array( $_idUtente, $_nome, $_morada, $_contacto, $_idLar) );
        if ($_result === false) {
            die("false");
        }
        die("true");
    }else{
        $_sql = "EXEC spCreateUtente ?, ?, ?, ?";
        $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
        $_result = $_stmt->execute( array( $_nome, $_morada, $_contacto, $_idLar) );
        if ($_result === false) {
            die("false");
        }
        die("true");
    }

} catch(Exception $e) {
    die($e->getMessage());
}

?>