<?php

include "DatabaseConnection.php";

try{
    $_idLar = (int)$_POST["idLar"];
    $_descricao = $_POST["descricao"];

    if ($_idLar > 0) {
        $_sql = "EXEC spUpdateLar @idLar=?, @descricao=?";
        $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
        $_result = $_stmt->execute( array( $_idLar, $_descricao) );
        if ($_result === false) {
            die("false");
        }
        die("true");
    }else{
        $_sql = "EXEC spCreateLar @descricao=?";
        $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
        $_result = $_stmt->execute( array( $_descricao) );
        if ($_result === false) {
            die("false");
        }
        die("true");
    }

} catch(Exception $e) {
    die($e->getMessage());
}

?>