<?php

include "validateSession.php";
include 'DataBaseConnection.php';
try {

    $_idLar = "NULL";
    if(isset($_SESSION["idLar"])){
        $_idLar = $_SESSION["idLar"];
    }

    $_ModoVis = $_POST["modoVis"];

    if (isset($_POST["idUtente"])){
        $_idUtente = (int)$_POST["idUtente"];
    }

    

    if($_ModoVis == "1"){
        $_sql = "EXEC spSelectStock @idLar=?";
        $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
        $_stmt->execute( array( $_idLar ) ); 
    }else if($_ModoVis == "2"){
        if (isset($_idUtente)){
            $_sql = "EXEC spSelectStockUtente @idLar=?, @idUtente=?";
            $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
            $_stmt->execute(array( $_idLar, $_idUtente )); 
        }else{
            $_sql = "EXEC spSelectStockUtente @idLar=?";
            $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
            $_stmt->execute(array( $_idLar )); 
        }
    }
    
    while ( $_row = $_stmt->fetch( PDO::FETCH_ASSOC ) ){
        $myObj[] = $_row;
    }

    // VERIFICAR SE EXISTEM VALORES A NULL
    if (!isset($myObj)){
        $_json = "{}";
    }else{
        $_json = json_encode($myObj);
    }

    die($_json);

} catch (Exception $e) {
    echo $e->getMessage();
}

?>