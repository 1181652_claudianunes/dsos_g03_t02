<?php  

include 'DatabaseConnection.php';

     $_file = file_get_contents('http://localhost:8091/appMedicamentos');
     $_json = json_decode($_file, true);

     foreach($_json['medicamentos'] as $_item){
          try{

               $_sql = "EXEC spImportWEBMedicamentos @medicamento=?, @posologia=?, @marca=?, @pvenda=?";
               $_stmt = $conn->prepare( $_sql );         
               $_stmt->bindParam(1, $_item['nome']); 
               $_stmt->bindParam(2, $_item['marca']); 
               $_stmt->bindParam(3, $_item['posologia']); 
               $_stmt->bindParam(4, $_item['pVenda']); 
               $_result = $_stmt->execute();
               
               $_row = $_stmt->fetch( PDO::FETCH_ASSOC );
               $_idMedicamento = $_row['idMedicamento'];

               //PRINCIPIOS ATIVOS
               if (isset($_item['principiosativos'])){
                    foreach($_item['principiosativos'] as $_pa){
                         $_sql = "EXEC spImportWEBMedicamentosPA @idMedicamento=?, @principioativo=?, @dose=?, @unidade=?";
                         $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
                         $_result = $_stmt->execute( array( $_idMedicamento, $_pa['principioativo'], $_pa['dose'], $_pa['unidade'] ) );

                    }
               }
               die("true");
          } catch(Exception $e) {
               die($e->getMessage());
          }
     }
 ?>  