<?php

include 'DatabaseConnection.php';
try{
    $_idMedicamento = (int)$_POST["idMedicamento"];
    $_sql = "EXEC spDeleteMedicamento @idMedicamento=?";
    $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
    $_result = $_stmt->execute( array( $_idMedicamento) );
    if ($_result === false) {
        die("false");
    }
    die("true");
} catch (Exception $e) {
    die($e->getMessage());
}

?>