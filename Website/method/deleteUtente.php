<?php

include 'DatabaseConnection.php';
try{
    $idUtente = (int)$_POST["idUtente"];
    $_sql = "EXEC spDeleteUtente @idUtente=?";
    $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
    $_result = $_stmt->execute( array( $idUtente) );
    if ($_result === false) {
        die("false");
    }
    die("true");
} catch (Exception $e) {
    die($e->getMessage());
}

?>