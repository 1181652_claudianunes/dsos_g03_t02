<?php
//file which will process the AJAX request and insert the user records in the database table.

include ('DatabaseConnection.php');
    try{

        $_idAgendamento = (int)$_POST["idAgendamento"];
        $_idMedicamento = $_POST['idMedicamento'];
        $_idUtente = $_POST['idUtente'];
        $_data = $_POST['data'];
        $_qtd = $_POST['qtd'];
        $_idAgendamentoTipo = $_POST['idTipoAgendamento'];
        $_Periodicidade = $_POST['periodicidade'];
        $_dataFim = $_POST['dataFim'];
    
        $_sql = "EXEC spCreateAgendamento @idMedicamento=?, @idUtente=?, @data=?, @qtd=?, @idAgendamentoTipo=?, @Periodicidade=?, @dataFim=?";
        $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
        $_result = $_stmt->execute( array( $_idMedicamento, $_idUtente, $_data, $_qtd, $_idAgendamentoTipo, $_Periodicidade, $_dataFim) );
          if ($_result === false) {
                die("false");
          }
          die("true");   
            
    } catch (Exception $e) {
        die($e->getMessage());
    }

?>