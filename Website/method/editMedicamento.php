<?php

include "DatabaseConnection.php";

try{
    $_idMedicamento = (int)$_POST["idMedicamento"];
    $_medicamento = $_POST["medicamento"];
    $_preco = $_POST["preco"];
    $_idMarca = $_POST["idMarca"];
    $_idPosologia = $_POST["idPosologia"];
    $_principioAtivo = $_POST["principioAtivo"];

    if ($_idMedicamento > 0) {
        $_sql = "EXEC spUpdateMedicamento @idMedicamento=?, @descricao=?, @preco=?, @idMarca=?, @idPosologia=?";
        $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
        $_result = $_stmt->execute( array( $_idMedicamento, $_medicamento, $_preco, $_idMarca, $_idPosologia ) );
        if ($_result === false) {
            die("false - 1");
        }
    }else{
        $_sql = "EXEC spCreateMedicamento @descricao=?, @preco=?, @idMarca=?, @idPosologia=?";
        $_stmt = $conn->prepare( $_sql );         
        $_stmt->bindParam(1, $_medicamento); 
        $_stmt->bindParam(2, $_preco); 
        $_stmt->bindParam(3, $_idMarca); 
        $_stmt->bindParam(4, $_idPosologia); 
        $_result = $_stmt->execute();
        if ($_result === false) {
            die("false - 1");
        }
        
        $_row = $_stmt->fetch( PDO::FETCH_ASSOC );
        $_idMedicamento = (int)$_row['idMedicamento'];
    }

    // LIMPAR RELACIONAMENTO ENTRE MEDICAMENTOS E PRINCIPIOS ATIVOS
    $_sql = "EXEC spDeleteAllMedicamentosPrincipiosAtivos @idMedicamento=?";
    $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
    $_result = $_stmt->execute( array( $_idMedicamento ) );
    if ($_result === false) {
        die("false - 2");
    }

    // CRIAR RELACIONAMENTO ENTRE MEDICAMENTOS E PRINCIPIOS ATIVOS
    $_objPA = json_decode($_principioAtivo);
    foreach ($_objPA as $_linha) {
        $_idPA = $_linha->_idprincipioativo;
        $_dose = $_linha->_dose;
        $_idUnidade = $_linha->_idunidade;

        $_sql = "EXEC spCreateMedicamentoPrincipiosAtivos @idMedicamento=?, @idPrincipioAtivo=?, @dose=?, @idUnidade=?";
        $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
        $_result = $_stmt->execute( array( $_idMedicamento, $_idPA, $_dose, $_idUnidade ) );
        if ($_result === false) {
            die("false - 3");
        }
    }

    die("true");
} catch(Exception $e) {
    die($e->getMessage());
}

?>