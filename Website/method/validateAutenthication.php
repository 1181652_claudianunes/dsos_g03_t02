<?php

include 'DatabaseConnection.php';

$_username = $_REQUEST["username"];
$_password = $_REQUEST["password"];

if($_username !== "" && $_password !== ""){
    try {
        $_sql = "EXEC spValidateAutenthication @username=?, @password=?";
        $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
        $_result = $_stmt->execute( array( $_username, $_password ) );
        if($_result === true){

            $_row_count = $_stmt->rowCount();
            if($_row_count === false){
                die("false");
            } elseif ($_row_count === 0) {
                die("false");
            } else {
                $_row = $_stmt->fetch( PDO::FETCH_ASSOC );
                session_start();
                $_SESSION["userId"] = $_row['idUtilizador'];
                $_SESSION["nome"] = $_row['nome'];
                $_SESSION["idTipo"] = $_row['idTipo'];
                $_SESSION["TipoUtilizador"] = $_row['TipoUtilizador'];
                $_SESSION["idLar"] = $_row['idLar'];
                $_SESSION["Lar"] = $_row['Lar'];
                //die( $_SESSION["userId"] . "|" . $_SESSION["nome"] . "|" . $_SESSION["idTipo"] . "|" . $_SESSION["TipoUtilizador"] . "|" . $_SESSION["idLar"] . "|" . $_SESSION["Lar"]);
                die("true");
            }
        }else{
            die("false");
        }
        

        /*
            while ( $row = $stmt->fetch( PDO::FETCH_ASSOC ) ){
            print "$row[Name]\n";
            }
            echo "\n..\n";

            $row = $stmt->fetch( PDO::FETCH_BOTH, PDO::FETCH_ORI_FIRST );
            print_r($row);

            $row = $stmt->fetch( PDO::FETCH_ASSOC, PDO::FETCH_ORI_REL, 1 );
            print "$row[Name]\n";

            $row = $stmt->fetch( PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT );
            print "$row[1]\n";

            $row = $stmt->fetch( PDO::FETCH_NUM, PDO::FETCH_ORI_PRIOR );
            print "$row[1]..\n";

            $row = $stmt->fetch( PDO::FETCH_NUM, PDO::FETCH_ORI_ABS, 0 );
            print_r($row);

            $row = $stmt->fetch( PDO::FETCH_NUM, PDO::FETCH_ORI_LAST );
            print_r($row);
        */
    } catch (Exception $e) {
        echo $e->getMessage();
    }

}else{
    die("false");
}

?>