<?php


if(!isset($_SESSION)){ 
    session_start(); 
} 
if( !isset($_SESSION['userId']) ) {
    header("Location: login.php");
    die();
}

?>