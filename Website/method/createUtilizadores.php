<?php
//file which will process the AJAX request and insert the user records in the database table.

include ('DatabaseConnection.php');
    try{

        $_idUtilizador = (int)$_POST["idUtilizador"];
        $_idTipoUtilizador = $_POST['tipo'];
        $_idLar = $_POST['idLar'];
        $_nome = $_POST['nome'];
        $_username = $_POST['username'];
        $_password = $_POST['password'];
    
        
        if ($_idUtilizador > 0) { 
            $_sql = "EXEC spUpdateUtilizador @idUtilizador=?, @idTipoUtilizador=?, @idLar=?, @nome=?, @username=?, @password=?";
            $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
            $_result = $_stmt->execute( array( $_idUtilizador, $_idTipoUtilizador, $_idLar, $_nome, $_username, $_password) );
            if ($_result === false) {
                die("false");
            }
            die("true");
         } else{
            $_sql = "EXEC spCreateUtilizador @idTipoUtilizador=?, @idLar=?, @nome=?, @username=?, @password=?";
            $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
            $_result = $_stmt->execute( array( $_idTipoUtilizador, $_idLar, $_nome, $_username, $_password) );
            if ($_result === false) {
                die("false");
            }
            die("true");   
         }
            
    } catch (Exception $e) {
        die($e->getMessage());
    }

?>