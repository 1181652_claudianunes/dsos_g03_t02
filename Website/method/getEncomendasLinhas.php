<?php

include 'DatabaseConnection.php';
try {

    if (isset($_POST["idEncomenda"])){
        $_idEncomenda = (int)$_POST["idEncomenda"];
    }

    if(isset($_idEncomenda)){
        $_sql = "EXEC spSelectEncomendasLinhas @idEncomenda=?";
        $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
        $_stmt->execute(array($_idEncomenda));
    }
    
    while ( $_row = $_stmt->fetch( PDO::FETCH_ASSOC ) ){
        $myObj[] = $_row;
    }

    // VERIFICAR SE OS VALORES RETORNADOS VEM VAZIOS
    if (!isset($myObj)){
        $_json = "{}";
    }else{
        $_json = json_encode($myObj);
    }

    die($_json);

} catch (Exception $e) {
    die($e->getMessage());
}

?>