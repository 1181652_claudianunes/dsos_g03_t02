<?php
//file which will process the AJAX request and insert the user records in the database table.

include ('DatabaseConnection.php');
    try{

        $_idKey = $_POST['idKey'];
        if (isset($_POST['data'])){ $_data = $_POST['data']; }
        if (isset($_POST['gerarmovimento'])){ $_gerarmovimento = $_POST['gerarmovimento']; }
    
        if(isset($_gerarmovimento)){
            $_sql = "EXEC spExecuteAgendamento @idAgendamento=?, @data=?, @gerarMovimentos=?";
            $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
            $_result = $_stmt->execute( array( $_idKey, $_data, $_gerarmovimento) );
              if ($_result === false) {
                    die("false");
              }
              die("true"); 
        }else{
           $_sql = "EXEC spExecuteAgendamento @idAgendamento=?";
            $_stmt = $conn->prepare( $_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 )); 
            $_result = $_stmt->execute(array( $_idKey));
              if ($_result === false) {
                    die("false");
              }
              die("true"); 
        }
    } catch (Exception $e) {
        die($e->getMessage());
    }

?>