USE [DSOSG3]
GO
/****** Object:  Table [dbo].[TblLares]    Script Date: 28/12/2019 18:39:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblLares](
	[idLar] [int] IDENTITY(1,1) NOT NULL,
	[descricao] [varchar](200) NOT NULL,
	[ativo] [bit] NOT NULL,
 CONSTRAINT [PK_TblLares] PRIMARY KEY CLUSTERED 
(
	[idLar] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblUtentes]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblUtentes](
	[idUtente] [int] IDENTITY(1,1) NOT NULL,
	[idLar] [int] NOT NULL,
	[nome] [varchar](200) NOT NULL,
	[morada] [varchar](500) NOT NULL,
	[contacto] [varchar](20) NOT NULL,
	[ativo] [bit] NOT NULL,
 CONSTRAINT [PK_TblUtentes] PRIMARY KEY CLUSTERED 
(
	[idUtente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[vwUtentes]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwUtentes]
AS
SELECT        dbo.TblUtentes.idUtente, dbo.TblUtentes.idLar, dbo.TblLares.descricao, dbo.TblUtentes.nome, dbo.TblUtentes.morada, dbo.TblUtentes.contacto
FROM            dbo.TblUtentes INNER JOIN
                         dbo.TblLares ON dbo.TblUtentes.idLar = dbo.TblLares.idLar
WHERE        (dbo.TblUtentes.ativo = 1)
GO
/****** Object:  Table [dbo].[TblMedicamentos]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblMedicamentos](
	[idMedicamento] [int] IDENTITY(1,1) NOT NULL,
	[nome] [varchar](100) NOT NULL,
	[idMarca] [int] NOT NULL,
	[idPosologia] [int] NOT NULL,
	[pVenda] [decimal](18, 4) NOT NULL,
	[ativo] [bit] NOT NULL,
 CONSTRAINT [PK_TblMedicamentos] PRIMARY KEY CLUSTERED 
(
	[idMedicamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblAgendamentos]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblAgendamentos](
	[idAgendamento] [int] IDENTITY(1,1) NOT NULL,
	[idMedicamento] [int] NOT NULL,
	[idUtente] [int] NOT NULL,
	[data] [datetime] NOT NULL,
	[qtd] [decimal](18, 4) NOT NULL,
	[dataFim] [datetime] NULL,
	[idAgendamentoTipo] [int] NOT NULL,
	[Periodicidade] [int] NULL,
	[ativo] [bit] NOT NULL,
	[dataProximaExecucao] [datetime] NULL,
 CONSTRAINT [PK_TblAgendamentos] PRIMARY KEY CLUSTERED 
(
	[idAgendamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblAgendamentosTipos]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblAgendamentosTipos](
	[idAgendamentoTipo] [int] IDENTITY(1,1) NOT NULL,
	[descricao] [varchar](200) NOT NULL,
 CONSTRAINT [PK_TblAgendamentosTipos] PRIMARY KEY CLUSTERED 
(
	[idAgendamentoTipo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[vwTerapeuticas]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vwTerapeuticas] AS
	SELECT
        idAgendamento AS idAgendamento,
		TblUtentes.idUtente AS idUtente,
        TblUtentes.nome AS utente,
		TblMedicamentos.idMedicamento AS idMedicamento,
		TblMedicamentos.nome AS medicamento,
        TblAgendamentos.[data] AS dataHora,
		TblAgendamentos.dataProximaExecucao AS dataExecucao,
        TblAgendamentos.qtd AS quantidade,
		TblAgendamentosTipos.idAgendamentoTipo AS idAgendamentoTipo,
        TblAgendamentosTipos.descricao AS agendamento,
        TblAgendamentos.Periodicidade AS periodicidade,
		TblAgendamentos.dataFim AS fim
    FROM
        TblAgendamentos INNER JOIN 
        TblUtentes ON TblAgendamentos.idUtente = TblUtentes.idUtente INNER JOIN
		TblMedicamentos ON TblAgendamentos.idMedicamento = TblMedicamentos.idMedicamento INNER JOIN
        TblAgendamentosTipos ON TblAgendamentos.idAgendamentoTipo = TblAgendamentosTipos.idAgendamentoTipo
	WHERE TblAgendamentos.ativo=1

GO
/****** Object:  Table [dbo].[TblMovimentos]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblMovimentos](
	[idMovimento] [int] IDENTITY(1,1) NOT NULL,
	[idMovimentoTipo] [int] NOT NULL,
	[idKey] [int] NULL,
	[idMedicamento] [int] NOT NULL,
	[data] [datetime] NOT NULL,
	[qtd] [decimal](18, 4) NOT NULL,
	[idUtente] [int] NULL,
 CONSTRAINT [PK_TblMovimentos] PRIMARY KEY CLUSTERED 
(
	[idMovimento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblMovimentosTipos]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblMovimentosTipos](
	[idMovimentoTipo] [int] IDENTITY(1,1) NOT NULL,
	[descricao] [varchar](200) NOT NULL,
 CONSTRAINT [PK_TblMovimentosTipos] PRIMARY KEY CLUSTERED 
(
	[idMovimentoTipo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[vwMovimentos]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vwMovimentos] AS
	SELECT
        TblMovimentos.idMovimento AS idMovimento,
		TblMovimentosTipos.idMovimentoTipo AS idMovimentoTipo,
        TblMovimentosTipos.descricao AS movimento,
		TblAgendamentos.idAgendamento AS idAgendamento,
		TblMovimentos.[data] AS 'data',
		TblAgendamentos.idUtente AS idUtente,
        TblUtentes.nome AS utente,
        TblMedicamentos.idMedicamento AS idMedicamento,
		TblMedicamentos.nome AS medicamento,
        TblMovimentos.qtd AS quantidade
    FROM
        TblMovimentos INNER JOIN 
        TblMovimentosTipos ON TblMovimentos.idMovimento = TblMovimentosTipos.idMovimentoTipo INNER JOIN
		TblAgendamentos ON TblMovimentos.idKey = TblAgendamentos.idAgendamento INNER JOIN
        TblUtentes ON TblAgendamentos.idAgendamento = TblUtentes.idUtente INNER JOIN
		TblMedicamentos ON TblMovimentos.idMovimento = TblMedicamentos.idMedicamento
GO
/****** Object:  Table [dbo].[TblUtilizadores]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblUtilizadores](
	[idUtilizador] [int] IDENTITY(1,1) NOT NULL,
	[idTipoUtilizador] [int] NOT NULL,
	[nome] [varchar](200) NOT NULL,
	[username] [varchar](20) NOT NULL,
	[password] [varchar](20) NOT NULL,
	[idLar] [int] NULL,
	[ativo] [bit] NOT NULL,
 CONSTRAINT [PK_TblUtilizadores] PRIMARY KEY CLUSTERED 
(
	[idUtilizador] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblTiposUtilizador]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblTiposUtilizador](
	[idTipoUtilizador] [int] IDENTITY(1,1) NOT NULL,
	[codigo] [varchar](20) NOT NULL,
	[descricao] [varchar](200) NOT NULL,
 CONSTRAINT [PK_TblTiposUtilizador] PRIMARY KEY CLUSTERED 
(
	[idTipoUtilizador] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[vwUtilizadores]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE VIEW [dbo].[vwUtilizadores] AS
	SELECT idUtilizador AS 'ID', TblTiposUtilizador.idTipoUtilizador as idTipo, TblTiposUtilizador.descricao as 'Tipo', nome AS 'Nome', username AS 'Username', [password] AS 'Password', TblLares.idLar as idLar, TblLares.descricao AS 'Lar'
	FROM TblUtilizadores
	INNER JOIN TblTiposUtilizador ON TblUtilizadores.idTipoUtilizador=TblTiposUtilizador.idTipoUtilizador
	INNER JOIN TblLares ON TblUtilizadores.idLar=TblLares.idLar
	WHERE TblUtilizadores.ativo=1

GO
/****** Object:  Table [dbo].[TblEncomendas]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblEncomendas](
	[idEncomenda] [int] IDENTITY(1,1) NOT NULL,
	[idUtilizador] [int] NOT NULL,
	[idUtente] [int] NOT NULL,
	[data] [datetime] NOT NULL,
	[valido] [bit] NULL,
	[ativo] [bit] NULL,
 CONSTRAINT [PK_TblEncomendas] PRIMARY KEY CLUSTERED 
(
	[idEncomenda] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblEncomendasLinhas]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblEncomendasLinhas](
	[idLinha] [int] IDENTITY(1,1) NOT NULL,
	[idEncomenda] [int] NOT NULL,
	[idMedicamento] [int] NOT NULL,
	[qtd] [decimal](18, 4) NOT NULL,
	[pCusto] [decimal](18, 4) NOT NULL,
	[idUnidade] [int] NULL,
 CONSTRAINT [PK_TblEncomendasLinhas] PRIMARY KEY CLUSTERED 
(
	[idLinha] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblMarcas]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblMarcas](
	[idMarca] [int] IDENTITY(1,1) NOT NULL,
	[descricao] [varchar](200) NOT NULL,
	[ativo] [bit] NOT NULL,
 CONSTRAINT [PK_TblMarcas] PRIMARY KEY CLUSTERED 
(
	[idMarca] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblMedicamentosPrincipiosAtivos]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblMedicamentosPrincipiosAtivos](
	[idMedicamento] [int] NOT NULL,
	[idPrincipioAtivo] [int] NOT NULL,
	[dose] [decimal](18, 2) NOT NULL,
	[idUnidade] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[idMedicamento] ASC,
	[idPrincipioAtivo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblPosologias]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblPosologias](
	[idPosologia] [int] IDENTITY(1,1) NOT NULL,
	[descricao] [varchar](200) NOT NULL,
 CONSTRAINT [PK_TblPosologias] PRIMARY KEY CLUSTERED 
(
	[idPosologia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblPrincipiosAtivos]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblPrincipiosAtivos](
	[idPrincipioAtivo] [int] IDENTITY(1,1) NOT NULL,
	[descricao] [varchar](200) NOT NULL,
 CONSTRAINT [PK_TblPrincipiosAtivos] PRIMARY KEY CLUSTERED 
(
	[idPrincipioAtivo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblUnidades]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblUnidades](
	[idUnidade] [int] IDENTITY(1,1) NOT NULL,
	[descricao] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[idUnidade] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TblAgendamentos] ADD  DEFAULT ((1)) FOR [ativo]
GO
ALTER TABLE [dbo].[TblEncomendas] ADD  CONSTRAINT [DF_TblEncomendas_data]  DEFAULT (getdate()) FOR [data]
GO
ALTER TABLE [dbo].[TblEncomendas] ADD  DEFAULT ((1)) FOR [ativo]
GO
ALTER TABLE [dbo].[TblLares] ADD  DEFAULT ((1)) FOR [ativo]
GO
ALTER TABLE [dbo].[TblMarcas] ADD  DEFAULT ((1)) FOR [ativo]
GO
ALTER TABLE [dbo].[TblMedicamentos] ADD  DEFAULT ((1)) FOR [ativo]
GO
ALTER TABLE [dbo].[TblUtentes] ADD  DEFAULT ((1)) FOR [ativo]
GO
ALTER TABLE [dbo].[TblUtilizadores] ADD  DEFAULT ((1)) FOR [ativo]
GO
ALTER TABLE [dbo].[TblAgendamentos]  WITH CHECK ADD  CONSTRAINT [FK_TblAgendamentos_TblAgendamentosTipos] FOREIGN KEY([idAgendamentoTipo])
REFERENCES [dbo].[TblAgendamentosTipos] ([idAgendamentoTipo])
GO
ALTER TABLE [dbo].[TblAgendamentos] CHECK CONSTRAINT [FK_TblAgendamentos_TblAgendamentosTipos]
GO
ALTER TABLE [dbo].[TblAgendamentos]  WITH CHECK ADD  CONSTRAINT [FK_TblAgendamentos_TblMedicamentos] FOREIGN KEY([idMedicamento])
REFERENCES [dbo].[TblMedicamentos] ([idMedicamento])
GO
ALTER TABLE [dbo].[TblAgendamentos] CHECK CONSTRAINT [FK_TblAgendamentos_TblMedicamentos]
GO
ALTER TABLE [dbo].[TblAgendamentos]  WITH CHECK ADD  CONSTRAINT [FK_TblAgendamentos_TblUtentes] FOREIGN KEY([idUtente])
REFERENCES [dbo].[TblUtentes] ([idUtente])
GO
ALTER TABLE [dbo].[TblAgendamentos] CHECK CONSTRAINT [FK_TblAgendamentos_TblUtentes]
GO
ALTER TABLE [dbo].[TblEncomendas]  WITH CHECK ADD  CONSTRAINT [FK_TblEncomendas_TblUtentes] FOREIGN KEY([idUtente])
REFERENCES [dbo].[TblUtentes] ([idUtente])
GO
ALTER TABLE [dbo].[TblEncomendas] CHECK CONSTRAINT [FK_TblEncomendas_TblUtentes]
GO
ALTER TABLE [dbo].[TblEncomendas]  WITH CHECK ADD  CONSTRAINT [FK_TblEncomendas_TblUtilizadores] FOREIGN KEY([idUtilizador])
REFERENCES [dbo].[TblUtilizadores] ([idUtilizador])
GO
ALTER TABLE [dbo].[TblEncomendas] CHECK CONSTRAINT [FK_TblEncomendas_TblUtilizadores]
GO
ALTER TABLE [dbo].[TblEncomendasLinhas]  WITH CHECK ADD  CONSTRAINT [FK_TblEncomendas_TblEncomendasLinhas] FOREIGN KEY([idEncomenda])
REFERENCES [dbo].[TblEncomendas] ([idEncomenda])
GO
ALTER TABLE [dbo].[TblEncomendasLinhas] CHECK CONSTRAINT [FK_TblEncomendas_TblEncomendasLinhas]
GO
ALTER TABLE [dbo].[TblEncomendasLinhas]  WITH CHECK ADD  CONSTRAINT [FK_TblEncomendasLinhas_TblMedicamentos] FOREIGN KEY([idMedicamento])
REFERENCES [dbo].[TblMedicamentos] ([idMedicamento])
GO
ALTER TABLE [dbo].[TblEncomendasLinhas] CHECK CONSTRAINT [FK_TblEncomendasLinhas_TblMedicamentos]
GO
ALTER TABLE [dbo].[TblEncomendasLinhas]  WITH CHECK ADD  CONSTRAINT [FK_TblEncomendasLinhas_TblUnidades] FOREIGN KEY([idUnidade])
REFERENCES [dbo].[TblUnidades] ([idUnidade])
GO
ALTER TABLE [dbo].[TblEncomendasLinhas] CHECK CONSTRAINT [FK_TblEncomendasLinhas_TblUnidades]
GO
ALTER TABLE [dbo].[TblMedicamentos]  WITH CHECK ADD  CONSTRAINT [FK_TblMedicamentos_TblMarcas] FOREIGN KEY([idMarca])
REFERENCES [dbo].[TblMarcas] ([idMarca])
GO
ALTER TABLE [dbo].[TblMedicamentos] CHECK CONSTRAINT [FK_TblMedicamentos_TblMarcas]
GO
ALTER TABLE [dbo].[TblMedicamentos]  WITH CHECK ADD  CONSTRAINT [FK_TblMedicamentos_TblPosologia] FOREIGN KEY([idPosologia])
REFERENCES [dbo].[TblPosologias] ([idPosologia])
GO
ALTER TABLE [dbo].[TblMedicamentos] CHECK CONSTRAINT [FK_TblMedicamentos_TblPosologia]
GO
ALTER TABLE [dbo].[TblMedicamentosPrincipiosAtivos]  WITH CHECK ADD  CONSTRAINT [FK_TblMedicamentosPrincipiosAtivos_TblUnidades] FOREIGN KEY([idUnidade])
REFERENCES [dbo].[TblUnidades] ([idUnidade])
GO
ALTER TABLE [dbo].[TblMedicamentosPrincipiosAtivos] CHECK CONSTRAINT [FK_TblMedicamentosPrincipiosAtivos_TblUnidades]
GO
ALTER TABLE [dbo].[TblMovimentos]  WITH CHECK ADD FOREIGN KEY([idUtente])
REFERENCES [dbo].[TblUtentes] ([idUtente])
GO
ALTER TABLE [dbo].[TblMovimentos]  WITH CHECK ADD  CONSTRAINT [FK_TblMovimentos_TblMedicamentos] FOREIGN KEY([idMedicamento])
REFERENCES [dbo].[TblMedicamentos] ([idMedicamento])
GO
ALTER TABLE [dbo].[TblMovimentos] CHECK CONSTRAINT [FK_TblMovimentos_TblMedicamentos]
GO
ALTER TABLE [dbo].[TblMovimentos]  WITH CHECK ADD  CONSTRAINT [FK_TblMovimentos_TblMovimentosTipos] FOREIGN KEY([idMovimentoTipo])
REFERENCES [dbo].[TblMovimentosTipos] ([idMovimentoTipo])
GO
ALTER TABLE [dbo].[TblMovimentos] CHECK CONSTRAINT [FK_TblMovimentos_TblMovimentosTipos]
GO
ALTER TABLE [dbo].[TblTiposUtilizador]  WITH CHECK ADD  CONSTRAINT [FK_TblTiposUtilizador_TblTiposUtilizador] FOREIGN KEY([idTipoUtilizador])
REFERENCES [dbo].[TblTiposUtilizador] ([idTipoUtilizador])
GO
ALTER TABLE [dbo].[TblTiposUtilizador] CHECK CONSTRAINT [FK_TblTiposUtilizador_TblTiposUtilizador]
GO
ALTER TABLE [dbo].[TblUtentes]  WITH CHECK ADD  CONSTRAINT [FK_TblUtentes_TblLares] FOREIGN KEY([idLar])
REFERENCES [dbo].[TblLares] ([idLar])
GO
ALTER TABLE [dbo].[TblUtentes] CHECK CONSTRAINT [FK_TblUtentes_TblLares]
GO
ALTER TABLE [dbo].[TblUtilizadores]  WITH CHECK ADD  CONSTRAINT [FK_TblUtilizadores_TblLares] FOREIGN KEY([idLar])
REFERENCES [dbo].[TblLares] ([idLar])
GO
ALTER TABLE [dbo].[TblUtilizadores] CHECK CONSTRAINT [FK_TblUtilizadores_TblLares]
GO
ALTER TABLE [dbo].[TblUtilizadores]  WITH CHECK ADD  CONSTRAINT [FK_TblUtilizadores_TblTiposUtilizador] FOREIGN KEY([idTipoUtilizador])
REFERENCES [dbo].[TblTiposUtilizador] ([idTipoUtilizador])
GO
ALTER TABLE [dbo].[TblUtilizadores] CHECK CONSTRAINT [FK_TblUtilizadores_TblTiposUtilizador]
GO
/****** Object:  StoredProcedure [dbo].[spCreateAgendamento]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




--STORED PROCEDURE - CREATE TERAPEUTICA--
CREATE PROCEDURE [dbo].[spCreateAgendamento]
	@idMedicamento INT,
	@idUtente INT,
	@data VARCHAR(19),
	@qtd DECIMAL(18,4),
	@idAgendamentoTipo INT,
	@Periodicidade INT,
	@dataFim VARCHAR(19) = NULL
AS
BEGIN
	DECLARE @FinalData DATETIME, @FinalDataFim DATETIME = NULL, @dataProximaExecucao DATETIME = NULL
	SET @FinalData = CONVERT(DATETIME, @data, 105)
	IF NOT NULLIF(@dataFim, '') IS NULL
		SET @FinalDataFim = CONVERT(DATETIME, @dataFim, 105)

		IF @idAgendamentoTipo IN (1,2) 
		BEGIN
			SET @dataProximaExecucao = DATEADD(hour, @Periodicidade, @FinalData) 
		END 


	INSERT INTO dbo.TblAgendamentos
		(idMedicamento, idUtente, [data], qtd, idAgendamentoTipo, Periodicidade, dataFim, dataProximaExecucao)
	VALUES 
		(@idMedicamento, @idUtente,  @FinalData, @qtd, @idAgendamentoTipo, @Periodicidade, @FinalDataFim, @dataProximaExecucao)

	SELECT @@IDENTITY AS 'idAgendamento'
END


GO
/****** Object:  StoredProcedure [dbo].[spCreateEncomendas]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spCreateEncomendas]
	@idUtilizador INT,
	@idUtente INT,
	@data VARCHAR(19)
AS
    SET NOCOUNT ON
	INSERT INTO	TblEncomendas (idUtilizador, idUtente, data)
	VALUES (@idUtilizador, @idUtente, CONVERT(DATETIME, @data, 105))

	SELECT @@IDENTITY AS 'idEncomenda'

GO
/****** Object:  StoredProcedure [dbo].[spCreateEncomendasLinhas]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCreateEncomendasLinhas]
	@idEncomenda INT,
	@idMedicamento INT,
	@qtd DECIMAL(18,4),
	@pCusto DECIMAL(18,4),
	@idUnidade INT
AS
	INSERT INTO TblEncomendasLinhas (idEncomenda, idMedicamento, qtd, pCusto, idUnidade)
	VALUES (@idEncomenda, @idMedicamento, @qtd, @pCusto, @idUnidade)
GO
/****** Object:  StoredProcedure [dbo].[spCreateLar]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCreateLar] 
    @descricao VARCHAR(200)
AS
    INSERT INTO TblLares (descricao)
    VALUES(@descricao)
    SELECT @@IDENTITY AS idLar

GO
/****** Object:  StoredProcedure [dbo].[spCreateMedicamento]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCreateMedicamento] 
    @descricao VARCHAR(200),
    @preco DECIMAL(18,2),
    @idMarca INT, 
    @idPosologia INT
AS
    SET NOCOUNT ON
    INSERT INTO TblMedicamentos(nome, pVenda, idMarca, idPosologia)
    VALUES(@descricao, @preco, @idMarca, @idPosologia)

    SELECT @@IDENTITY AS 'idMedicamento'
GO
/****** Object:  StoredProcedure [dbo].[spCreateMedicamentoPrincipiosAtivos]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCreateMedicamentoPrincipiosAtivos] 
    @idMedicamento INT,
    @idPrincipioAtivo INT,
    @dose DECIMAL(18,2),
    @idUnidade INT
AS
    INSERT INTO TblMedicamentosPrincipiosAtivos(idMedicamento, idPrincipioAtivo, dose, idUnidade)
    VALUES(@idMedicamento, @idPrincipioAtivo, @dose, @idUnidade)
GO
/****** Object:  StoredProcedure [dbo].[spCreateUtente]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCreateUtente]
	@nome VARCHAR(200),
	@morada VARCHAR(500),
	@contacto VARCHAR(20),
	@idLar INT
	
AS
BEGIN
	INSERT INTO TblUtentes 
		(nome, morada, contacto, idLar)
	VALUES 
		(@nome, @morada, @contacto, @idLar)
END
GO
/****** Object:  StoredProcedure [dbo].[spCreateUtilizador]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--STORED PROCEDURE - CREATE USER--
CREATE PROCEDURE [dbo].[spCreateUtilizador]
	@idUtilizador INT = NULL OUTPUT,
	@idTipoUtilizador INT,
	@idLar INT,
	@nome VARCHAR(200),
	@username VARCHAR(20),
	@password VARCHAR(20)
	
AS
BEGIN
	INSERT INTO dbo.TblUtilizadores 
		(idTipoUtilizador, idLar, nome, username, [password])
	VALUES 
		(@idTipoUtilizador, @idLar, @nome, @username, @password)
	IF @idUtilizador IS NULL SET @idUtilizador = SCOPE_IDENTITY();
END
GO
/****** Object:  StoredProcedure [dbo].[spDeleteAgendamento]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



--STORED PROCEDURE - DELETE TERAPEUTICA--
CREATE PROCEDURE [dbo].[spDeleteAgendamento]
	@idAgendamento int
AS
BEGIN
	UPDATE TblAgendamentos SET ativo = 0 WHERE idAgendamento = @idAgendamento
END
GO
/****** Object:  StoredProcedure [dbo].[spDeleteAllEncomendasLinhas]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spDeleteAllEncomendasLinhas]
	@idEncomenda INT
AS
	DELETE FROM TblEncomendasLinhas
	WHERE idEncomenda = @idEncomenda
GO
/****** Object:  StoredProcedure [dbo].[spDeleteAllMedicamentosPrincipiosAtivos]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spDeleteAllMedicamentosPrincipiosAtivos] 
    @idMedicamento INT
AS
    DELETE FROM TblMedicamentosPrincipiosAtivos
    WHERE idMedicamento = @idMedicamento
GO
/****** Object:  StoredProcedure [dbo].[spDeleteEncomenda]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spDeleteEncomenda]
	@idEncomenda INT
AS
    UPDATE TblEncomendas
    SET ativo = 0
    WHERE idEncomenda = @idEncomenda

GO
/****** Object:  StoredProcedure [dbo].[spDeleteLar]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spDeleteLar] 
    @idLar INT
AS
    UPDATE TblLares SET ativo = 0 WHERE idLar = @idLar
GO
/****** Object:  StoredProcedure [dbo].[spDeleteMedicamento]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[spDeleteMedicamento]
	@idMedicamento int
AS
BEGIN
	UPDATE TblMedicamentos SET ativo = 0 WHERE idMedicamento = @idMedicamento
END
GO
/****** Object:  StoredProcedure [dbo].[spDeleteUtente]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spDeleteUtente]
	@idUtente int
AS
BEGIN
	UPDATE TblUtentes SET ativo = 0 WHERE idUtente = @idUtente
END
GO
/****** Object:  StoredProcedure [dbo].[spDeleteUtilizador]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--STORED PROCEDURE - DELETE USER--
CREATE PROCEDURE [dbo].[spDeleteUtilizador]
	@idUtilizador int
AS
BEGIN
	UPDATE TblUtilizadores SET ativo = 0 WHERE idUtilizador = @idUtilizador
END
GO
/****** Object:  StoredProcedure [dbo].[spEstatisticaMedicamentosMaisUsados]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[spEstatisticaMedicamentosMaisUsados]
@top INT = 10
AS 
    SELECT TOP (@top) 
        TblMedicamentos.nome + ' (' + TblMarcas.descricao + ')' AS nome, 
        SUM((-TblMovimentos.qtd)) AS quantidade
    FROM 
        TblMedicamentos INNER JOIN
        TblMarcas ON TblMedicamentos.idMarca = TblMarcas.idMarca INNER JOIN
        TblMovimentos ON TblMedicamentos.idMedicamento = TblMovimentos.idMedicamento
    WHERE 
        TblMedicamentos.ativo = 1 AND 
		TblMovimentos.idMovimentoTipo = 2         
    GROUP BY  
        TblMedicamentos.nome,
        TblMarcas.descricao
	ORDER BY
		SUM((-TblMovimentos.qtd)) DESC

GO
/****** Object:  StoredProcedure [dbo].[spEstatisticaNumUtentePorLar]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spEstatisticaNumUtentePorLar]
AS 

    SELECT TblLares.idLar, TblLares.descricao as lar, Count(*) AS nrUtentes
    FROM 
        TblLares INNER JOIN
        TblUtentes ON TblLares.idLar = TblUtentes.idLar
    WHERE 
        TblLares.ativo = 1 AND
        TblUtentes.ativo = 1
    GROUP BY 
        TblLares.idLar, 
        TblLares.descricao
    HAVING
        Count(*) > 0
GO
/****** Object:  StoredProcedure [dbo].[spEstatisticaUtentesMedicamentosSOS]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spEstatisticaUtentesMedicamentosSOS]
@top INT = 10

AS 
    SELECT TOP (@top) TblUtentes.nome AS nome, COUNT(TblAgendamentos.idAgendamentoTipo) AS quantidade
    FROM 
        TblUtentes INNER JOIN
		TblMovimentos ON TblUtentes.idUtente = TblMovimentos.idUtente INNER JOIN
		TblAgendamentos ON TblMovimentos.idKey = TblAgendamentos.idAgendamento INNER JOIN
		TblMedicamentos ON TblAgendamentos.idMedicamento = TblMedicamentos.idMedicamento 
	WHERE 
        TblMedicamentos.ativo = 1 AND 
		TblMovimentos.idMovimentoTipo = 2  AND
		TblAgendamentos.idAgendamentoTipo = 3
    GROUP BY  
        TblUtentes.nome
	ORDER BY
		COUNT(TblAgendamentos.idAgendamentoTipo) DESC
GO
/****** Object:  StoredProcedure [dbo].[spExecuteAgendamento]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[spExecuteAgendamento]
	@idAgendamento INT,
	@data VARCHAR(19) = NULL,
	@gerarMovimentos BIT = 0
AS
BEGIN
	DECLARE @bitMovimentos BIT = 0
	DECLARE @idAgendamentoTipo INT
	SELECT @idAgendamentoTipo = idAgendamentoTipo FROM TblAgendamentos WHERE idAgendamento = @idAgendamento

	IF @idAgendamentoTipo in(1,2)
	BEGIN
		DECLARE @novadata DATETIME, @periodicidade INT
		IF @data IS NULL 
		BEGIN
			IF EXISTS(SELECT * FROM TblAgendamentos WHERE idAgendamento =@idAgendamento AND (GETDATE() <= datafim OR dataFim IS NULL))
			BEGIN
				UPDATE TblAgendamentos 
				SET dataProximaExecucao = CASE WHEN DATEADD(hour, Periodicidade, dataProximaExecucao) <= datafim or dataFim IS NULL THEN DATEADD(hour, Periodicidade, dataProximaExecucao) ELSE NULL END
				WHERE idAgendamento = @idAgendamento
				SET @bitMovimentos = 1
			END
            ELSE IF EXISTS(SELECT * FROM TblAgendamentos WHERE idAgendamento =@idAgendamento AND (GETDATE() > datafim))
            BEGIN
                UPDATE TblAgendamentos 
				SET dataProximaExecucao = NULL
				WHERE idAgendamento = @idAgendamento
            END
				
		END 
		ELSE 
		BEGIN
			IF EXISTS(SELECT * FROM TblAgendamentos WHERE idAgendamento =@idAgendamento AND (CONVERT(datetime, @data, 105) <= dataFim OR dataFim IS NULL))
			BEGIN
				UPDATE TblAgendamentos 
				SET dataProximaExecucao = CASE WHEN DATEADD(hour, Periodicidade, CONVERT(datetime, @data, 105))<= dataFim or dataFim IS NULL THEN DATEADD(hour, Periodicidade, CONVERT(datetime, @data, 105)) ELSE NULL END
				WHERE idAgendamento = @idAgendamento
				SET @bitMovimentos = 1
			END 
		END
	END
	ELSE
	BEGIN
		SET @bitMovimentos = 1
	END 

	IF (@gerarMovimentos = 1 AND @bitMovimentos = 1) 
		BEGIN
			INSERT INTO TblMovimentos
				(idMovimentoTipo, idKey, idMedicamento, [data], qtd, idUtente)
			SELECT 2, idAgendamento as idkey, idMedicamento, ISNULL(CONVERT(DATETIME, @data, 105), GETDATE()), -qtd, idUtente
			FROM TblAgendamentos
			WHERE idAgendamento = @idAgendamento
		END 
END



GO
/****** Object:  StoredProcedure [dbo].[spImportCSVMedicamentos]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




--STORED PROCEDURE - CREATE TERAPEUTICA--
CREATE PROCEDURE [dbo].[spImportCSVMedicamentos]
	@principiosativos VARCHAR(MAX),
	@medicamento VARCHAR(MAX),
	@posologia VARCHAR(MAX),
	@doses VARCHAR(MAX),
	@marca VARCHAR(MAX)
AS
BEGIN
    SET NOCOUNT ON
    
    IF OBJECT_ID('tempdb..##TmpPrincipiosAtivos') IS NOT NULL DROP TABLE ##TmpPrincipiosAtivos
    CREATE TABLE ##TmpPrincipiosAtivos(id INT, principioativo VARCHAR(max), dose DECIMAL(18,4), unidade VARCHAR(MAX) )
    IF ((NOT @principiosativos IS NULL) AND (NOT @doses IS NULL))
    BEGIN
        INSERT INTO ##TmpPrincipiosAtivos(id, principioativo)
        SELECT 
            ROW_NUMBER() OVER (ORDER BY (SELECT 1)), 
            RTRIM(LTRIM(value)) 
        FROM 
            STRING_SPLIT(@principiosativos, '+')

        UPDATE ##TmpPrincipiosAtivos
        SET
            dose = (
                SELECT TOP 1 descricao FROM (SELECT ROW_NUMBER() OVER (ORDER BY (SELECT 1)) as id,  RTRIM(LTRIM(value)) as descricao 
                FROM STRING_SPLIT(TmpDose.descricao, ' ')) tmp WHERE id = 1
            ),
            unidade = (
                SELECT TOP 1 descricao FROM (SELECT ROW_NUMBER() OVER (ORDER BY (SELECT 1)) as id,  RTRIM(LTRIM(value)) as descricao 
                FROM STRING_SPLIT(TmpDose.descricao, ' ')) tmp WHERE id = 2
            )
        FROM 
            ##TmpPrincipiosAtivos TmpPrincipiosAtivos INNER JOIN
            (
                SELECT ROW_NUMBER() OVER (ORDER BY (SELECT 1)) as id,  RTRIM(LTRIM(value)) as descricao 
                FROM STRING_SPLIT(@doses, '+')
            ) TmpDose ON TmpPrincipiosAtivos.id = TmpDose.id
        
        INSERT INTO TblPrincipiosAtivos(descricao)
        SELECT Tmp.principioativo 
        FROM ##TmpPrincipiosAtivos Tmp LEFT OUTER JOIN TblPrincipiosAtivos ON Tmp.principioativo = TblPrincipiosAtivos.descricao
        WHERE TblPrincipiosAtivos.idPrincipioAtivo IS NULL

        INSERT INTO TblUnidades(descricao)
        SELECT Tmp.unidade 
        FROM ##TmpPrincipiosAtivos Tmp LEFT OUTER JOIN TblUnidades ON Tmp.unidade = TblUnidades.descricao
        WHERE TblUnidades.idUnidade IS NULL
    END

    --CRIAR POSOLOGIA
    DECLARE @idPosologia INT
    IF NOT EXISTS( SELECT * FROM TblPosologias WHERE descricao = @posologia)
    BEGIN
        INSERT INTO TblPosologias(descricao)
        VALUES(@posologia)
        SET @idPosologia = @@IDENTITY
    END
    ELSE
    BEGIN
        SELECT TOP 1 @idPosologia = idPosologia FROM TblPosologias WHERE descricao = @posologia
    END

    -- CRIAR MARCAR
    DECLARE @idMarca INT
    IF NOT EXISTS( SELECT * FROM TblMarcas WHERE descricao = @marca)
    BEGIN
        INSERT INTO TblMarcas(descricao)
        VALUES(@marca)
        SET @idMarca = @@IDENTITY
    END
    ELSE
    BEGIN
        SELECT TOP 1 @idMarca = idMarca FROM TblMarcas WHERE descricao = @marca
    END

    -- CRIAR MEDICAMENTO
   DECLARE @idMedicamento INT
   IF NOT EXISTS( SELECT * FROM TblMedicamentos WHERE nome = @medicamento AND idMarca = @idMarca AND idPosologia = @idPosologia)
    BEGIN
        INSERT INTO TblMedicamentos(nome, idMarca, idPosologia, pVenda)
        VALUES(@medicamento, @idMarca, @idPosologia, 0)
        SET @idMedicamento = @@IDENTITY
    END
    ELSE
    BEGIN
        SELECT TOP 1 @idMedicamento = idMedicamento FROM TblMedicamentos WHERE nome = @medicamento AND idMarca = @idMarca AND idPosologia = @idPosologia
    END
   
    -- CRIAR RELACIONAMENTO ENTRE MEDICAMENTOS E PRINCIPIOS ATIVOS
    
    UPDATE TblMedicamentosPrincipiosAtivos
    SET TblMedicamentosPrincipiosAtivos.dose = Tmp.dose
    FROM 
        ##TmpPrincipiosAtivos Tmp INNER JOIN
        TblPrincipiosAtivos ON Tmp.principioativo = TblPrincipiosAtivos.descricao INNER JOIN
        TblUnidades ON Tmp.unidade = TblUnidades.descricao INNER JOIN
        TblMedicamentosPrincipiosAtivos ON
            TblMedicamentosPrincipiosAtivos.idMedicamento = @idMedicamento AND
            TblMedicamentosPrincipiosAtivos.idPrincipioAtivo = TblPrincipiosAtivos.idPrincipioAtivo AND
            TblMedicamentosPrincipiosAtivos.idUnidade = TblUnidades.idUnidade
    
    INSERT INTO TblMedicamentosPrincipiosAtivos(idMedicamento, idPrincipioAtivo, dose, idUnidade)
    SELECT
        @idMedicamento,
        TblPrincipiosAtivos.idPrincipioAtivo,
        Tmp.dose,
        TblUnidades.idUnidade
    FROM 
        ##TmpPrincipiosAtivos Tmp INNER JOIN
        TblPrincipiosAtivos ON Tmp.principioativo = TblPrincipiosAtivos.descricao INNER JOIN
        TblUnidades ON Tmp.unidade = TblUnidades.descricao LEFT OUTER JOIN
        TblMedicamentosPrincipiosAtivos ON
            TblMedicamentosPrincipiosAtivos.idMedicamento = @idMedicamento AND
            TblMedicamentosPrincipiosAtivos.idPrincipioAtivo = TblPrincipiosAtivos.idPrincipioAtivo AND
            TblMedicamentosPrincipiosAtivos.idUnidade = TblUnidades.idUnidade
    WHERE 
        TblMedicamentosPrincipiosAtivos.idMedicamento IS NULL

END




GO
/****** Object:  StoredProcedure [dbo].[spSelectAgendamentos]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spSelectAgendamentos]
    @idLar INT = NULL,
	@idAgendamento INT = NULL
AS 
    SELECT DISTINCT vwTerapeuticas.*
    FROM 
        vwTerapeuticas INNER JOIN
        TblUtentes ON vwTerapeuticas.idUtente = TblUtentes.idUtente
	WHERE 
    ((@idAgendamento IS NULL) OR (NOT @idAgendamento IS NULL AND vwTerapeuticas.idAgendamento = @idAgendamento)) AND 
    ((NULLIF(@idLar, 0)  IS NULL) OR (NOT NULLIF(@idLar, 0)  IS NULL AND TblUtentes.idLar = NULLIF(@idLar, 0) ))



GO
/****** Object:  StoredProcedure [dbo].[spSelectAgendamentosExecutado]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spSelectAgendamentosExecutado]
    @idLar INT = NULL
AS 

    SELECT TOP 10
        vwTerapeuticas.idAgendamento,
        vwTerapeuticas.idUtente,
        vwTerapeuticas.Utente,
        vwTerapeuticas.idMedicamento,
        vwTerapeuticas.medicamento + ' (' + TblMarcas.descricao + ')' AS medicamento,
        (-TblMovimentos.qtd) AS quantidade,
        TblMovimentos.data AS dataExecucao
    FROM 
        vwTerapeuticas INNER JOIN
        TblMovimentos ON 
            vwTerapeuticas.idAgendamento = TblMovimentos.idkey AND 
            vwTerapeuticas.idUtente = TblMovimentos.idUtente AND 
            TblMovimentos.idMovimentoTipo = 2 INNER JOIN
        TblMedicamentos ON vwTerapeuticas.idMedicamento = TblMedicamentos.idMedicamento INNER JOIN
        TblMarcas ON TblMedicamentos.idMarca = TblMarcas.idMarca INNER JOIN
        TblUtentes ON vwTerapeuticas.idUtente = TblUtentes.idUtente
    WHERE
        ((NULLIF(@idLar, 0)  IS NULL) OR (NOT NULLIF(@idLar, 0)  IS NULL AND TblUtentes.idLar = NULLIF(@idLar, 0) ))
    ORDER BY 
        TblMovimentos.data DESC


GO
/****** Object:  StoredProcedure [dbo].[spSelectAgendamentosPorExecutar]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spSelectAgendamentosPorExecutar]
    @idLar INT = NULL
AS 
    SELECT
        vwTerapeuticas.idAgendamento,
        vwTerapeuticas.idUtente,
        vwTerapeuticas.Utente,
        vwTerapeuticas.idMedicamento,
        vwTerapeuticas.medicamento + ' ('+ TblMarcas.descricao + ')' AS medicamento,
        vwTerapeuticas.quantidade,
        vwTerapeuticas.dataExecucao
    FROM
        vwTerapeuticas INNER JOIN
        TblMedicamentos ON vwTerapeuticas.idMedicamento = TblMedicamentos.idMedicamento INNER JOIN
        TblMarcas ON TblMedicamentos.idMarca = TblMarcas.idMarca INNER JOIN
        TblUtentes ON vwTerapeuticas.idUtente = TblUtentes.idUtente
    WHERE 
        idAgendamentoTipo IN(1,2) AND
        GETDATE() >= ISNULL(fim, GETDATE()) AND
        NOT dataExecucao IS NULL AND
        ((NULLIF(@idLar, 0)  IS NULL) OR (NOT NULLIF(@idLar, 0)  IS NULL AND TblUtentes.idLar = NULLIF(@idLar, 0) ))
    ORDER BY 
        dataExecucao


GO
/****** Object:  StoredProcedure [dbo].[spSelectEncomendas]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSelectEncomendas]
    @idLar INT = NULL
	AS
		SELECT 
            TblEncomendas.idEncomenda,
			'Enc.' + CONVERT(VARCHAR,YEAR(data)) + '/' + CONVERT(VARCHAR,idEncomenda) AS 'numEncomenda',
			TblEncomendas.data,
			TblUtentes.nome AS utente,
			TblUtilizadores.nome AS utilizador,
            TblEncomendas.valido,
            CASE TblEncomendas.valido WHEN 1 THEN 'Valido' WHEN 0 THEN 'Invalido' ELSE 'Sem estado' END descValido
		FROM 
			TblEncomendas INNER JOIN 
			TblUtentes ON TblEncomendas.idUtente = TblUtentes.idUtente INNER JOIN
			TblUtilizadores ON TblEncomendas.idUtilizador = TblUtilizadores.idUtilizador
        WHERE
            TblEncomendas.ativo = 1 AND
            ((NULLIF(@idLar, 0)  IS NULL) OR (NOT NULLIF(@idLar, 0)  IS NULL AND TblUtentes.idLar = NULLIF(@idLar, 0) ))






GO
/****** Object:  StoredProcedure [dbo].[spSelectEncomendasLinhas]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSelectEncomendasLinhas]
    @idEncomenda INT
	AS
        SELECT
            TblMedicamentos.idMedicamento,
            TblMedicamentos.nome as medicamento,
            TblEncomendasLinhas.qtd,
            TblEncomendasLinhas.pCusto,
            TblUnidades.idUnidade,
            TblUnidades.descricao as unidade
        FROM 
            TblEncomendasLinhas INNER JOIN
            TblMedicamentos ON TblEncomendasLinhas.idMedicamento = TblMedicamentos.idMedicamento LEFT OUTER JOIN
            TblUnidades ON TblEncomendasLinhas.idUnidade = TblUnidades.idUnidade
        WHERE
            TblEncomendasLinhas.idEncomenda = @idEncomenda

GO
/****** Object:  StoredProcedure [dbo].[spSelectLar]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSelectLar] 
    @idLar INT = NULL
AS
    SELECT idLar, descricao 
    FROM TblLares 
    WHERE 
        ((NULLIF(@idLar, 0) IS NULL AND Ativo = 1) OR (NOT NULLIF(@idLar, 0)  IS NULL AND idLar = NULLIF(@idLar, 0) ))




GO
/****** Object:  StoredProcedure [dbo].[spSelectMarca]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[spSelectMarca] 
AS
    SELECT idMarca, descricao FROM TblMarcas WHERE ativo = 1

GO
/****** Object:  StoredProcedure [dbo].[spSelectMedicamentos]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSelectMedicamentos] 
AS
    SELECT
        TblMedicamentos.idMedicamento,
        TblMedicamentos.nome,
        TblMedicamentos.idMarca,
        TblMarcas.descricao as marca,
        TblMedicamentos.idPosologia,
        TblPosologias.descricao as posologia,
        TblMedicamentos.pVenda
    FROM
        TblMedicamentos INNER JOIN 
        TblMarcas ON TblMedicamentos.idMarca = TblMarcas.idMarca INNER JOIN
        TblPosologias ON TblMedicamentos.idPosologia = TblPosologias.idPosologia
    WHERE
        TblMedicamentos.ativo = 1



GO
/****** Object:  StoredProcedure [dbo].[spSelectMedicamentosPA]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSelectMedicamentosPA] 
    @idMedicamento INT
AS
    SELECT
        TblPrincipiosAtivos.idPrincipioAtivo,
        TblPrincipiosAtivos.descricao AS principioativo,
        TblMedicamentosPrincipiosAtivos.dose,
        TblMedicamentosPrincipiosAtivos.idUnidade,
        TblUnidades.descricao as unidade
    FROM
        TblPrincipiosAtivos INNER JOIN
        TblMedicamentosPrincipiosAtivos ON TblPrincipiosAtivos.idPrincipioAtivo = TblMedicamentosPrincipiosAtivos.idPrincipioAtivo INNER JOIN
        TblUnidades ON TblMedicamentosPrincipiosAtivos.idUnidade = TblUnidades.idUnidade
    WHERE
        TblMedicamentosPrincipiosAtivos.idMedicamento = @idMedicamento



GO
/****** Object:  StoredProcedure [dbo].[spSelectPosologia]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSelectPosologia] 
AS
    SELECT TblPosologias.idPosologia, TblPosologias.descricao
    FROM TblPosologias

GO
/****** Object:  StoredProcedure [dbo].[spSelectPrincipiosAtivos]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSelectPrincipiosAtivos] 
    @idPrincipioAtivo INT = NULL
AS
    SELECT
        TblPrincipiosAtivos.idPrincipioAtivo,
        TblPrincipiosAtivos.descricao
    FROM 
        TblPrincipiosAtivos 
    WHERE
        (@idPrincipioAtivo IS NULL) OR (NOT @idPrincipioAtivo IS NULL AND TblPrincipiosAtivos.idPrincipioAtivo = @idPrincipioAtivo)

GO
/****** Object:  StoredProcedure [dbo].[spSelectStock]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSelectStock] 
    @idLar INT = NULL
AS
    SELECT
        TblMedicamentos.idMedicamento,
		TblMedicamentos.nome + ' (' + TblMarcas.descricao + ')' as medicamento, 
        TblMarcas.idMarca,
        TblMarcas.descricao as marca,
		SUM(qtd) AS qtd
	FROM
		TblMovimentos INNER JOIN 
		TblMedicamentos ON tblMovimentos.idMedicamento = TblMedicamentos.idMedicamento INNER JOIN
        TblMarcas ON TblMedicamentos.idMarca = TblMarcas.idMarca INNER JOIN
        TblUtentes ON TblMovimentos.idUtente = TblUtentes.idUtente
    WHERE
        (NULLIF(@idLar, 0) IS NULL) OR (NOT NULLIF(@idLar, 0) IS NULL AND TblUtentes.idLar = NULLIF(@idLar, 0))
	GROUP BY
		TblMedicamentos.idMedicamento,
		TblMedicamentos.nome, 
        TblMarcas.idMarca,
        TblMarcas.descricao
    HAVING 
        SUM(qtd) <> 0







GO
/****** Object:  StoredProcedure [dbo].[spSelectStockUtente]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSelectStockUtente] 
    @idLar INT = NULL,
    @idUtente INT = NULL
AS
    SELECT
        TblMovimentos.idUtente,
        TblUtentes.nome as utente,
        TblMovimentos.idMedicamento,
		TblMedicamentos.nome + ' (' + TblMarcas.descricao + ')' as medicamento, 
        TblMarcas.idMarca,
        TblMarcas.descricao as marca,
		SUM(qtd) AS qtd
	FROM
		TblMovimentos INNER JOIN 
		TblMedicamentos ON tblMovimentos.idMedicamento = TblMedicamentos.idMedicamento INNER JOIN
        TblMarcas ON TblMedicamentos.idMarca = TblMarcas.idMarca INNER JOIN
        TblUtentes ON TblMovimentos.idUtente = TblUtentes.idUtente
    WHERE
        ((NULLIF(@idLar, 0) IS NULL) OR (NOT NULLIF(@idLar, 0) IS NULL AND TblUtentes.idLar = NULLIF(@idLar, 0))) AND
        ((@idUtente IS NULL) OR (NOT @idUtente IS NULL AND TblMovimentos.idUtente = @idUtente))
	GROUP BY
		TblMovimentos.idUtente,
        TblUtentes.nome,
        TblMovimentos.idMedicamento,
		TblMedicamentos.nome,
        TblMarcas.idMarca,
        TblMarcas.descricao
    HAVING
        SUM(qtd) <> 0







GO
/****** Object:  StoredProcedure [dbo].[spSelectTipoUtilizador]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spSelectTipoUtilizador]
AS
    SELECT idTipoUtilizador, descricao FROM TblTiposUtilizador 
	WHERE idTipoUtilizador <> 1

GO
/****** Object:  StoredProcedure [dbo].[spSelectUnidades]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSelectUnidades] 
    @idUnidade INT = NULL
AS
    SELECT * FROM TblUnidades
    WHERE (@idUnidade IS NULL) OR (NOT @idUnidade IS NULL AND TblUnidades.idUnidade = @idUnidade)

GO
/****** Object:  StoredProcedure [dbo].[spSelectUtente]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spSelectUtente]
	@idUtente INT = NULL,
    @idLar INT = NULL
AS
    SELECT * 
	FROM vwUtentes
	WHERE 
        ((@idUtente IS NULL) OR (NOT @idUtente IS NULL AND idUtente = @idUtente)) AND 
        ((NULLIF(@idLar, 0)  IS NULL) OR (NOT NULLIF(@idLar, 0)  IS NULL AND idLar = NULLIF(@idLar, 0) ))

GO
/****** Object:  StoredProcedure [dbo].[spSelectUtilizador]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spSelectUtilizador]
	@idUtilizador INT = NULL,
    @idLar INT = NULL
AS
    SELECT * 
	FROM vwUtilizadores 
	WHERE 
        ((@idUtilizador IS NULL) OR (NOT @idUtilizador IS NULL AND id = @idUtilizador)) AND
        ((NULLIF(@idLar, 0)  IS NULL) OR (NOT NULLIF(@idLar, 0)  IS NULL AND idLar = NULLIF(@idLar, 0) ))


GO
/****** Object:  StoredProcedure [dbo].[spTipoAgendamento]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spTipoAgendamento]
AS
    SELECT idAgendamentoTipo, descricao FROM TblAgendamentosTipos
	WHERE idAgendamentoTipo <> 0
GO
/****** Object:  StoredProcedure [dbo].[spTradeStock]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spTradeStock] 
    @idUtenteDe INT,
    @idUtentePara INT,
    @idMedicamento INT,
    @qtd DECIMAL(18,4)
AS
    INSERT INTO TblMovimentos (idMovimentoTipo, idMedicamento, [data], qtd, idUtente)
    VAlUES(3, @idMedicamento, GETDATE(), -@qtd, @idUtenteDe),(3, @idMedicamento, GETDATE(), @qtd, @idUtentePara)

GO
/****** Object:  StoredProcedure [dbo].[spUpdateEncomendas]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[spUpdateEncomendas] 
    @idEncomenda INT,
    @data VARCHAR(19),
    @idUtente INT
AS

    UPDATE TblEncomendas 
    SET 
        [data] = convert(datetime, @data, 105),
        idUtente = @idUtente
    WHERE
        idEncomenda = @idEncomenda

    SELECT @idEncomenda AS idEncomenda



GO
/****** Object:  StoredProcedure [dbo].[spUpdateLar]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdateLar] 
    @idLar INT,
    @descricao VARCHAR(200)
AS
    UPDATE TblLares
    SET descricao = @descricao
    WHERE idLar = @idLar

    SELECT @idLar AS idLar

GO
/****** Object:  StoredProcedure [dbo].[spUpdateMedicamento]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdateMedicamento] 
    @idMedicamento INT,
    @descricao VARCHAR(200),
    @preco DECIMAL(18,2),
    @idMarca INT, 
    @idPosologia INT 
AS
    UPDATE TblMedicamentos
    SET 
        nome = @descricao,
        pVenda = @preco,
        idMarca = @idMarca,
        idPosologia = @idPosologia
    WHERE 
        idMedicamento = @idMedicamento

    SELECT @idMedicamento AS idMedicamento


GO
/****** Object:  StoredProcedure [dbo].[spUpdateUtente]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdateUtente] 
	@idUtente INT = NULL OUTPUT,
	@nome VARCHAR(200),
	@morada VARCHAR(500),
	@contacto VARCHAR(20),
	@idLar INT
AS
BEGIN
	SET NOCOUNT ON
	BEGIN TRY
		BEGIN TRANSACTION
			UPDATE dbo.TblUtentes
			SET 
				nome = @nome, 
				morada = @morada,
				contacto = @contacto,
				idLar = @idLar
			WHERE idUtente = @idUtente
		COMMIT
	END TRY
	BEGIN CATCH
		ROLLBACK;
		THROW;
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[spUpdateUtilizador]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--STORED PROCEDURE - UPDATE USER--
CREATE PROCEDURE [dbo].[spUpdateUtilizador] 
	@idUtilizador INT = NULL OUTPUT,
	@idTipoUtilizador INT,
	@nome VARCHAR(200),
	@username VARCHAR(20),
	@password VARCHAR(20),
	@idLar INT
AS
BEGIN
	SET NOCOUNT ON
	BEGIN TRY
		BEGIN TRANSACTION
			UPDATE dbo.TblUtilizadores
			SET 
				idTipoUtilizador = @idTipoUtilizador, 
				nome = @nome, 
				username = @username,
				[password] = @password, 
				idLar = @idLar
			WHERE idUtilizador = @idUtilizador
		COMMIT
	END TRY
	BEGIN CATCH
		ROLLBACK;
		THROW;
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[spValidateAutenthication]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spValidateAutenthication] 
    @username VARCHAR(20),
    @password VARCHAR(20)
AS
    SELECT DISTINCT
        TblUtilizadores.idUtilizador,
        TblUtilizadores.nome,
        TblTiposUtilizador.idTipoUtilizador AS idTipo,
        TblTiposUtilizador.codigo AS TipoUtilizador,
        ISNULL(TblLares.idLar, 0) AS idLar,
        ISNULL(TblLares.descricao, '') AS Lar
    FROM 
        TblUtilizadores INNER JOIN
        TblTiposUtilizador ON TblUtilizadores.idTipoUtilizador = TblTiposUtilizador.idTipoUtilizador LEFT OUTER JOIN
        TblLares ON TblUtilizadores.idLar = TblLares.idLar
    WHERE
        TblUtilizadores.username = @username AND
        TblUtilizadores.[password] = @password and
        TblUtilizadores.ativo = 1

GO
/****** Object:  StoredProcedure [dbo].[spValidateEncomenda]    Script Date: 28/12/2019 18:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spValidateEncomenda]
	@idEncomenda INT,
    @valido BIT
AS
    UPDATE TblEncomendas
    SET valido = @valido
    WHERE idEncomenda = @idEncomenda


    if (@valido = 1)
    BEGIN
        INSERT INTO TblMovimentos
            (idMovimentoTipo, idKey, idMedicamento, [data], qtd, idUtente)
        SELECT DISTINCT 1, TBLEncomendas.idEncomenda as idkey, idMedicamento, GETDATE(), qtd, TBLEncomendas.idUtente
        FROM 
            TBLEncomendas INNER JOIN 
            TblEncomendasLinhas ON TBLEncomendas.idEncomenda = TblEncomendasLinhas.idEncomenda
        WHERE TBLEncomendas.idEncomenda = @idEncomenda

    END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "TblUtentes"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TblLares"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 102
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwUtentes'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwUtentes'
GO
