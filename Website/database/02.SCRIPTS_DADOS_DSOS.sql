USE [DSOSG3]
GO
INSERT INTO TblTiposUtilizador (codigo, descricao)
VALUES						   ('admin', 'Administrador Geral')
GO 
INSERT INTO TblTiposUtilizador (codigo, descricao)
VALUES						   ('gest', 'Administrador')
GO
INSERT INTO TblTiposUtilizador (codigo, descricao)
VALUES						   ('user', 'Respons�vel T�cnico')
GO
INSERT INTO TblUtilizadores (idTipoUtilizador, nome, username, password, ativo)
VALUES						(1, 'Administrador', 'admin', '1234', 1)
GO
INSERT INTO TblMovimentosTipos (descricao)
VALUES						   ('Encomenda')
GO
INSERT INTO TblMovimentosTipos (descricao)
VALUES						   ('Agendamento')
GO
INSERT INTO TblMovimentosTipos (descricao)
VALUES						   ('Trocas')
GO
INSERT INTO TblPosologias (descricao)
VALUES					  ('Oral')
GO
INSERT INTO TblPosologias (descricao)
VALUES					  ('Anal')
GO
INSERT INTO TblPosologias (descricao)
VALUES					  ('Injet�vel')
GO
INSERT INTO TblUnidades (descricao)
VALUES					('mg')
GO
INSERT INTO TblAgendamentosTipos (descricao)
VALUES					('Contínuo')
GO
INSERT INTO TblAgendamentosTipos (descricao)
VALUES					('Periódico')
GO
INSERT INTO TblAgendamentosTipos (descricao)
VALUES					('SOS')
GO