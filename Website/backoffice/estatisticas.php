<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>SB Admin 2 - Estatísticas</title>

  <!-- Custom styles for this template-->
  <link href="../styles/fontawesome-free/all.css" rel="stylesheet">
  <link href="../styles/font/nunito.css" rel="stylesheet">
  <link href="../styles/sb-admin-2.css" rel="stylesheet"> 
  <link href="../styles/datatables/dataTables.bootstrap4.css" rel="stylesheet">

  <link href="../styles/style.css" rel="stylesheet">
</head>

<body>

<!-- Page Heading -->
<div class="container-fluid h-100perc">

  <div class="card-columns mt-3">
    <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-white">Medicamentos Mais Administrados</h6>
        </div>

        <div class="card-body">
          <div class="input-group col-3 pl-0 pr-0">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1">Top</span>
            </div>
            <input type="number" class="form-control" id="topNumberMed" value="10" aria-label="TopNumber" aria-describedby="basic-addon1">
          </div>

          <div class="col-12 pl-0 pr-0">
            <table id="tblMedicamentoMaisUsados" class="dataTable table table-striped table-bordered" width="100%" height="100%" cellspacing="0"></table>
          </div>

        </div>
    </div>  

    <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-white">Utentes Por Lar</h6>
        </div>

        <div class="card-body">
          <canvas id="chartUtentesPorLar" height="250" width="250"></canvas>
        </div>
    </div> 

    <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-white">Utentes Que Tomam Mais Medicamentos SOS</h6>
        </div>

        <div class="card-body">

          <div class="input-group col-3 pl-0 pr-0">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1">Top</span>
            </div>
            <input type="number" class="form-control" id="topNumberUt" value="10" aria-label="TopNumber" aria-describedby="basic-addon1">
          </div>

          <div class="col-12 pl-0 pr-0">
            <table id="tblUtentesMedicamentosSOS" class="dataTable table table-striped table-bordered" width="100%" height="100%" cellspacing="0"></table>
          </div>

        </div>

    </div>

    

  </div>

</div>

    <!-- Bootstrap core JavaScript-->
    <script src="../scripts/jquery/jquery.js"></script>
    <script src="../scripts/bootstrap/js/bootstrap.bundle.js"></script>
    <script src="../scripts/jquery-easing/jquery.easing.js"></script>
    <script src="../scripts/sb-admin-2.js"></script>

    <script src="../scripts/jquery/jquery.dataTables.js"></script>
    <script src="../scripts/jquery/jquery.validate.js"></script>
    <script src="../scripts/datatables/dataTables.bootstrap4.js"></script>
    <script src="../scripts/chart/Chart.js"></script>

    <script src="estatisticas.js"></script>
    <script src="../scripts/geral.js"></script>
</body>

</html>