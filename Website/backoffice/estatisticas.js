$(document).ready(function () {

    $.ajax({
        url: "../method/getEstatisticaUtentesLar.php",
        type: "POST",
        data: {
            tipo: "1"
        },
        success: function (data) {
            var retJson = JSON.parse(data);

            var labels = retJson.map(function (e) { return e.lar; });
            var data = retJson.map(function (e) { return e.nrUtentes; });
            var colors = [];

            var dif = function () {
                var r = Math.floor(Math.random() * 255);
                var g = Math.floor(Math.random() * 255);
                var b = Math.floor(Math.random() * 255);
                return "rgb(" + r + "," + g + "," + b + ")";
            }

            for (var i in retJson) { colors.push(dif()); }

            var chart = $('#chartUtentesPorLar');
            var myChart = new Chart(chart, {
                type: 'doughnut',
                data: {
                    labels: labels,
                    datasets: [{
                        data: data,
                        backgroundColor: colors,
                    }]
                },
                options: {
                    maintainAspectRatio: false,
                    tooltips: {
                        backgroundColor: "rgb(255,255,255)",
                        bodyFontColor: "#858796",
                        borderColor: '#dddfeb',
                        borderWidth: 1,
                        displayColors: false
                    },
                    legend: {
                        display: true,
                        position: "right",
                        align: "center"
                    },
                    cutoutPercentage: 40
                }
            });
        }
    });

    var tableMedMaisUsados = $('#tblMedicamentoMaisUsados').DataTable({
        ajax: {
            url: "../method/getEstatisticaMedMaisUsados.php",
            dataSrc: "",
            type: "POST",
            data: {
                tipo: "1",
                top: function () { return $("#topNumberMed").val(); }
            }
        },
        bSort: false,
        paging: false,
        language: { url: "../scripts/datatables/lang_pt.json" },
        columns: [
            { title: "Medicamento", data: 'nome', orderable: false, width: "75%" },
            { title: "Qtd Administrada", data: 'quantidade', orderable: false, width: "25%" }
        ],
        select: { style: 'single' },
        scrollY: '90vh',
        scrollCollapse: true,
        lengthChange: false,
        dom: "<'row'<'col-md-6 d-flex mb-1'<'toolbar'>>>" +
            "<'row'<'col-sm-12't>>",
    });

    var tableUtenteMedSOS = $('#tblUtentesMedicamentosSOS').DataTable({
        ajax: {
            url: "../method/getEstatisticaUtentesMedSOS.php",
            dataSrc: "",
            type: "POST",
            data: {
                tipo: "1",
                top: function () { return $("#topNumberUt").val(); }
            }
        },
        bSort: false,
        paging: false,
        language: { url: "../scripts/datatables/lang_pt.json" },
        columns: [
            { title: "Utente", data: 'nome', orderable: false, width: "65%" },
            { title: "Nr. Medicamentos SOS", data: 'quantidade', orderable: false, width: "35%" }
        ],
        select: { style: 'single' },
        scrollY: '90vh',
        scrollCollapse: true,
        lengthChange: false,
        dom: "<'row'<'col-md-6 d-flex mb-1'<'toolbar'>>>" +
            "<'row'<'col-sm-12't>>",
    });

  
});

$("#topNumberUt").on("focusout", function () {
    var table = $('#tblUtentesMedicamentosSOS').DataTable();
    table.ajax.reload();
    
    });

$("#topNumberMed").on("focusout", function () {
    var table = $('#tblMedicamentoMaisUsados').DataTable();
    table.ajax.reload();
    });