<!DOCTYPE html>
<html>

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>SB Admin 2 - Utentes</title>

  <!-- Custom styles for this template-->
  <link href="../styles/fontawesome-free/all.css" rel="stylesheet">
  <link href="../styles/font/nunito.css" rel="stylesheet">
  <link href="../styles/sb-admin-2.css" rel="stylesheet">
  <link href="../styles/datatables/dataTables.bootstrap4.css" rel="stylesheet">

  <link href="../styles/style.css" rel="stylesheet">

</head>

<body>

<!-- Page Heading -->
  <div class="container-fluid h-100perc mt-3">
	  <div class="table-responsive">
		  <table id="tblUtentes" class="dataTable table table-striped table-bordered" id="dataTable" width="100%" height="100%" cellspacing="0">
			<thead>
				<tr>
					<th class="sorting">Nome</th>
					<th class="sorting">Morada</th>
					<th class="sorting">Contacto</th>
					<th class="sorting">Lar</th>
				</tr>
			</thead>

		  </table>
    </div>
  
	<!-- Novo / Editar Utente-->
  <div class="modal fade" id="editarUtente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Novo Utente</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div>
          <form action="../method/editUtente.php" method="POST" id="formUtente">
            <div class="modal-body">
              <input type="hidden" id="hfRowId" value=""/>
              <div class="form-row">
                        <div class="form-group col-12">
                            <label for="txtNomeUtente" class="col-form-label">Nome:</label>
                            <input type="text" class="form-control" id="txtNomeUtente" name="nomeUtente" placeholder="Introduza o nome do utente...">
                        </div>
              </div>
              <div class="form-row">
                        <div class="form-group col-12">
                            <label for="txtMorada" class="col-form-label">Morada:</label>
                            <input type="text" class="form-control" id="txtMorada" name="morada" placeholder="Introduza a morada do utente...">
                        </div>
              </div>
              <div class="form-row">
                        <div class="form-group col-6">
                            <label for="contacto" class="col-form-label">Contacto:</label>
                            <input type="number" min="0" class="form-control" id="contacto" name="contacto" placeholder="Introduza o contacto do utente...">
                        </div>
                <div class="form-group col-6">
                            <label for="inlineFormInput" class="col-form-label">Lar:</label>
                            <select for="inlineFormInput" class="custom-select" id="lar" name="idLar">
                              <option value="0">...</option>
                            </select>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <div class='btn-group' role='group' aria-label='Basic example'>
                <button class="btn btn-primary" type="submit" id="btnGravar" name="submit">
                  <i class="far fa-save"></i>
                </button>
                <button class="btn btn-secondary" type="button" data-dismiss="modal">
                  <i class="fas fa-chevron-left"></i>
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  </div>

    <!-- Bootstrap core JavaScript-->
    <script src="../scripts/jquery/jquery.js"></script>
    <script src="../scripts/bootstrap/js/bootstrap.bundle.js"></script>
    <script src="../scripts/jquery-easing/jquery.easing.js"></script>
    <script src="../scripts/sb-admin-2.js"></script>


  <script src="../scripts/jquery/jquery.validate.js"></script>
    <script src="../scripts/jquery/jquery.dataTables.js"></script>
  <script src="../scripts/datatables/dataTables.bootstrap4.js"></script>


    <script src="listUtentes.js"></script>
    <script src="../scripts/geral.js"></script>
</body>
</html>