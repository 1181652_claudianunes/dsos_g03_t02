//LISTAR UTILIZADORES
$(document).ready(function () {
    var table = $('#tblUtilizadores').DataTable({
        ajax: {
            url: "../method/getUtilizadores.php",
            dataSrc: ""
        },
        language: {
            url: "../scripts/datatables/lang_pt.json"
        },
        columns: [
            { data: 'Tipo' },
            { data: 'Nome' },
            { data: 'Username' },
            { data: 'Lar' }
        ],
        select: {
            style: 'single'
        },
        scrollY:'90vh',
        scrollCollapse: true,
        lengthChange: false,
        dom: "<'row'<'col-md-6 d-flex'<'toolbar'>><'col-md-6'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
        fnInitComplete: function () {
            $('div.toolbar').html("<div class='btn-group' role='group' aria-label='Basic example'>" + 
            "<button id='btnNew' type='button' class='btn btn-light' onclick='createUtilizador()'><i class='far fa-file'></i></button>" + 
            "<button id='btnEdit' type='button' class='btn btn-light' onclick='editUtilizador()'><i class='far fa-edit'></i></button>" + 
            "<button type='button' class='btn btn-light' onclick='deleteUtilizador()'><i class='far fa-trash-alt'></i></button>" + 
            "</div>");
        }
    });

    $('#tblUtilizadores tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });
});

function bindForm(utilizador) {

    var idLar, idTipo
    if (utilizador != undefined){
        document.getElementById("hfRowId").value = utilizador.ID;
        $("#nome").val(utilizador.Nome);
        $("#username").val(utilizador.Username);
        $("#password").val(utilizador.Password);
        idLar = utilizador.idLar;
        idTipo = utilizador.idTipo;
    }else{
        document.getElementById("hfRowId").value = "0"
        $("#nome").val("");
        $("#username").val("");
        $("#password").val("");
        $("#tipo").val("");
        $("#lar").val("");
    }

    var loadTipo = $.ajax({
        url: '../method/getTiposUtilizador.php',
        success: function (data) {
            var retJson = JSON.parse(data);
            var combo = $("#tipo");
            combo.children('option:not(:first)').remove();
            $.each(retJson, function (index, obj) {
                var strSelected = ((idTipo == obj.idTipoUtilizador) ? 'selected' : '') 
                combo.append("<option value='" + obj.idTipoUtilizador + "' " + strSelected + ">" + obj.descricao + "</option>");
            });
        }
    });

    var loadLar = $.ajax({
        url: '../method/getLares.php',
        success: function (data) {
            var retJson = JSON.parse(data);
            var combo = $("#lar");
            combo.children('option:not(:first)').remove();
            $.each(retJson, function (index, obj) {
                var strSelected = ((idLar == obj.idLar) ? 'selected' : '') 
                combo.append("<option value='" + obj.idLar + "' " + strSelected + ">" + obj.descricao + "</option>");
            });
        }
    });

    $.when(loadTipo, loadLar).then(function (e) {

        var modal = $('#editUtilizador');
        modal.modal('show');

        if (document.getElementById("hfRowId").value == "0"){
            modal.find('.modal-title').text('Novo Utilizador');
        }else{
            modal.find('.modal-title').text('Editar Utilizador');
        }
        

        return;
    });
}

//MODAL - CRIAÇÃO E EDIÇÃO DE UTILIZADOR
function createUtilizador(e) {
    bindForm();
}

//EDIÇÃO DE UTILIZADORES
function editUtilizador(e) {
    var table = $('#tblUtilizadores').DataTable();
    if (!table.rows('.selected').any()) {
        alert("Selecione uma linha.");
        return;
    }

    var obj = table.rows('.selected').data()[0];
    bindForm(obj);

}


//ELIMINAÇÃO DE UTILIZADORES
function deleteUtilizador() {
    var table = $('#tblUtilizadores').DataTable();
    if (! table.rows( '.selected' ).any() ) {
        alert("Selecione uma linha.");
        return;
    }

    var obj = table.rows('.selected').data()[0];
    document.getElementById("hfRowId").value = obj.ID;
    $.ajax({
        url: "../method/deleteUtilizador.php",
        type: "POST",
        data: {
            idUtilizador: obj.ID
        },
        cache: false,
        success: function (dataResult) {
            if (dataResult === "true") {
                var table = $('#tblUtilizadores').DataTable();
                table.ajax.reload();
            } else {
                alert("Erro!");
            }
        },
        fail: function (jqXHR, textStatus) {
            alert("Erro!");
        }

    });
}

//FORMULÁRIO - EDIÇÃO E CRIAÇÃO DE UM UTILIZADOR
function validateForm(form) {
    var selLar = document.getElementById("lar");
    var selTipo = document.getElementById("tipo");

    $.ajax({
        url: form.action,
        type: form.method,
        data: {
            idUtilizador: document.getElementById("hfRowId").value,
            tipo: $("#tipo").val(),
            idLar: $("#lar").val(),
            nome: $('#nome').val(),
            username: $('#username').val(),
            password: $('#password').val()
        },
        cache: false,
        success: function (dataResult) {
            if (dataResult == "true") {
                var table = $('#tblUtilizadores').DataTable();
                table.ajax.reload();

                var modal = $('#editUtilizador');
                modal.modal('hide');
            } else {
                alert(dataResult)

            }
        }
    });

}

//Formulário Criação e Edição de Utilizadores (Validação)
$(function () {
    $("#formUtilizador").validate({
            rules: {
                tipo: {
                    required: true,
                    isSelected:"0"
                },

                idLar: {
                    required: true,
                    isSelected: "0"
                },

                nome: {
                    required: true,
                    maxlength: 200
                },

                username: {
                    required: true,
                    maxlength: 20,
                    minlength: 8
                },

                password: {
                    required: true,
                    maxlength: 20,
                    minlength: 8
                },
            },

            submitHandler: validateForm
    });
});