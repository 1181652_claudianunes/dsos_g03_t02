$(document).ready(function() {
    var table = $('#tblMedicamentos').DataTable( {
        ajax: {
            url: "../method/getMedicamentos.php",
            dataSrc: ""
        },
        language: {
            url: "../scripts/datatables/lang_pt.json"
        },
        columns: [
            { data: 'nome' }, 
            { data: 'marca' },
            { data: 'posologia' },
            { data: 'pVenda' }
        ],
        select: {
            style: 'single'
        },
        scrollY:'90vh',
        scrollCollapse: true,
        lengthChange: false,
        dom: "<'row'<'col-md-6 d-flex'<'toolbar'>><'col-md-6'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
        fnInitComplete: function(){
            $('div.toolbar').html("<div class='btn-group' role='group' aria-label='Basic example'> " +
            "<button id='btnNew' type='button' class='btn btn-light' onclick='newMedicamento()'><i class='far fa-file'></i></button>" +
            "<button id='btnEdit' type='button' class='btn btn-light' onclick='editMedicamento()'><i class='far fa-edit'></i></button>" +
            "<button id='btnImportCSV' type='button' class='btn btn-light' onclick='importCSV()'><i class='fas fa-file-upload'></i></button>" +
            "<button id='btnImportWebAPI' type='button' class='btn btn-light' onclick='importWebAPI()'><i class='fas fa-cloud-upload-alt'></i></button>" +
            "<button type='button' class='btn btn-light' onclick='deleteMedicamento()'><i class='far fa-trash-alt'></i></button>" +
            "</div>");
        }
    } );

    $('#tblMedicamentos tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } ); 
});

// CRIAR MEDICAMENTOS
function newMedicamento(e){
    
    bindForm();

    var modal = $('#editarMedicamento') ;
    modal.modal('show');
    modal.find('.modal-title').text('Novo Medicamento');
}

//EDITAR MEDICAMENTOS
function editMedicamento(e){
    var table = $('#tblMedicamentos').DataTable();
    if (! table.rows( '.selected' ).any() ){
        alert("Selecione uma linha.");
        return;
    }
    var obj = table.rows( '.selected' ).data()[0];
    bindForm(obj)

    var modal = $('#editarMedicamento') ;
    modal.modal('show');
    modal.find('.modal-title').text('Editar Medicamento');

}

//IMPORTAR CSV MEDICAMENTOS
function importCSV(){

    var modal = $('#importCSVMedicamentos') ;
    modal.modal('show');
    modal.find('.modal-title').text('Importar CSV');
}

function importWebAPI(){

    $.ajax({
        url: "../method/importWEBMedicamentos.php",
        type: "GET",
        cache: false,
        success: function (dataResult) {
            if (dataResult === "true") {
                alert("Importado com sucesso!");
                var table = $('#tblMedicamentos').DataTable();
                table.ajax.reload();
            } else {
                alert("Erro!");
            }
        },
        fail: function(jqXHR, textStatus){
            alert("Erro!");
        }

    });

}

//ELIMINAR MEDICAMENTOS
function deleteMedicamento(e){
    var table = $('#tblMedicamentos').DataTable();
    if (! table.rows( '.selected' ).any() ){
        alert("Selecione uma linha.");
        return;
    }

    var obj = table.rows( '.selected' ).data()[0];
    $.ajax({
        url: "../method/deleteMedicamento.php",
        type: "POST",
        data: {
            idMedicamento: obj.idMedicamento           
        },
        cache: false,
        success: function (dataResult) {
            if (dataResult === "true") {
                var table = $('#tblMedicamentos').DataTable();
                table.ajax.reload();
            } else {
                alert("Erro!");
            }
        },
        fail: function(jqXHR, textStatus){
            alert("Erro!");
        }

    });

}



// ------------------------------
// EDIÇÃO DOS MEDICAMENTOS
// ------------------------------
var listMedicamentoPA = [];
function bindForm(medicamento){

    listMedicamentoPA = [];
    var idMarca, idPosologia
    if (medicamento != undefined){
        document.getElementById("hfRowId").value = medicamento.idMedicamento;
        $("#txtMedicamento").val(medicamento.nome);
        $("#txtPrecoVenda").val(medicamento.pVenda);
        idMarca = medicamento.idMarca;
        idPosologia = medicamento.idPosologia;
        
    }else{
        document.getElementById("hfRowId").value = "0"
        $("#txtMedicamento").val("");
        $("#txtPrecoVenda").val("");
    }

    $("#tblMedicamentosPA").DataTable().destroy();
    $("#tblMedicamentosPA").DataTable().clear().draw();

    var carregarPrincipiosAtivos =  $.ajax({
        url: '../method/getPrincipiosAtivos.php',
        success: function(data) { listPrincipiosAtivos = JSON.parse(data); },
        fail: function(result){ alert(result); }
    });

    var carregarUnidades = $.ajax({
        url: '../method/getUnidades.php',
        success: function(data) { listUnidades = JSON.parse(data); },
        fail: function(result){ alert(result); }
    });

    var carregarMarca = $.ajax({
        url: '../method/getMarcas.php',
        success: function(data) {
            var retJson = JSON.parse(data);
            var combo = $("#cbMarca");
            combo.children('option:not(:first)').remove();
            $.each(retJson, function(index, obj) {
                var strSelected = ((idMarca == obj.idMarca) ? 'selected' : '') 
                combo.append("<option value='" + obj.idMarca + "' " + strSelected + ">" + obj.descricao + "</option>");
            });
        },
        fail: function(result){
            alert(result);
        }
    });

    var carregarPosologia = $.ajax({
        url: '../method/getPosologias.php',
        success: function(data) {
            var retJson = JSON.parse(data);
            var combo = $("#cbPosologia");
            combo.children('option:not(:first)').remove();
            $.each(retJson, function(index, obj) {
                var strSelected = ((idPosologia == obj.idPosologia) ? 'selected' : '') 
                combo.append("<option value='" + obj.idPosologia + "' " + strSelected + ">" + obj.descricao + "</option>");
            });
        },
        fail: function(result){
            alert(result);
        }
    });


    var carregarPA = $.ajax({
        url: '../method/getMedicamentosPA.php',
        type: 'POST',
        data:{
            idMedicamento: document.getElementById("hfRowId").value
        },
        success: function(data) {
            var retJson = JSON.parse(data);
            listMedicamentoPA = [];
            $.each(retJson, function(index, obj) {
                var element = new medicamentoPA(obj.idPrincipioAtivo, obj.principioativo, obj.dose, obj.idUnidade, obj.unidade)
                listMedicamentoPA.push(element);
            });
        },
        fail: function(result){
            alert(result);
        }
    });

    $.when(carregarPrincipiosAtivos, carregarUnidades, carregarMarca, carregarPosologia, carregarPA).then(function(e){ 

        $("#TblMedicamentosPA").DataTable({
            data: listMedicamentoPA,
            language: {
                url: "../scripts/datatables/lang_pt.json"
            },
            destroy: true,
            searching: false,
            paging: false,
            ordering: false,
            lengthChange: false,
            dom: "<'row'<'col-sm-12't>>",
            columnDefs: [{
                targets: 0,
                data: 'principioativo',
                orderable: false,
                render: function( data, type, row, meta ){
                    if(type === 'display'){
                        var sel = $('<select>');
                        sel.attr('id', 'cbPA_' + meta.row);
                        sel.attr('index', meta.row);
                        sel.attr('class', 'form-control');
                        sel.attr('isSelected', '0');
                        sel.rules('required', 'true');
                        sel.css("width", "100%");

                        sel.append($("<option>").attr('value','0').text('...')); 
                        
                        $(listPrincipiosAtivos).each(function() {
                            var strSelected = ((row.idprincipioativo() == this.idPrincipioAtivo) ? ' selected' : '') 
                            var op = $("<option" + strSelected + ">").attr('value',this.idPrincipioAtivo).text(this.descricao)

                            sel.append(op); 
                        });

                        var div = $('<div>').append(sel);
                        div.css("width", "100%");
                        div.css("height", "100%");

                        return $('<div>').append(div).html();
                    }else{
                        return data;
                    }
                }
            },{
                targets: 1,
                data: 'dose',
                orderable: false,
                type: 'dom-text',
                render: function( data, type, row, meta ){
                    var input = $('<input>');
                    input.attr('min', '0');
                    input.attr('index', meta.row);
                    input.attr('class', 'form-control');
                    input.attr('value', row.dose());

                    input.attr({
                        type: 'number',
                        id: 'txtDose_' + meta.row,
                        pattern: '\d+(\.\d{1,2})?',
                        required: true,
                        number: true,
                        min: 0
                    }).css({
                        width: '100%'
                    });

                    var div = $('<div>').append(input);
                    div.css("width", "100%");
                    div.css("height", "100%");

                    return $('<div>').append(div).html();
                }
            },
            {
                targets: 2,
                data: 'unidade',
                orderable: false,
                render: function( data, type, row, meta ){
                    var sel = $('<select>');
                    sel.attr('id', 'cbUnidade_' + meta.row);
                    sel.attr('index', meta.row);
                    sel.attr('class', 'form-control');
                    sel.attr('isSelected', '0');
                    sel.rules('required', 'true');
                    sel.css("width", "100%");
                    sel.append($("<option>").attr('value','0').text('...')); 
                    $(listUnidades).each(function() {  
                        var strSelected = ((row.idunidade() == this.idUnidade) ? ' selected' : '')
                        var op = $("<option " + strSelected +">").attr('value',this.idUnidade).text(this.descricao)
                        sel.append(op); 
                    });
                    var div = $('<div>').append(sel);
                    div.css("width", "100%");
                    div.css("height", "100%");

                    return $('<div>').append(div).html();
                }
            },
            {
                targets: 3,
                className: "center",
                render: function( data, type, row, meta ){
                    return "<a id='btnDelete_" +  rowID + "' class='btn btn-danger btn-circle btn-sm'><i class='fas fa-times'></a>";
                }
            }],
            select: {
                style: 'single'
            }

        })
    });
}




// --------
// EVENTOS
// --------

// ADICIONAR UMA LINHA A TABELA DOS PRINCIPIOS ATIVOS
$(document).on("click", "#btnAdd", function(){
    rowID--;
    $("#TblMedicamentosPA").DataTable().row.add(new medicamentoPA("", "", "", "", "")).draw();
})

$(document).on("change", "#TblMedicamentosPA tbody select[id^='cbPA_']", function(e){ 
    var cb = e.target;
    var data = $("#TblMedicamentosPA").DataTable().data()[cb.getAttribute("index")];
    var option = cb.selectedOptions[0]

    if ($("#TblMedicamentosPA").DataTable().data().filter(function(value, index){
        if (index == cb.getAttribute("index")){ return false; }
        if(value.idprincipioativo() != cb.value){ return false; }
        return true;
    }).length != 0){
        alert("O principio ativo já existe.");
        $("#" + cb.id).val(data.idprincipioativo());
        return ;
    };

    if (option.value != "0"){
        data.SetIdPrincipioativo(option.value);
        data.setPrincipioativo(option.text);
    }else{
        data.SetIdPrincipioativo(0);
        data.setPrincipioativo("");
    }
});

$(document).on("focusout", "#TblMedicamentosPA tbody input[id^='txtDose_']", function(e){ 
    var txt = e.target;
    var data = $("#TblMedicamentosPA").DataTable().data()[txt.getAttribute("index")];
    data.setDose(txt.value);
});

$(document).on("change", "#TblMedicamentosPA tbody select[id^='cbUnidade_']", function(e){ 
    var cb = e.target;
    var data = $("#TblMedicamentosPA").DataTable().data()[cb.getAttribute("index")];
    var option = cb.selectedOptions[0]
    if (option.value != "0"){
        data.setIdUnidade(option.value);
        data.setUnidade(option.text);
    }else{
        data.setIdUnidade(0);
        data.setUnidade("");
    }
});

// ELIMINAR PRINCIPIO ATIVO
$(document).on("click", "#TblMedicamentosPA tbody a[id^='btnDelete_']", function (e) {
    e.preventDefault();

    $("#TblMedicamentosPA").DataTable().row( $(this).closest('tr') ).remove().draw();
} );



// VALIDAR FORMULÁRIO DE EDIÇÃO
function validateForm(form){
    var data = $("#TblMedicamentosPA").DataTable().data();
    if(data.length <= 0){
        alert("Obrigatório ter pelo menos um principio ativo.")
        return;
    }

    if (data.filter(function(value, index){
        if(value.idprincipioativo() == ""){ return true; }
        if(value.idunidade() == ""){ return true; }
        if(value.dose() == ""){ return true; }
        return false;
    }).length > 0){
        alert("Obrigatório preencher todos os campos nos principios ativos.")
        return;
    }

    var PA = JSON.stringify(data.toArray())

    $.ajax({
        url: "../method/editMedicamento.php",
        type: "POST",
        data: {
            idMedicamento: document.getElementById("hfRowId").value,
            medicamento: $("#txtMedicamento").val(),
            preco: $("#txtPrecoVenda").val(),
            idMarca: $("#cbMarca").val(),
            idPosologia: $("#cbPosologia").val(),
            principioAtivo: PA
        },
        success: function(dataResult) {
            if (dataResult === "true") {
                var table = $('#tblMedicamentos').DataTable();
                table.ajax.reload();

                var modal = $('#editarMedicamento') ;
                modal.modal('hide');
            } else {
                alert(dataResult);
            }            
        }
    });


}

function importForm(form){
    var newdata = new FormData(form);
    $.ajax({  
        url: form.action,  
        method: form.method,  
        data: newdata,  
        contentType:false,          // The content type used when sending data to the server.  
        cache:false,                // To unable request pages to be cached  
        processData:false,          // To send DOMDocument or non processed data file it is set to false  
        success: function(data){  
            if(data=='Error1'){  
                alert("Ficheiro invalido");  
            } else if(data == "Error2") {  
            alert("Please Select File");  
            } else if (data == "true"){  
                alert("Documento importado com sucesso");
                var table = $('#tblMedicamentos').DataTable();
                table.ajax.reload();

                var modal = $('#importCSVMedicamentos') ;
                modal.modal('hide');
            }  else{
                alert(data);
            }
        }  
   })  
}

$(function () {
    $("#formMedicamento").validate({
            rules: {
                medicamento: {
                    required: true,
                    maxlength: 100
                },
                precoVenda: {
                    required: true,
                    number: true,
                    min: 0
                },
                marca: {
                    required: true,
                    isSelected: "0"
                },
                posologia: {
                    required: true,
                    isSelected: "0"
                }
            },
            submitHandler: validateForm
        });
});

$(function () {
    $("#formImportCSVMedicamento").validate({
            rules: {
                file: {
                    required: true
                }
            },
            submitHandler: importForm
        });
});


//MEDICAMENTOS PRINCIPIOS ATIVOS
var rowID = 0;
var listPrincipiosAtivos, listUnidades;
function medicamentoPA(idprincipioativo, principioativo, dose, idunidade, unidade){
    this._idprincipioativo = idprincipioativo;
    this._principioativo = principioativo;
    this._dose = dose;
    this._idunidade = idunidade;
    this._unidade = unidade;

    this.idprincipioativo = function(){ return this._idprincipioativo; }
    this.SetIdPrincipioativo = function(value){ this._idprincipioativo=value; }

    this.principioativo = function(){ return this._principioativo; }
    this.setPrincipioativo = function(value){ this._principioativo=value; }

    this.dose = function(){ return this._dose; }
    this.setDose = function(value){ this._dose=value; }
    
    this.idunidade = function(){ return this._idunidade; }
    this.setIdUnidade = function(value){ this._idunidade=value; }

    this.unidade = function(){ return this._unidade; }
    this.setUnidade = function(value){ this._unidade=value; }

}