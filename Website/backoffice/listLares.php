<!DOCTYPE html>
<html>

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>SB Admin 2 - Lares</title>

  <!-- Custom styles for this template-->
  <link href="../styles/fontawesome-free/all.css" rel="stylesheet">
  <link href="../styles/font/nunito.css" rel="stylesheet">
  <link href="../styles/sb-admin-2.css" rel="stylesheet">
  <link href="../styles/datatables/dataTables.bootstrap4.css" rel="stylesheet">

  <link href="../styles/style.css" rel="stylesheet">

</head>

<body>

<!-- Page Heading -->
  <div class="container-fluid h-100perc mt-3">
    <div class="table-responsive">
      <table id="tblLares" class="dataTable table table-striped table-bordered" width="100%" height="100%">
        <thead>
            <tr>
              <th class="sorting">Lar</th>
            </tr>
        </thead>

      </table>
    </div>
  </div>

  <div class="modal fade" id="editarLar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" width="100%" height="100%">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        
        <!-- HEADER DO MODAL -->
        <div class="modal-header">
          <h5 class="modal-title" id="editarLarCabecalho"></h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>

        <form action="../method/editLar.php" method="POST" id="formLar">
          <!-- CONTENT DO MODAL -->
          <div class="modal-body">
              
                <input type="hidden" id="hfRowId" value=""/>
                <div class="form-group row">
                  <label for="txtNomeLar" class="col-sm-2 col-form-label">Lar</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="txtNomeLar" name="nomeLar" placeholder="Introduza o nome do lar...">
                  </div>
                </div>
              
          </div>
          <!-- FOOTER DO MODAL -->
          <div class="modal-footer">
            <div class='btn-group' role='group' aria-label='Basic example'>
              <button class="btn btn-primary" type="submit" id="btnGravar" name="submit">
                <i class="far fa-save"></i>
              </button>
              <button class="btn btn-secondary" type="button" data-dismiss="modal">
                <i class="fas fa-chevron-left"></i>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="../scripts/jquery/jquery.js"></script>
  <script src="../scripts/bootstrap/js/bootstrap.bundle.js"></script>
  <script src="../scripts/jquery-easing/jquery.easing.js"></script>
  <script src="../scripts/sb-admin-2.js"></script>

  <script src="../scripts/jquery/jquery.validate.js"></script>
  <script src="../scripts/jquery/jquery.dataTables.js"></script>
  <script src="../scripts/datatables/dataTables.bootstrap4.js"></script>

  <script src="listLares.js"></script>
  <script src="../scripts/geral.js"></script>
</body>
</html>