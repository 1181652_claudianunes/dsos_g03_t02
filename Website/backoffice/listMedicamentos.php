<!DOCTYPE html>
<html>

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>SB Admin 2 - Medicamentos</title>

  <!-- Custom styles for this template-->
  <link href="../styles/fontawesome-free/all.css" rel="stylesheet">
  <link href="../styles/font/nunito.css" rel="stylesheet">
  <link href="../styles/sb-admin-2.css" rel="stylesheet">
  <link href="../styles/datatables/dataTables.bootstrap4.css" rel="stylesheet">

  <link href="../styles/style.css" rel="stylesheet">

</head>

<body>

<!-- Page Heading -->
  <div class="container-fluid h-100perc mt-3">
    <div class="table-responsive">
      <table id="tblMedicamentos" class="dataTable table table-striped table-bordered" width="100%" height="100%">
        <thead>
            <tr>
              <th class="sorting">Medicamento</th>
              <th class="sorting">Marca</th>
              <th class="sorting">Posologia</th>
              <th class="sorting">Preço</th>
            </tr>
        </thead>
      </table>
    </div>


    <div class="modal fade" id="editarMedicamento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" width="100%" height="100%">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          
          <!-- HEADER DO MODAL -->
          <div class="modal-header">
            <h5 class="modal-title" id="editarMedicamentoCabecalho"></h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>

          <form action="../method/editMedicamento.php" method="POST" id="formMedicamento">
            <!-- CONTENT DO MODAL -->
            <div class="modal-body">
              <input type="hidden" id="hfRowId" value=""/>
              <div class="form-row">
                <div class="col-9">
                  <label for="lblMedicamento" class="col-sm-2 col-form-label">Medicamento:</label>
                  <input type="text" class="form-control" id="txtMedicamento" name="medicamento" placeholder="Introduza o nome do medicamento...">
                </div>

                <div class="col-3">
                  <label for="lblPrecoVenda" class="col-sm-2 col-form-label">Preço de Venda:</label>
                  <input type="number" class="form-control" id="txtPrecoVenda" name="precoVenda" placeholder="Introduza o preço de venda...">  
                </div>
              </div>
              <div class="form-row">
                <div class="col-6">
                  <label for="lblMarcar" class="col-sm-2 col-form-label">Marca:</label>
                  <select class="form-control" id="cbMarca" name="marca">
                    <option value="0">...</option>
                  </select>
                </div>
                <div class="col-6">
                  <label for="lblPosologia" class="col-sm-2 col-form-label">Posologia:</label>
                  <select class="form-control" id="cbPosologia" name="posologia">
                    <option value="0">...</option>
                  </select>
                </div>
              </div>
              <div class="form-row pt-1">
                <div class="col-12">
                  <button class="btn btn-light mb-1" type="button" id="btnAdd">
                    <i class="fas fa-plus"></i>
                  </button>
                  <div class="table-responsive">
                    <table id="TblMedicamentosPA" name="medicamentosPA" class="dataTable table table-striped table-bordered" width="100%" height="100%">
                      <thead>
                          <tr>
                            <th>Principio Ativo</th>
                            <th>Dose</th>
                            <th>Unidade</th>
                            <th></th>
                          </tr>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <!-- FOOTER DO MODAL -->
            <div class="modal-footer">
              <div class='btn-group' role='group' aria-label='Basic example'>
                <button class="btn btn-primary" type="submit" id="btnGravar" name="submit">
                  <i class="far fa-save"></i>
                </button>
                <button class="btn btn-secondary" type="button" data-dismiss="modal">
                  <i class="fas fa-chevron-left"></i>
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>


    <div class="modal fade" id="importCSVMedicamentos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" width="100%" height="100%">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          
          <!-- HEADER DO MODAL -->
          <div class="modal-header">
            <h5 class="modal-title" id="importCSVMedicamentosCabecalho"></h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>

          <form action="../method/importCSVMedicamentos.php" method="POST" id="formImportCSVMedicamento" enctype="multipart/form-data">
            <!-- CONTENT DO MODAL -->
            <div class="modal-body">
              <div class="form-row">
                <div class="col-12">
                  <label for="lblMedicamento" class="col-sm-2 col-form-label">CSV:</label>
                  <input type="file" class="form-control" name="file" id="txtImportCSV" placeholder="Selecione o ficheiro CSV..." accept=".csv">
                </div>
              </div>
            </div>
            <!-- FOOTER DO MODAL -->
            <div class="modal-footer">
              <div class='btn-group' role='group' aria-label='Basic example'>
                <button class="btn btn-primary" type="submit" id="btnImportar" name="submit">
                  <i class="far fa-save"></i>
                </button>
                <button class="btn btn-secondary" type="button" data-dismiss="modal">
                  <i class="fas fa-chevron-left"></i>
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>


  </div>

    <!-- Bootstrap core JavaScript-->
    <script src="../scripts/jquery/jquery.js"></script>
    <script src="../scripts/bootstrap/js/bootstrap.bundle.js"></script>
    <script src="../scripts/jquery-easing/jquery.easing.js"></script>
    <script src="../scripts/sb-admin-2.js"></script>

    <script src="../scripts/jquery/jquery.validate.js"></script>
    <script src="../scripts/jquery/jquery.dataTables.js"></script>
    <script src="../scripts/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="listMedicamentos.js"></script>
    <script src="../scripts/geral.js"></script>

</body>
</html>