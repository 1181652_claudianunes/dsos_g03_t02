$(document).ready(function() {
    var table = $('#tblLares').DataTable( {
        ajax: {
            url: "../method/getLares.php",
            dataSrc: ""
        },
        language: { url: "../scripts/datatables/lang_pt.json" },
        columns: [ { data: 'descricao' } ],
        select: { style: 'single' },
        scrollY:'90vh',
        scrollCollapse: true,
        lengthChange: false,
        dom: "<'row'<'col-md-6 d-flex'<'toolbar'>><'col-md-6'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
        fnInitComplete: function(){
            $('div.toolbar').html("<div class='btn-group' role='group' aria-label='Basic example'>" +
            "<button id='btnNew' type='button' class='btn btn-light' onclick='newLar()'><i class='far fa-file'></i></button>" + 
            "<button id='btnEdit' type='button' class='btn btn-light' onclick='editLar()'><i class='far fa-edit'></i></button>" +
            "<button type='button' class='btn btn-light' onclick='deleteLar()'><i class='far fa-trash-alt'></i></button>" +
            "</div>");
        }
    } );

    $('#tblLares tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } ); 
});

function newLar(e){
    
    document.getElementById("hfRowId").value = 0;
    $("#txtNomeLar").val("");

    var modal = $('#editarLar') ;
    modal.modal('show');
    modal.find('.modal-title').text('Novo lar');
}

function editLar(e){
    var table = $('#tblLares').DataTable();
    if (! table.rows( '.selected' ).any() ){
        alert("Selecione um linha.");
        return;
    }

    var obj = table.rows( '.selected' ).data()[0];
    document.getElementById("hfRowId").value = obj.idLar;
    $("#txtNomeLar").val(obj.descricao);
    
    var modal = $('#editarLar') ;
    modal.modal('show');
    modal.find('.modal-title').text('Editar lar');
}

function deleteLar(){
    var table = $('#tblLares').DataTable();
    if (! table.rows( '.selected' ).any() ){
        alert("Selecione um linha.");
        return;
    }

    var obj = table.rows( '.selected' ).data()[0];
    $.ajax({
        url: "../method/deleteLar.php",
        type: "POST",
        data: {
            idLar: obj.idLar           
        },
        cache: false,
        success: function (dataResult) {
            if (dataResult === "true") {
                var table = $('#tblLares').DataTable();
                table.ajax.reload();
            } else {
                alert("Erro!");
            }
        },
        fail: function(jqXHR, textStatus){
            alert("Erro!");
        }

    });

    
}

// FORMULÁRIO EDIÇÃO E CRIAÇÃO DE UM LAR
function validateForm(e){
    var txtNomeLar = $("#editarLar").find("input[id=txtNomeLar]");

    $.ajax({
        url: "../method/editLar.php",
        type: "POST",
        data: {
            idLar: document.getElementById("hfRowId").value,
            descricao: txtNomeLar.val()                  
        },
        cache: false,
        success: function (dataResult) {
            if (dataResult === "true") {
                var table = $('#tblLares').DataTable();
                table.ajax.reload();

                var modal = $('#editarLar') ;
                modal.modal('hide');
            } else {
                alert("Erro!");
            }
        },
        fail: function(jqXHR, textStatus){
            alert("Erro!");
        }

    });

}


$(function () {
    $("#formLar").validate({
            rules: {
                nomeLar: {
                    required: true,
                    maxlength: 200
                }
            },

            messages: {
                nomeLar: {
                    required: "Preenchimento obrigatório.",
                    maxlength: "No máximo 200 carácteres."
                }
            },
            submitHandler: validateForm
        });
});