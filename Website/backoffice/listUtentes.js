$(document).ready(function() {
    var table = $('#tblUtentes').DataTable( {
        ajax: {
            url: "../method/getUtentes.php",
            dataSrc: ""
        },
        language: {
            url: "../scripts/datatables/lang_pt.json"
        },
        columns: [
			{ data: 'nome' },
			{ data: 'morada' },
			{ data: 'contacto' },
			{ data: 'descricao' }
        ],
        select: {
            style: 'single'
        },
        scrollY:'90vh',
        scrollCollapse: true,
        lengthChange: false,
        dom: "<'row'<'col-md-6 d-flex'<'toolbar'>><'col-md-6'f>>" +
			"<'row'<'col-sm-12'tr>>" +
			"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
        fnInitComplete: function(){
            $('div.toolbar').html("<div class='btn-group' role='group' aria-label='Basic example'>" + 
            "<button id='btnNew' type='button' class='btn btn-light' onclick='newUtente()'><i class='far fa-file'></i></button>" + 
            "<button id='btnEdit' type='button' class='btn btn-light' onclick='editUtente()'><i class='far fa-edit'></i></button>" + 
            "<button type='button' class='btn btn-light' onclick='deleteUtente()'><i class='far fa-trash-alt'></i></button>" + 
            "</div>");
        }
    } );

    $('#tblUtentes tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } ); 
});

function bindForm() {

    var loadLar = $.ajax({
        url: '../method/getLares.php',
        success: function (data) {
            var retJson = JSON.parse(data);
            var combo = $("#lar");
            combo.children('option:not(:first)').remove();
            $.each(retJson, function (index, obj) {
                combo.append("<option value='" + obj.idLar + "'>" + obj.descricao + "</option>");
            });
        }
    });

    $.when(loadLar).then(function (e) {
        return;
    });
}

function newUtente(e){
    
    document.getElementById("hfRowId").value = 0;
    $("#txtNomeUtente").val("");
	$("#txtMorada").val("");
	$("#contacto").val("");
	$("#lar").val("");

	bindForm();

    var modal = $('#editarUtente') ;
    modal.modal('show');
    modal.find('.modal-title').text('Novo Utente');
}

function editUtente(e){
    var table = $('#tblUtentes').DataTable();
    if (! table.rows( '.selected' ).any() ){
        alert("Selecione uma linha.");
        return;
    }

	bindForm();

    var obj = table.rows( '.selected' ).data()[0];
	var selLar = document.getElementById("lar");
    document.getElementById("hfRowId").value = obj.idUtente;
    
	$.when(bindForm).then(function (e) {
        $.ajax({
            url: "../method/getUtentes.php",
            type: "POST",
            data: {
                idUtente: obj.idUtente
            },
            cache: false,
            success: function (dataResult) {
                var retJson = JSON.parse(dataResult)[0];
                $("#txtNomeUtente").val(retJson.nome);
                $("#txtMorada").val(retJson.morada);
                $("#contacto").val(retJson.contacto);
                $("#lar").val(retJson.idLar);

                var modal = $('#editarUtente');
                modal.modal('show');
                modal.find('.modal-title').text('Editar Utente');
            },
            fail: function (jqXHR, textStatus) {
                alert("Erro!");
            }

        });
    });
}

function deleteUtente(){
    var table = $('#tblUtentes').DataTable();
    if (! table.rows( '.selected' ).any() ){
        alert("Selecione uma linha.");
        return;
    }

    var obj = table.rows( '.selected' ).data()[0];
    $.ajax({
        url: "../method/deleteUtente.php",
        type: "POST",
        data: {
            idUtente: obj.idUtente           
        },
        cache: false,
        success: function (dataResult) {
            if (dataResult === "true") {
                var table = $('#tblUtentes').DataTable();
                table.ajax.reload();
            } else {
                alert("Erro!");
            }
        },
        fail: function(jqXHR, textStatus){
            alert("Erro!");
        }

    });

    
}

// Formulário criação e edição de um utente
function validateForm(e){
	var selLar = document.getElementById("lar");
	
    //var txtNomeUtente = $("#editarUtente").find("input[id=txtNomeUtente]");
	//var txtMorada = $("#editarUtente").find("input[id=txtMorada]");
	//var contacto = $("#editarUtente").find("input[id=contacto]");
	//var txtNomeLar = $("#editarUtente").find("input[id=txtNomeLar]");	
	
	$.ajax({
        url: "../method/editUtente.php",
        type: "POST",
        data: {
            idUtente: document.getElementById("hfRowId").value,
            nome: $('#txtNomeUtente').val(),
            morada: $('#txtMorada').val(),
            contacto: $('#contacto').val(),
			idLar: selLar.options[selLar.selectedIndex].value
        },
        cache: false,
        success: function (dataResult) {
            if (dataResult == "true") {
                var table = $('#tblUtentes').DataTable();
                table.ajax.reload();

                var modal = $('#editarUtente');
                modal.modal('hide');
            } else {
                alert(dataResult)

            }
        }
    });

}

$(function () {
    $("#formUtente").validate({
            rules: {
                nomeUtente: {
                    required: true,
                    maxlength: 200
                },
				morada: {
                    required: true,
                    maxlength: 500
                },
				contacto: {
                    required: true,
                    min: 0
                },
				idLar: {
                    required: true,
                    isSelected: "0"
				}
            },

            submitHandler: validateForm
        });
});