$(document).ready(function() {
    var table = $('#tblEncomendas').DataTable( {
        ajax: {
            url: "../method/getEncomendas.php",
            dataSrc: ""
        },
        language: {
            url: "../scripts/datatables/lang_pt.json"
        },
        columns: [
			{ data: 'numEncomenda' },
			{ data: 'data' },
			{ data: 'utente' },
            { data: 'descValido' },
            { data: 'utilizador' }
        ],
        select: {
            style: 'single'
        },
        scrollY:'90vh',
        scrollCollapse: true,
        lengthChange: false,
        dom: "<'row'<'col-md-6 d-flex'<'toolbar'>><'col-md-6'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
        fnInitComplete: function(){
            $('div.toolbar').html("<div class='btn-group' role='group' aria-label='Basic example'>"+
            "<button id='btnNew' type='button' class='btn btn-light' onclick='newEncomenda()'><i class='far fa-file'></i></button>" +
            "<button id='btnEdit' type='button' class='btn btn-light' onclick='editEncomenda()'><i class='far fa-edit'></i></button>" +
            "<button id='btnEncPendentes' type='button' class='btn btn-light' onclick='validarEncomenda(true)'><i class='far fa-check-circle'></i></button>"+
            "<button id='btnEncPendentes' type='button' class='btn btn-light' onclick='validarEncomenda(false)'><i class='far fa-times-circle'></i></button>"+
            "<button type='button' class='btn btn-light' onclick='deleteEncomenda()'><i class='far fa-trash-alt'></i></button>" + 
            "</div>");
        }
    } );

    $('#tblEncomendas tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } ); 
});


// CRIAR ENCOMENDAS
function newEncomenda(e){
    bindForm();
}

var disabled = false;
function editEncomenda(e){
    var table = $('#tblEncomendas').DataTable();
    if (! table.rows( '.selected' ).any() ){
        alert("Selecione uma linha.");
        return;
    }
    var obj = table.rows( '.selected' ).data()[0];
    bindForm(obj);
} 

function deleteEncomenda(e){
    var table = $('#tblEncomendas').DataTable();
    if (! table.rows( '.selected' ).any() ){
        alert("Selecione uma linha.");
        return;
    }

    var obj = table.rows( '.selected' ).data()[0];
    $.ajax({
        url: "../method/deleteEncomenda.php",
        type: "POST",
        data: {
            idEncomenda: obj.idEncomenda           
        },
        cache: false,
        success: function (dataResult) {
            if (dataResult === "true") {
                var table = $('#tblEncomendas').DataTable();
                table.ajax.reload();
            } else {
                alert("Erro!");
            }
        },
        fail: function(jqXHR, textStatus){
            alert("Erro!");
        }
    });
}

function validarEncomenda(valida){
    var table = $('#tblEncomendas').DataTable();
    if (! table.rows( '.selected' ).any() ){
        alert("Selecione uma linha.");
        return;
    }

    var obj = table.rows( '.selected' ).data()[0];
    if (obj.valido != null){
        alert("Não é possivel mudar o estado de encomendas que já tenham estado.");
        return;
    }

    $.ajax({
        url: "../method/validateEncomenda.php",
        type: "POST",
        data: {
            idEncomenda: obj.idEncomenda,
            valida: (valida ? "1" : "0")         
        },
        cache: false,
        success: function (dataResult) {
            if (dataResult === "true") {
                var table = $('#tblEncomendas').DataTable();
                table.ajax.reload();
            } else {
                alert("Erro!");
            }
        },
        fail: function(jqXHR, textStatus){
            alert("Erro!");
        }
    });
}

var listEncomendasLinhas = [];
function bindForm(encomenda){
	

    listEncomendasLinhas = [];
    //var idUtilizador, idUtente
    if (encomenda != undefined){
        if(encomenda.valido != null){
            disabled = true;
        }else{
            disabled = false;
        }

        document.getElementById("hfRowId").value = encomenda.idEncomenda;
        $("#data").val(encomenda.data.replace(" ", "T"));
        $("#utente").val(encomenda.utente);
        
        
    }else{
        document.getElementById("hfRowId").value = "0"
        $("#data").val("");
        $("#utente").val("");
        disabled = false
    }

    $("#TblEncomendasLinhas").DataTable().destroy();
    $("#TblEncomendasLinhas").DataTable().clear().draw();
	
	var loadUtente = $.ajax({
        url: '../method/getUtentes.php',
        success: function (data) {
            var retJson = JSON.parse(data);
            var combo = $("#utente");
            combo.children('option:not(:first)').remove();
            $.each(retJson, function (index, obj) {
                combo.append("<option value='" + obj.idUtente + "'>" + obj.nome + "</option>");
            });
        }
    });

    var carregarMedicamentos =  $.ajax({
        url: '../method/getMedicamentos.php',
        success: function(data) { listMedicamentos = JSON.parse(data); },
        fail: function(result){ alert(result); }
    });

    var carregarUnidades = $.ajax({
        url: '../method/getUnidades.php',
        success: function(data) { listUnidades = JSON.parse(data); },
        fail: function(result){ alert(result); }
    });

    var carregarLinhas = $.ajax({
        url: '../method/getEncomendasLinhas.php',
        type: 'POST',
        data:{
            idEncomenda: document.getElementById("hfRowId").value
        },
        success: function(data) {
            var retJson = JSON.parse(data);
            listEncomendasLinhas = [];
            $.each(retJson, function(index, obj) {
                var element = new encomendaLinhas(obj.idMedicamento, obj.medicamento, obj.qtd, obj.pCusto ,obj.idUnidade, obj.unidade)
                listEncomendasLinhas.push(element);
            });
        },
        fail: function(result){
            alert(result);
        }
    });

    $.when(loadUtente, carregarMedicamentos, carregarUnidades, carregarLinhas).then(function(e){ 

        $("#data").prop("disabled", disabled);
        $("#utente").prop("disabled", disabled);
        $("#btnAdd").prop("disabled", disabled);
        $("#btnGravar").prop("disabled", disabled);

        $("#TblEncomendasLinhas").DataTable({
            data: listEncomendasLinhas,
            language: {
                url: "../scripts/datatables/lang_pt.json"
            },
            destroy: true,
            searching: false,
            paging: false,
            ordering: false,
            lengthChange: false,
            dom: "<'row'<'col-sm-12't>>",
            columnDefs: [{
                targets: 0,
                orderable: false,
                render: function( data, type, row, meta ){
                    if(type === 'display'){
                        var sel = $('<select>');
                        sel.attr('id', 'cbMd_' + rowID);
                        sel.attr('index', meta.row);
                        sel.attr('class', 'form-control');
                        sel.attr('isSelected', '0');
                        sel.prop("disabled", disabled);
                        sel.rules('required', 'true');
                        sel.css("width", "100%");

                        sel.append($("<option>").attr('value','0').text('...')); 
                        
                        $(listMedicamentos).each(function() {
                            var strSelected = ((row.idMedicamento() == this.idMedicamento) ? ' selected' : '') 
                            var op = $("<option" + strSelected + ">").attr('value',this.idMedicamento).text(this.nome + " (" + this.marca + ")")

                            sel.append(op); 
                        });

                        var div = $('<div>').append(sel);
                        div.css("width", "100%");
                        div.css("height", "100%");

                        return $('<div>').append(div).html();
                    }else{
                        return data;
                    }
                }
            },{
                targets: 1,
                orderable: false,
                type: 'dom-text',
                render: function( data, type, row, meta ){
                    var input = $('<input>');
                    input.attr('min', '0');
                    input.attr('index', meta.row);
                    input.attr('class', 'form-control');
                    input.attr('value', row.qtd());
                    input.prop("disabled", disabled);
                    input.rules('required', 'true');
                    input.rules('number', 'true');
                    input.attr({
                        type: 'number',
                        id: 'txtQtd_' + rowID,
                        pattern: '\d+(\.\d{1,2})?'
                    }).css({
                        width: '100%'
                    });

                    var div = $('<div>').append(input);
                    div.css("width", "100%");
                    div.css("height", "100%");

                    return $('<div>').append(div).html();
                }
            },
            {
                targets: 3,
                orderable: false,
                type: 'dom-text',
                render: function( data, type, row, meta ){
                    var input = $('<input>');
                    input.attr('min', '0');
                    input.attr('index', meta.row);
                    input.attr('class', 'form-control');
                    input.attr('value', row.pCusto());
                    input.prop("disabled", disabled);
                    input.rules('required', 'true');
                    input.rules('number', 'true');
                    input.attr({
                        type: 'number',
                        id: 'txtPCusto_' + rowID,
                        pattern: '\d+(\.\d{1,2})?'
                    }).css({
                        width: '100%'
                    });

                    var div = $('<div>').append(input);
                    div.css("width", "100%");
                    div.css("height", "100%");

                    return $('<div>').append(div).html();
                }
            },
			{
                targets: 2,
                orderable: false,
                render: function( data, type, row, meta ){
                    var sel = $('<select>');
                    sel.attr('id', 'cbUnidade_' + rowID);
                    sel.attr('index', meta.row);
                    sel.attr('class', 'form-control');
                    sel.attr('isSelected', '0');
                    sel.prop("disabled", disabled);
                    sel.rules('required', 'true');
                    sel.css("width", "100%");
                    sel.append($("<option>").attr('value','0').text('...')); 
                    $(listUnidades).each(function() {  
                        var strSelected = ((row.idUnidade() == this.idUnidade) ? ' selected' : '')
                        var op = $("<option " + strSelected +">").attr('value',this.idUnidade).text(this.descricao)
                        sel.append(op); 
                    });
                    var div = $('<div>').append(sel);
                    div.css("width", "100%");
                    div.css("height", "100%");

                    return $('<div>').append(div).html();
                }
            },
            {
                targets: 4,
                className: "center",
                render: function( data, type, row, meta ){
                    return (disabled ? "" : "<a id='btnDelete_" +  rowID + "' class='btn btn-danger btn-circle btn-sm'><i class='fas fa-times'></a>");
                }
            }]

        })

        var modal = $('#editarEncomenda') ;
        modal.modal('show');
        if (document.getElementById("hfRowId").value == "0"){
            modal.find('.modal-title').text('Nova Encomenda');
        }else{
            if (disabled){
                modal.find('.modal-title').text('Visualizar Encomenda');
            }else{
                modal.find('.modal-title').text('Editar Encomenda');
            }
        }
    });
}

// ADICIONAR UMA LINHA A TABELA DAS ENCOMENDAS LINHAS
$(document).on("click", "#btnAdd", function(){
    rowID--;
    $("#TblEncomendasLinhas").DataTable().row.add(new encomendaLinhas("", "", "", "", "", "")).draw();
})

$(document).on("change", "#TblEncomendasLinhas tbody select[id^='cbMd_']", function(e){ 
    var cb = e.target;
    var data = $("#TblEncomendasLinhas").DataTable().data()[cb.getAttribute("index")];
    var option = cb.selectedOptions[0]

    if ($("#TblEncomendasLinhas").DataTable().data().filter(function(value, index){
        if (index == cb.getAttribute("index")){ return false; }
        if(value.idMedicamento() != cb.value){ return false; }
        return true;
    }).length != 0){
        alert("O medicamento já existe.");
        $("#" + cb.id).val(data.idMedicamento());
        return ;
    };

    if (option.value != "0"){
        data.SetIdMedicamento(option.value);
        data.setMedicamento(option.text);
    }else{
        data.SetIdMedicamento(0);
        data.setMedicamento("");
    }
});

$(document).on("focusout", "#TblEncomendasLinhas tbody input[id^='txtQtd_']", function(e){ 
    var txt = e.target;
    var data = $("#TblEncomendasLinhas").DataTable().data()[txt.getAttribute("index")];
    data.setQtd(txt.value);
});

$(document).on("focusout", "#TblEncomendasLinhas tbody input[id^='txtPCusto_']", function(e){ 
    var txt = e.target;
    var data = $("#TblEncomendasLinhas").DataTable().data()[txt.getAttribute("index")];
    data.setPCusto(txt.value);
});

$(document).on("change", "#TblEncomendasLinhas tbody select[id^='cbUnidade_']", function(e){ 
    var cb = e.target;
    var data = $("#TblEncomendasLinhas").DataTable().data()[cb.getAttribute("index")];
    var option = cb.selectedOptions[0];

    if (option.value != "0"){
        data.setIdUnidade(option.value);
        data.setUnidade(option.text);
    }else{
        data.setIdUnidade(0);
        data.setUnidade("");
    }
});

// ELIMINAR MEDICAMENTO
$(document).on("click", "#TblEncomendasLinhas tbody a[id^='btnDelete_']", function (e) {
    e.preventDefault();

    $("#TblEncomendasLinhas").DataTable().row( $(this).closest('tr') ).remove().draw();
} );

// VALIDAR FORMULÁRIO DE EDIÇÃO
function validateForm(form){
    var data = $("#TblEncomendasLinhas").DataTable().data();
    if(data.length <= 0){
        alert("Obrigatório ter pelo menos um medicamento.")
        return;
    }

    if (data.filter(function(value, index){
        if(value.idMedicamento() == ""){ return true; }
        if(value.qtd() == ""){ return true; }
        if(value.pCusto() == ""){ return true; }
        if(value.idUnidade() == ""){ return true; }
        return false;
    }).length > 0){
        alert("Obrigatório preencher todos os campos nos medicamentos.")
        return;
    }


    var encomendaLinhas = JSON.stringify(data.toArray());
    var novadata = new Date($("#data").val());
    var strdata = formatDate(novadata);

    $.ajax({
        url: form.action,
        type: form.method,
        data: {
            idEncomenda: document.getElementById("hfRowId").value,
            data: strdata,
            idUtente: $("#utente").val(),
            encomendaLinhas: encomendaLinhas
        },
        success: function(dataResult) {
            if (dataResult === "true") {
                var table = $('#tblEncomendas').DataTable();
                table.ajax.reload();

                var modal = $('#editarEncomenda') ;
                modal.modal('hide');
            } else {
                alert(dataResult);
            }            
        }
    });


}

$(function () {
    $("#formEncomenda").validate({
            rules: {
                data: {
                    required: true
                },
                utente: {
                    required: true
                },
            },
            submitHandler: validateForm
        });
});

//MEDICAMENTOS PRINCIPIOS ATIVOS
var rowID = 0;
var listEncomenda;
function encomendaLinhas(idMedicamento, medicamento, qtd, pCusto, idUnidade, unidade){
    this._idMedicamento = idMedicamento;
    this._medicamento = medicamento;
	this._qtd = qtd;
	this._pCusto = pCusto;
	this._idUnidade = idUnidade;
	this._unidade = unidade;

    this.idMedicamento = function(){ return this._idMedicamento; }
    this.SetIdMedicamento = function(value){ this._idMedicamento=value; }
	
    this.medicamento = function(){ return this._medicamento; }
    this.setMedicamento = function(value){ this._medicamento=value; }
	
	this.qtd = function(){ return this._qtd; }
    this.setQtd = function(value){ this._qtd=value; }
	
	this.pCusto = function(){ return this._pCusto; }
    this.setPCusto = function(value){ this._pCusto=value; }
	
	this.idUnidade = function(){ return this._idUnidade; }
    this.setIdUnidade = function(value){ this._idUnidade=value; }
	
	this.unidade = function(){ return this._unidade; }
    this.setUnidade = function(value){ this._unidade=value; }

    }
