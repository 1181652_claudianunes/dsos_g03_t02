<!DOCTYPE html>
<html>

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>SB Admin 2 - Encomendas</title>

  <!-- Custom styles for this template-->
  <link href="../styles/fontawesome-free/all.css" rel="stylesheet">
  <link href="../styles/font/nunito.css" rel="stylesheet">
  <link href="../styles/sb-admin-2.css" rel="stylesheet">
  <link href="../styles/datatables/dataTables.bootstrap4.css" rel="stylesheet">

  <link href="../styles/style.css" rel="stylesheet">

</head>

<body>

	<!-- Page Heading -->
	  <div class="container-fluid h-100perc mt-3">
		  <div class="table-responsive">
			  <table id="tblEncomendas" class="dataTable table table-striped table-bordered" id="dataTable" width="100%" height="100%" cellspacing="0">
				<thead>
					<tr>
						<th class="sorting">Nr. Enc.</th>
						<th class="sorting">Data</th>
						<th class="sorting">Utente</th>
                        <th class="sorting">Estado</th>
						<th class="sorting">Encomendado Por</th>
            
					</tr>
				</thead>

			  </table>
	  </div>

	  <!-- Nova / Editar Encomenda-->
  <div class="modal fade" id="editarEncomenda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Nova Encomenda</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div>
	<form action="../method/editEncomenda.php" method="POST" id="formEncomenda">
		<div class="modal-body">
			<input type="hidden" id="hfRowId" value=""/>
		  <div class="form-row">
                <div class="form-group col-6">
                    <label for="inlineFormInput" class="col-form-label">Data:</label>
                    <input type="datetime-local" class="form-control" id="data" name="data">
                </div>
                <div class="form-group col-6">
                    <label for="inlineFormInput" class="col-form-label">Utente:</label>
                    <select for="inlineFormInput" class="custom-select" id="utente" name="idUtente">
                        <option value="" disabled selected>Selecione o Utente:</option>
                              <option value="0">...</option>
                    </select>
                </div>
              </div>
              <div class="form-row pt-1">
                <div class="col-12">
                  <button class="btn btn-light mb-1" type="button" id="btnAdd">
                      <i class="fas fa-plus"></i>
                  </button>
                  <div class="table-responsive">
                    <table id="TblEncomendasLinhas" class="dataTable table table-striped table-bordered" width="100%" height="100%">
                      <thead>
                          <tr>
                            <th class="sorting">Medicamento</th>
                            <th class="sorting">Quantidade</th>
                            <th class="sorting">Unidade</th>
                            <th class="sorting">Preço Custo</th>
                            <th></th>
                          </tr>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
            </div>
<!-- FOOTER DO MODAL -->
            <div class="modal-footer">
              <div class='btn-group' role='group' aria-label='Basic example'>
                <button class="btn btn-primary" type="submit" id="btnGravar" name="submit">
                  <i class="far fa-save"></i>
                </button>
                <button class="btn btn-secondary" type="button" data-dismiss="modal">
                  <i class="fas fa-chevron-left"></i>
                </button>
              </div>
            </div>
          </form>
		  </div>
		  
		  </div>
	
		</div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="../scripts/jquery/jquery.js"></script>
    <script src="../scripts/bootstrap/js/bootstrap.bundle.js"></script>
    <script src="../scripts/jquery-easing/jquery.easing.js"></script>
    <script src="../scripts/sb-admin-2.js"></script>


    <script src="../scripts/jquery/jquery.validate.js"></script>
    <script src="../scripts/jquery/jquery.dataTables.js"></script>
    <script src="../scripts/datatables/dataTables.bootstrap4.js"></script>

    <script src="../scripts/geral.js"></script>
    <script src="listEncomendas.js"></script>
</body>
</html>