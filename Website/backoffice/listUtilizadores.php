<!DOCTYPE html>
<html>

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>SB Admin 2 - Utilizadores</title>

  <!-- Custom styles for this template-->
  <link href="../styles/fontawesome-free/all.css" rel="stylesheet">
  <link href="../styles/font/nunito.css" rel="stylesheet">
  <link href="../styles/sb-admin-2.css" rel="stylesheet">
  <link href="../styles/datatables/dataTables.bootstrap4.css" rel="stylesheet">

  <link href="../styles/style.css" rel="stylesheet">

</head>

<body>
   
<!-- Page Heading -->
  <div class="container-fluid h-100perc mt-3">
    <div class="table-responsive">
      <table id="tblUtilizadores" class="dataTable table table-striped table-bordered" width="100%" height="100%">
        <thead>
            <tr>
                <th>Tipo de Utilizador</th>
                <th>Nome</th>
                <th>Username</th>
                <th>Lar</th> 
            </tr>
        </thead>
      </table>
    </div>
  </div>

<!-- Novo / Editar Utilizador-->
<div class="modal fade" id="editUtilizador" tabindex="-1" role="dialog" aria-labelledby="editUtilizador" aria-hidden="true" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="editarUtilizadorCabecalho"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
      </div>

        <form action="../method/createUtilizadores.php" method="POST" id="formUtilizador">
          
            <div class="modal-body">
             
              <input type="hidden" id="hfRowId" value=""/>
              <div class="form-row">
                <div class="form-group col-6">
                    <label for="inlineFormInput" class="col-form-label">Tipo de Utilizador:</label>
                    <select for="inlineFormInput" class="custom-select" id="tipo" name="tipo">
                      <option value="0">...</option>
                    </select>
                </div>
                <div class="form-group col-6">
                    <label for="inlineFormInput" class="col-form-label">Lar:</label>
                    <select for="inlineFormInput" class="custom-select" id="lar" name="idLar">
                        <option value="0">...</option>
                     </select>
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col-12">
                    <label for="inlineFormInput" class="col-form-label">Nome:</label>
                    <input type="text" class="form-control" id="nome" name="nome" placeholder="Introduza o nome do utilizador...">
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col-6">
                    <label for="inlineFormInput" class="col-form-label">Username:</label>
                    <input type="text" class="form-control" id="username" name="username" placeholder="Username">
                </div>
                <div class="form-group col-6">
                    <label for="inlineFormInput" class="col-form-label">Password:</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                </div>
              </div>
        
          </div>
          <div class="modal-footer">
            <div class='btn-group' role='group' aria-label='Basic example'>
              <button class="btn btn-primary" type="submit" id="btnGravar" name="submit">
                <i class="far fa-save"></i>
              </button>
              <button class="btn btn-secondary" type="button" data-dismiss="modal">
                <i class="fas fa-chevron-left"></i>
              </button>
            </div>
          </div>
        </form>
    </div>
  </div>
</div>

<!-- Bootstrap core JavaScript-->
    <script src="../scripts/jquery/jquery.js"></script>
    <script src="../scripts/bootstrap/js/bootstrap.bundle.js"></script>
    <script src="../scripts/jquery-easing/jquery.easing.js"></script>
    <script src="../scripts/sb-admin-2.js"></script>

    <script src="../scripts/jquery/jquery.dataTables.js"></script>
    <script src="../scripts/jquery/jquery.validate.js"></script>
    <script src="../scripts/datatables/dataTables.bootstrap4.js"></script>

    <script src="listUtilizadores.js"></script>
    <script src="../scripts/geral.js"></script>

</body>

</html>