// MENSAGENS DAS VALIDAÇÕES
jQuery.extend(jQuery.validator.messages, {
    required: "Preenchimento obrigatório.",
    remote: "Please fix this field.",
    email: "Please enter a valid email address.",
    url: "Please enter a valid URL.",
    date: "Please enter a valid date.",
    dateISO: "Please enter a valid date (ISO).",
    number: "Inserir valores numéricos.",
    digits: "Please enter only digits.",
    creditcard: "Please enter a valid credit card number.",
    equalTo: "Please enter the same value again.",
    accept: "Please enter a value with a valid extension.",
    maxlength: jQuery.validator.format("No máximo {0} caracteres."),
    minlength: jQuery.validator.format("No mínimo {0} caracteres."),
    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
    max: jQuery.validator.format("Inserir valor inferior ou igual a {0}."),
    min: jQuery.validator.format("Inserir valor superior ou igual a {0}."),
    greaterThanZero: "Inserir valores positivos.",
    isSelected: "Selecione uma opção."
});

// VERIFICAR SE O VALOR É SUPERIOR A ZERO(0)
jQuery.validator.addMethod("greaterThanZero", function(value, element) {
    return this.optional(element) || (parseFloat(value) > 0);
});

// VERIFICAR SE EXISTE ALGUM VALOR SELECIONADO
jQuery.validator.addMethod("isSelected", function(value, element, arg) {
    return arg !== value;
});


function changeRightPane(url, title){
    $("#title")[0].innerText = title;
    document.getElementById("frameContent").src = url;
}

function logout(){
    XMLHttp("GET", "method/cleanSession.php", function() {
        if (this.readyState == 4 && this.status == 200) { 
            window.location.href = "login.php";
       } 
    })
}

function XMLHttp(type, url, func){
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = func;
    xmlhttp.open(type, url, true);
    xmlhttp.send();
}

function formatDate(date) {
    return date.getDate() + '-' + (date.getMonth()+1) + '-' + date.getFullYear() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() ;
}