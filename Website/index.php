<?php
include "method/validateSession.php";
?>

<!DOCTYPE html>
<html>

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>SB Admin 2 - Dashboard</title>

  <!-- Custom styles for this template-->
  <link href="styles/fontawesome-free/all.css" rel="stylesheet">
  <link href="styles/font/nunito.css" rel="stylesheet">
  <link href="styles/sb-admin-2.css" rel="stylesheet">
  <link href="styles/style.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
        <div class="sidebar-brand-icon">
          <i class="fas fa-hand-holding-heart"></i>
        </div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link collapsed" id="btnDashboard" href="#" onclick="changeRightPane('frontoffice/dashboard.php', 'Dashboard')" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      

      <?php 
        if($_SESSION["idTipo"]<=2): 
      ?>
        <!-- Divider -->
        <hr class="sidebar-divider">
        
        <!-- Heading -->
        <div class="sidebar-heading">
          Backoffice
        </div>

        <!-- Nav Item - UTILIZADORES -->
        <li class="nav-item">
          <a class="nav-link collapsed" href="#" onclick="changeRightPane('backoffice/listUtilizadores.php', 'Utilizadores')" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-users"></i>  
            <span>Utilizadores</span>
          </a>
        </li>

        <!-- Nav Item - UTENTES -->
        <li class="nav-item">
          <a class="nav-link collapsed" href="#" onclick="changeRightPane('backoffice/listUtentes.php', 'Utentes')" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-user-injured"></i>  
            <span>Utentes</span>
          </a>
        </li>

        <!-- Nav Item - MEDICAMENTOS -->
        <li class="nav-item">
          <a class="nav-link collapsed" href="#" onclick="changeRightPane('backoffice/listMedicamentos.php', 'Medicamentos')" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-capsules"></i>  
            <span>Medicamentos</span>
          </a>
        </li>

        <!-- Nav Item - ENCOMENDAS -->
        <li class="nav-item">
          <a class="nav-link collapsed" href="#" onclick="changeRightPane('backoffice/listEncomendas.php', 'Encomendas')" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-truck"></i>
            <span>Encomendas</span>
          </a>
        </li>

        <?php 
          if($_SESSION["idTipo"]<=1): 
        ?>
          <!-- Nav Item - LARES (APENAS PARA ADMINISTRADOR GERAL) -->
          <li class="nav-item">
            <a class="nav-link collapsed" href="#" onclick="changeRightPane('backoffice/listLares.php', 'Lares')" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
              <i class="fas fa-clinic-medical"></i>  
              <span>Lares</span>
            </a>
          </li>
          <!-- Nav Item - ESTATÍSTICAS -->
          <li class="nav-item">
            <a class="nav-link collapsed" href="#" onclick="changeRightPane('backoffice/estatisticas.php', 'Estatísticas')" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
              <i class="fas fa-chart-bar"></i>
              <span>Estatísticas</span>
            </a>
          </li>
        
        
        <?php 
          else: 
            echo ""; 
          endif; 
        ?> 

      <?php 
        else: 
          echo ""; 
        endif; 
      ?> 


      <?php 
        if($_SESSION["idTipo"]<=3): 
      ?>
        <!-- Divider -->
        <hr class="sidebar-divider">
        
        <!-- Heading -->
        <div class="sidebar-heading">
          Frontoffice
        </div>

        <!-- Nav Item - TERAPÊUTICAS -->
        <li class="nav-item">
          <a class="nav-link collapsed" href="#" onclick="changeRightPane('frontoffice/listTerapeuticas.php', 'Terapêuticas')" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
            <i class="fas fa-tasks"></i>  
            <span>Terapêuticas</span>
          </a>
        </li>

        <!-- Nav Item - STOCKS -->
        <li class="nav-item">
          <a class="nav-link collapsed" href="#" onclick="changeRightPane('frontoffice/listStock.php', 'Stocks')" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages"> 
            <i class="fas fa-clipboard-list"></i>
            <span>Stock</span></a>
        </li>
      <?php 
        else: 
          echo ""; 
        endif; 
      ?> 

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar static-top shadow" >

          <!-- Topbar Search -->
          <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <div class="d-sm-flex align-items-center justify-content-between mt-4 mb-4">
                <h1 class="h3 mb-0 text-gray-800" id="title"></h1>
            </div>
          </form>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $_SESSION["nome"]; ?></span>
                <img class="img-profile rounded-circle" src="images/profile.png" >
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#" onclick="logout()">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->
      

      <!-- Main Content -->
      <div id="content">
        <article class="w-100perc h-100perc">
          <iframe id="frameContent" width="100%" height="100%" scrolling="auto"></iframe>
        </article>
      </div>
      
      <!-- End of Main Content -->


      <!-- Footer -->
      <footer class="sticky-footer bg-white" style="height:80px; overflow:hidden;">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; <a target="_blank"  href="https://startbootstrap.com/themes/sb-admin-2/">Start Bootstrap - SB Admin 2</a>, 2019</span>

          </div>
        </div>
      </footer>
      <!-- End of Footer -->
    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->



  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <script src="scripts/jquery/jquery.js"></script>
  <script src="scripts/bootstrap/js/bootstrap.bundle.js"></script>
  <script src="scripts/jquery-easing/jquery.easing.js"></script>
  <script src="scripts/sb-admin-2.js"></script>

  <script src="scripts/jquery/jquery.validate.js"></script>

  <script src="scripts/geral.js"></script>

  <script>
  $(document).ready(function () {
    $("#btnDashboard").click();
  });
  </script>

</body>

</html>
