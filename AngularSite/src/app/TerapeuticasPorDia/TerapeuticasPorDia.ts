import {Component, OnInit, ViewChild} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DataSource } from '@angular/cdk/collections';
import { FormControl, Validators } from '@angular/forms';
import { MatSelectModule, MatSelect } from '@angular/material/select';
import { DatePipe, formatDate } from '@angular/common'

import { TerapeuticasPorDiaService } from '../TerapeuticasPorDia/TerapeuticasPorDia.service';
import { TerapeuticasPorDiaElement } from '../TerapeuticasPorDia/TerapeuticasPorDia.interface'

@Component({
    selector: 'TerapeuticasPorDia',
    styleUrls: ['../TerapeuticasPorDia/TerapeuticasPorDia.css'],
    templateUrl: '../TerapeuticasPorDia/TerapeuticasPorDia.html'
  })


  export class TerapeuticasPorDiaComponent implements OnInit{

    private _http: HttpClient;

    TerapeuticasPorDia: TerapeuticasPorDiaService;

    //Data
    selectedDate: Date;

    //Tabela - Terapeuticas
    dataSource: UserDataSource;
    displayedColumns = ['utente','medicamento','quantidade','dataHora','dataExecucao','periodicidade','fim'];

    constructor(private http: HttpClient) {
      this._http = http;
      this.TerapeuticasPorDia = new TerapeuticasPorDiaService(this._http);
  }

  ngOnInit() { }

  public getTerapeuticasPorDiaById() {
    if(this.selectedDate != undefined){
      try {
          var format: DatePipe = new DatePipe('en-US');
          var data = format.transform(this.selectedDate, 'dd-MM-yyyy');
          this.dataSource = new UserDataSource(this.TerapeuticasPorDia, data.toString());
          this.dataSource.connect();
      } catch (error) {
          alert("Erro - " + error);
      }
    }else{
      alert("Selecione uma Data.");
  }
  }
}

export class UserDataSource extends DataSource<any> {
  private _data: string;

  constructor(private userService: TerapeuticasPorDiaService, data: string) {
      super();
      this._data = data;
  }

  connect(): Observable<TerapeuticasPorDiaElement[]> {
      return this.userService.getTerapeuticasBy(this._data);
  }

  disconnect() {}
}