export interface TerapeuticasPorDiaElement {
    idAgendamento: number;
    idUtente: number;
    utente: string;
    idMedicamento: number;
    medicamento: number;
    quantidade: number;
    idAgendamentoTipo: number;
    dataHora: any;
    dataExecucao: any;
    periocidade: number;
    fim: any;
}