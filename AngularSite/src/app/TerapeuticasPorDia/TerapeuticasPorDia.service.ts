import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { TerapeuticasPorDiaElement } from '../TerapeuticasPorDia/TerapeuticasPorDia.interface'


const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST, PATCH, PUT, DELETE, OPTIONS'
    })
};
const apiUrl = 'http://localhost:8090';


@Injectable()
export class TerapeuticasPorDiaService {

    constructor(private http: HttpClient) { }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.error(error);
            return of(result as T);
        };
    }

    public getTerapeuticasBy(data: string): Observable<TerapeuticasPorDiaElement[]> {
        const url = `${apiUrl}/TerapeuticasPorDia/${data}`;
        return this.http.get<TerapeuticasPorDiaElement[]>(url);
    }
}