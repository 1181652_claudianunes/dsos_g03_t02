export interface UtentesPorTecnicoElement {
    idUtente: number;
    idLar: number;
    descricao: string;
    nome: string;
    morada: string;
    contacto: string;
    ativo: any;
    idUtilizador: number;
}

export interface Tecnico {
    ID: number;
    idTipo: number;
    Tipo: string;
    Nome: string;
    Username: string;
    Password: string;
    idLar: number;
    Lar: string;
}

