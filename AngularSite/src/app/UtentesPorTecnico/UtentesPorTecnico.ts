import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DataSource } from '@angular/cdk/collections';
import { FormControl, Validators } from '@angular/forms';
import { MatSelectModule, MatSelect } from '@angular/material/select';

import { UtentesPorTecnicoService } from '../UtentesPorTecnico/UtentesPorTecnico.service';
import { UtentesPorTecnicoElement, Tecnico } from '../UtentesPorTecnico/UtentesPorTecnico.interface'


@Component({
    selector: 'UtentesPorTecnico',
    styleUrls: ['../UtentesPorTecnico/UtentesPorTecnico.css'],
    templateUrl: '../UtentesPorTecnico/UtentesPorTecnico.html'
})


export class UtentesPorTecnicoComponent implements OnInit {
    private _http: HttpClient;
    private UtentesPorTecnico: UtentesPorTecnicoService;
    //Combobox - Técnicos
    tecnicos: Tecnico[];
    selectedTecnico: string;
    //Tabela - Utentes
    dataSource: UserDataSource;
    displayedColumns = ['nome','morada','contacto'];

    constructor(private http: HttpClient) {
        this._http = http;
        this.UtentesPorTecnico = new UtentesPorTecnicoService(this._http);

        this.UtentesPorTecnico.getTecnicos().subscribe(res => this.tecnicos = res);;
    }

    ngOnInit() { }

    public getUtentesPorTecnicoById() {
        if(this.selectedTecnico!=undefined){
            try {
                this.dataSource = new UserDataSource(this.UtentesPorTecnico, +this.selectedTecnico);
                this.dataSource.connect();
            } catch (error) {
                alert("Erro - " + error);
            }
        }else{
            alert("Selecione um Técnico.");
        }
    }
}

export class UserDataSource extends DataSource<any> {
    private _id: number;

    constructor(private userService: UtentesPorTecnicoService, id: number) {
        super();
        this._id = id;
    }

    connect(): Observable<UtentesPorTecnicoElement[]> {
        return this.userService.getUtentesBy(this._id);
    }

    disconnect() {}
}


