export interface TerapeuticasPorUtenteElement {
    idAgendamento: number;
    idUtente: number;
    utente: string;
    idMedicamento: number;
    medicamento: number;
    quantidade: number;
    idAgendamentoTipo: number;
    dataHora: any;
    dataExecucao: any;
    periodicidade: number;
    fim: any;
}


export interface Utente {
    idUtente: number;
    idLar: number;
    descricao: string;
    nome: string;
    morada: string;
    contacto: string;
  }
