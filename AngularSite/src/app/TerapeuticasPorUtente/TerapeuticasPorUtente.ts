import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DataSource } from '@angular/cdk/collections';
import {FormControl, Validators} from '@angular/forms';
import {MatSelectModule, MatSelect} from '@angular/material/select';

import { TerapeuticasPorUtenteService } from '../TerapeuticasPorUtente/TerapeuticasPorUtente.service';
import { TerapeuticasPorUtenteElement, Utente } from '../TerapeuticasPorUtente/TerapeuticasPorUtente.interface'


@Component({
    selector: 'TerapeuticasPorUtente',
    styleUrls: ['../TerapeuticasPorUtente/TerapeuticasPorUtente.css'],
    templateUrl: '../TerapeuticasPorUtente/TerapeuticasPorUtente.html'
  })



export class TerapeuticasPorUtenteComponent implements OnInit{

    private _http: HttpClient;
    private TerapeuticasPorUtente: TerapeuticasPorUtenteService;
    //COMBOBOX
    utentes: Utente[];
    selectedUtente: string;
    //TABELA
    dataSource: UserDataSource;
    displayedColumns = ['utente', 'medicamento', 'quantidade', 'dataHora', 'dataExecucao', 'periodicidade', 'fim'];

    constructor(private http: HttpClient){ 
        this._http = http;
        this.TerapeuticasPorUtente = new TerapeuticasPorUtenteService(this._http);

        this.TerapeuticasPorUtente.getUtentes().subscribe(res => this.utentes = res);;
    }

    ngOnInit() { }

    public getTerapeuticasPorUtenteById(){
      if(this.selectedUtente != undefined){
        try {
            this.dataSource = new UserDataSource(this.TerapeuticasPorUtente, +this.selectedUtente);
            this.dataSource.connect();
        } catch (error) {
            alert("Erro - " + error);
        }   
      }else{
        alert("Selecione um Utente.");
    }
    }
}

export class UserDataSource extends DataSource<any> {
    private _id:number;

    constructor(private userService: TerapeuticasPorUtenteService, id: number) {
      super();
      this._id = id;
    }
    connect(): Observable<TerapeuticasPorUtenteElement[]> {
      return this.userService.getTerapeuticasBy(this._id);
    }
    disconnect() {}
  }
  


