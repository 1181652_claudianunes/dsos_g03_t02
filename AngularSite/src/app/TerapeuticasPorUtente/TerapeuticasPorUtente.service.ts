import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { TerapeuticasPorUtenteElement, Utente } from '../TerapeuticasPorUtente/TerapeuticasPorUtente.interface'


const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST, PATCH, PUT, DELETE, OPTIONS'
    })
};
const apiUrl = 'http://localhost:8090';


@Injectable()
export class TerapeuticasPorUtenteService {

    constructor(private http: HttpClient) { }

    private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
        console.error(error);
        return of(result as T);
        };
    }

    public getTerapeuticasBy(id: number): Observable<TerapeuticasPorUtenteElement[]> {
        const url = `${apiUrl}/TerapeuticasPorUtente/${id}`;
        return this.http.get<TerapeuticasPorUtenteElement[]>(url);

    }

    public getUtentes(): Observable<Utente[]> {
        const url = `${apiUrl}/Utentes`;
        return this.http.get<Utente[]>(url);

    }


}


