
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {MatSelectModule, MatDatepickerModule, MatFormFieldModule,MatNativeDateModule, MatInputModule, MatCheckboxModule} from '@angular/material';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';


import { AppComponent } from './app.component';
import { TerapeuticasPorUtenteComponent } from './TerapeuticasPorUtente/TerapeuticasPorUtente';
import { UtentesPorTecnicoComponent } from './UtentesPorTecnico/UtentesPorTecnico';
import { AlterarEstadoMedicamentoComponent } from './AlterarEstadoMedicamento/AlterarEstadoMedicamento';
import { EliminarTerapeuticaComponent } from './EliminarTerapeutica/EliminarTerapeutica';
import { TerapeuticasPorDiaComponent } from './TerapeuticasPorDia/TerapeuticasPorDia';


@NgModule({
  declarations: [
    AppComponent,
    TerapeuticasPorUtenteComponent,
    UtentesPorTecnicoComponent,
    AlterarEstadoMedicamentoComponent,
    EliminarTerapeuticaComponent,
    TerapeuticasPorDiaComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatTableModule,
    MatPaginatorModule,
    MatSelectModule,
    MatFormFieldModule,
    FormsModule,
    MatDatepickerModule,
    ReactiveFormsModule,
    MatInputModule,
    MatCheckboxModule,
    MatNativeDateModule
  ],
  providers: [],
  bootstrap: [
    AppComponent, 
      TerapeuticasPorUtenteComponent
  ]
})
export class AppModule { }
