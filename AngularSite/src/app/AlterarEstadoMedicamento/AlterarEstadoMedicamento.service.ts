import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Medicamento } from '../AlterarEstadoMedicamento/AlterarEstadoMedicamento.interface'


const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST, PATCH, PUT, DELETE, OPTIONS'
    })
};
const apiUrl = 'http://localhost:8090';


@Injectable()
export class AlterarEstadoMedicamentoService {

    constructor(private http: HttpClient) { }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.error(error);
            return of(result as T);
        };
    }

    public updateEstadoMedicamentoBy(id: number): Observable<Medicamento[]> {
        const url = `${apiUrl}/AlterarEstadoMedicamento/${id}`;
        return this.http.put<Medicamento[]>(url, id);

    }

    public getMedicamentos(): Observable<Medicamento[]> {
        const url = `${apiUrl}/Medicamentos`;
        return this.http.get<Medicamento[]>(url);
    }
}


