export interface Medicamento {
    idMedicamento: number;
    nome: string;
    idMarca: number;
    marca: string;
    idPosologia: number;
    posologia: string;
    pVenda: any;
    estado: string;
}
