import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Observable } from 'rxjs';
import { DataSource } from '@angular/cdk/collections';
import { HttpClient } from '@angular/common/http';
import { MatPaginator } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';


import { AlterarEstadoMedicamentoService } from '../AlterarEstadoMedicamento/AlterarEstadoMedicamento.service';
import { Medicamento } from '../AlterarEstadoMedicamento/AlterarEstadoMedicamento.interface'


@Component({
    selector: 'AlterarEstadoMedicamento',
    styleUrls: ['../AlterarEstadoMedicamento/AlterarEstadoMedicamento.css'],
    templateUrl: '../AlterarEstadoMedicamento/AlterarEstadoMedicamento.html',
    providers: [AlterarEstadoMedicamentoService]
})

export class AlterarEstadoMedicamentoComponent implements OnInit {
    //TABELA
    dataSource: UserDataSource;
    displayedColumns = ['select', 'nome', 'marca', 'posologia', 'estado'];
    private medicamentoService: AlterarEstadoMedicamentoService;
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

    selection = new SelectionModel<Medicamento>(true, []);

    constructor(http: HttpClient) {
        this.medicamentoService = new AlterarEstadoMedicamentoService(http);
        this.dataSource = new UserDataSource(this.medicamentoService);
        this.dataSource.connect();
    };

    ngOnInit() { }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.toPromise.length;
        return numSelected === numRows;
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: Medicamento): string {
        if (!row) {
            return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.idMedicamento + 1}`;
    }

    wait = false;
    public alterarEstadoMedicamentoById() {
        if(this.selection.selected.length > 0){
            this.selection.selected.forEach(item => {
                this.medicamentoService.updateEstadoMedicamentoBy(item.idMedicamento).subscribe(response => {
                    this.selection.deselect(item);
                    this.dataSource = new UserDataSource(this.medicamentoService);
                    this.dataSource.connect();
                });
            });
        }else{
            alert("Selecione um Medicamento.")
        }
    }
}

export class UserDataSource extends DataSource<Medicamento>{
    data: Observable<Medicamento[]>;
    constructor(private medicamentoService: AlterarEstadoMedicamentoService) {
        super();
    }
    connect(): Observable<Medicamento[]> {
        this.data = null;
        this.data = this.medicamentoService.getMedicamentos();
        return this.data;
    }
    disconnect() { }
}
 





