export interface Terapeutica {
    idAgendamento: number;
    idUtente: number;
    utente: string;
    idMedicamento: number;
    medicamento: number;
    dataHora: any;
    dataExecucao: any;
    quantidade: number;
    idAgendamentoTipo: number;
    agendamento: string;
    periodicidade: number;
    fim: any;
}
