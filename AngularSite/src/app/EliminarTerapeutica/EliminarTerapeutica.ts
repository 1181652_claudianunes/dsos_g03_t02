import { SelectionModel, DataSource } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { MatPaginator } from '@angular/material';

import { EliminarTerapeuticaService } from '../EliminarTerapeutica/EliminarTerapeutica.service';
import { Terapeutica } from '../EliminarTerapeutica/EliminarTerapeutica.interface';


@Component({
    selector: 'EliminarTerapeutica',
    styleUrls: ['../EliminarTerapeutica/EliminarTerapeutica.css'],
    templateUrl: '../EliminarTerapeutica/EliminarTerapeutica.html'
})

export class EliminarTerapeuticaComponent implements OnInit {
    //TABELA
    dataSource: UserDataSource;
    displayedColumns = ['select','utente', 'medicamento', 'quantidade', 'dataHora', 'dataExecucao', 'periodicidade', 'fim']; 
    private terapeuticaService: EliminarTerapeuticaService;

    selection = new SelectionModel<Terapeutica>(true, []);

    constructor(http: HttpClient) {
        this.terapeuticaService = new EliminarTerapeuticaService(http);
        this.dataSource = new UserDataSource(this.terapeuticaService);
        this.dataSource.connect();
    };

    ngOnInit() { }


    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.toPromise.length;
        return numSelected === numRows;
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: Terapeutica): string {
        if (!row) {
            return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.idAgendamento + 1}`;
    }

    wait = false;
    public eliminarTerapeuticaById() {
        if(this.selection.selected.length > 0){
            this.selection.selected.forEach(item => {
                this.terapeuticaService.deleteTerapeuticaBy(item.idAgendamento).subscribe(response => {
                    this.selection.deselect(item);
                    this.dataSource = new UserDataSource(this.terapeuticaService);
                    this.dataSource.connect();
                });            
            });
        }else{
            alert("Selecione uma Terapeutica.")
        }
    }
}

export class UserDataSource extends DataSource<any>{
    data: Observable<Terapeutica[]>;
    constructor(private eliminarTerapeutica: EliminarTerapeuticaService) {
        super();
    }
    connect(): Observable<Terapeutica[]> {
        this.data = null;
        this.data = this.eliminarTerapeutica.getTerapeuticas();
        return this.data;
    }
    disconnect() { }
}

