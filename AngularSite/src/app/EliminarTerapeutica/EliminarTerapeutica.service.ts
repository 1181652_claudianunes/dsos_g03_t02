import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Terapeutica } from '../EliminarTerapeutica/EliminarTerapeutica.interface';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST, PATCH, PUT, DELETE, OPTIONS'
    })
};
const apiUrl = 'http://localhost:8090';


@Injectable()
export class EliminarTerapeuticaService {

    constructor(private http: HttpClient) { }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.error(error);
            return of(result as T);
        };
    }

    public deleteTerapeuticaBy(id: number): Observable<Terapeutica[]> {
        const url = `${apiUrl}/EliminarTerapeutica/${id}`;
        return this.http.delete<Terapeutica[]>(url);;
    }

    public getTerapeuticas(): Observable<Terapeutica[]> {
        const url = `${apiUrl}/Terapeuticas`;
        return this.http.get<Terapeutica[]>(url);
    }
}
