// AQUI FICA O WEBSERVICE

var express = require("express");
var app = express();
var sql = require("mssql");
var cors = require('cors');

app.use(cors());

//Comando para testar: 'node {path_ _file(api.js)}'

app.get("/TerapeuticasPorUtente/:id", function(req, res){
    sql.connect(sqlconfig, function (err) {
        if (err) console.log(err);
        var request = new sql.Request();
        var sqlquery = "exec spSelectAgendamentos @idUtente='" + req.params.id + "'"
        request.query(sqlquery, function (err, recordset) {
            if (err) console.log(err)
            res.send(recordset.recordset);
        });
    });
});

//Listar todos os utentes do lar a que o técnico pertence.
app.get("/UtentesPorTecnico/:id", function (req, res) {
    // connect to your database
    sql.connect(sqlconfig, function (err) {
        if (err) console.log(err);
        var request = new sql.Request();
        var stringRequest = "EXEC spSelectUtentesPorTecnico @idUtilizador =" + req.params.id;
        request.query(stringRequest, function (err, recordset) {
            if (err) console.log(err)
            res.send(recordset.recordset);
        });
    });
});

//Consultar as terapêuticas de um determinado dia.
app.get("/TerapeuticasPorDia/:data", function(req, res){
    sql.connect(sqlconfig, function (err) {
        if (err) console.log(err);
        var request = new sql.Request();
        var stringRequest = "EXEC spSelectAgendamentos @data = '" + req.params.data + "'";
        request.query(stringRequest, function (err, recordset) {
            if (err) console.log(err)
            res.send(recordset.recordset);
        });
    });
});

//Eliminar terapêuticas.
app.delete("/EliminarTerapeutica/:idTerapeutica", function(req, res){
    sql.connect(sqlconfig, function (err) {
        if (err) console.log(err);
        var request = new sql.Request();
        var stringRequest = "EXEC spDeleteAgendamento @idAgendamento =" + req.params.idTerapeutica;
        request.query(stringRequest, function (err, recordset) {
            if (err) console.log(err)
            res.send(recordset.recordset);
        });
    });
});

//Alterar o estado de um medicamento.
app.put("/AlterarEstadoMedicamento/:idMedicamento", function(req, res){
    sql.connect(sqlconfig, function (err) {
        if (err) console.log(err);
        var request = new sql.Request();
        var stringRequest = "EXEC spUpdateEstadoMedicamento @idMedicamento =" + req.params.idMedicamento;
        request.query(stringRequest, function (err, recordset) {
            if (err) console.log(err)
            res.send(recordset.recordset);
        });
    });
});

//Listar todos os Utentes
app.get("/Utentes", function(req, res){
    sql.connect(sqlconfig, function (err) {
        if (err) console.log(err);
        var request = new sql.Request();
        var sqlquery = "exec spSelectUtente";
        request.query(sqlquery, function (err, recordset) {
            if (err) console.log(err)
            res.send(recordset.recordset);
        });
    });
});

//Listar todos os Utilizadores(Técnicos)
app.get("/Tecnicos", function (req, res) {
    sql.connect(sqlconfig, function (err) {
        if (err) console.log(err);
        var request = new sql.Request();
        var sqlquery = "exec spSelectUtilizador";
        request.query(sqlquery, function (err, recordset) {
            if (err) console.log(err)
            res.send(recordset.recordset);
        });
    });
});

//Listar todos os Medicamentos
app.get("/Medicamentos", function (req, res) {
    sql.connect(sqlconfig, function (err) {
        if (err) console.log(err);
        var request = new sql.Request();
        var sqlquery = "exec spSelectMedicamentosAll";
        request.query(sqlquery, function (err, recordset) {
            if (err) console.log(err)
            res.send(recordset.recordset);
        });
    });
});

//Listar todas as Terapeuticas
app.get("/Terapeuticas", function (req, res) {
    sql.connect(sqlconfig, function (err) {
        if (err) console.log(err);
        var request = new sql.Request();
        var sqlquery = "exec spSelectAgendamentos";
        request.query(sqlquery, function (err, recordset) {
            if (err) console.log(err)
            res.send(recordset.recordset);
        });
    });
});

app.get("/appMedicamentos", function(req,res){
    var xhr = new XMLHttpRequest();
    xhr.open('GET', './files/medicamentos.json', true);
    xhr.responseType = 'blob';
    xhr.onload = function(e) { 
      if (this.status == 200) {
          var file = new File([this.response], 'temp');
          var fileReader = new FileReader();
          fileReader.addEventListener('load', function(){
               //do stuff with fileReader.result
          });
          fileReader.readAsText(file);
      } 
    }
    xhr.send();
})

var server = app.listen(8090, function(){
    var host = server.address().address
    var port = server.address().port
});

var sqlconfig = {
    user: 'sa',
    password: 'P1@2019.School',
    server: 'mail1.risi.pt', 
    port: 9999,
    database: 'DSOSG3'
}
